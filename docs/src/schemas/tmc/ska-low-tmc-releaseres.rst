
ska-low-tmc-releaseresources
============================

.. ska-schema:: https://schema.skao.int/ska-low-tmc-releaseresources/3.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-tmc-releaseresources/3.0

       Example JSON.

.. ska-schema:: https://schema.skao.int/ska-low-tmc-releaseresources/2.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-tmc-releaseresources/2.0

       Example JSON.

.. ska-schema:: https://schema.skatelescope.org/ska-low-tmc-releaseresources/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skatelescope.org/ska-low-tmc-releaseresources/1.0

       Example JSON.


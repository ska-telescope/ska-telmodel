"""
Functions to build the Low CSP JSON schemas for Subarray commmands.
"""

from inspect import cleandoc

from ska_telmodel.lowcbf.schema import (
    _get_lowcbf_assignresources_schema,
    _get_lowcbf_configurescan_schema,
    _get_lowcbf_releaseresources_schema,
    _get_lowcbf_scan_schema,
)
from ska_telmodel.lowcbf.version import (
    LOWCBF_ASSIGNRESOURCES_PREFIX,
    LOWCBF_CONFIGURESCAN_PREFIX,
    LOWCBF_RELEASERESOURCES_PREFIX,
    LOWCBF_SCAN_PREFIX,
)

from .._common import TMSchema, split_interface_version
from ..pss.schema import get_pss_config_schema
from ..pst.schema import get_pst_beam_config_schema, get_pst_scan_config_schema
from .common_schema import _get_common_config_schema_without_band
from .low_csp_schema_for_oso_tmc import get_lowcbf_conf_schema_for_oso_tmc
from .low_version import LOWCSP_CONFIGURE_PREFIX, get_csp_subsystem_version
from .schema import get_subarray_config_schema


def get_low_csp_assignresources_schema(version: str, strict: bool) -> TMSchema:
    """
    Build the assignresources JSON schema for the TM-CSP interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema for assignresources command.
    """
    csp_schema = TMSchema.new("LOWCSP assign resources", version, strict)
    (major, minor) = split_interface_version(version)

    csp_schema.add_field(
        "interface",
        str,
        description="URI of JSON schema for this command's JSON payload.",
    )

    if (major, minor) >= (3, 2):
        csp_schema.add_opt_field(
            "transaction_id",
            str,
            description="A transaction id specific to the command",
        )

    cbf_version = get_csp_subsystem_version(version, "lowcbf")
    uri_cbf = LOWCBF_ASSIGNRESOURCES_PREFIX + cbf_version
    cbf_schema = _get_lowcbf_assignresources_schema(uri_cbf, strict)

    common_schema = TMSchema.new(
        "LOWCSP releaseresources description", version, strict
    )
    common_schema.add_field(
        "subarray_id", int, description=cleandoc("subarray id")
    )
    desc = "LOWCSP subarray id arguments"
    csp_schema.add_field("common", common_schema, description=cleandoc(desc))

    csp_schema.add_field(
        "lowcbf",
        cbf_schema,
        description="Low CBF resources",
    ),

    # PST schema
    pst_schema = TMSchema.new("LOWCSP PST beams", version, strict)
    pst_schema.add_field(
        "beams_id",
        [int],
        check_strict=lambda lst: len(lst) <= 16,
        description="List of PST beam Ids to assign to the subarray.",
    )
    csp_schema.add_opt_field(
        "pst", pst_schema, description="Assign section for PST sub-system"
    )
    # PSS schema
    pss_schema = TMSchema.new("LOWCSP PSS beams", version, strict)
    pss_schema.add_field(
        "beams_id",
        [int],
        check_strict=lambda lst: len(lst) <= 500,
        description="List of PSS beam Ids to assign to the subarray.",
    )
    csp_schema.add_opt_field(
        "pss",
        pss_schema,
        description=cleandoc("Assign section for PSS sub-system"),
    )
    return csp_schema


def get_low_csp_configure_schema(
    version: str,
    strict: bool,
    tmc_schema_uri: str = "",
) -> TMSchema:
    """
    Build the configure JSON schema for the TM-CSP interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param tmc_schema_uri: tmc URI version. If defined, the CSP configure
        schema for the OSO-TMC interface is returned. Otherwise, the one
        for the TMC-CSP.

    :return: Schema for configure command.
    """
    main_schema = TMSchema.new(
        "LOWCSP configure",
        version,
        strict,
        description=cleandoc(
            """
            Low CSP specific parameters. This section
            contains the parameters relevant to configure the Low CSP
            sub-system.
            """
        ),
    )
    (major, minor) = split_interface_version(version)

    main_schema.add_field(
        "interface",
        str,
        description=("URI of JSON schema for this command's" "JSON payload.."),
    )

    if (major, minor) >= (3, 2):
        main_schema.add_opt_field(
            "transaction_id",
            str,
            description="A transaction id specific to the command",
        )
    if tmc_schema_uri:
        tmc_version = split_interface_version(tmc_schema_uri)

    # pylint: disable-next=possibly-used-before-assignment
    if not tmc_schema_uri or tmc_version <= (3, 0):
        # added for TMC-CSP interface and for old for OSO-TMC interface
        main_schema.add_opt_field(
            "subarray",
            get_subarray_config_schema(version, strict),
            description="Subarray elements",
        )
    common_schema = _get_common_config_schema_without_band(
        version, strict, tmc_schema_uri=tmc_schema_uri
    )
    main_schema.add_field("common", common_schema)

    cbf_version = get_csp_subsystem_version(version, "lowcbf")
    uri_cbf = LOWCBF_CONFIGURESCAN_PREFIX + cbf_version
    if tmc_schema_uri:
        # retrieve the Low CBF configuration schema for OSO-TMC interface
        main_schema.add_opt_field(
            "lowcbf",
            get_lowcbf_conf_schema_for_oso_tmc(
                uri_cbf, strict, tmc_schema_uri
            ),
        )
    else:
        # retrieve the Low CBF configuration schema for TMC-CSP interface
        main_schema.add_opt_field(
            "lowcbf",
            _get_lowcbf_configurescan_schema(
                uri_cbf, strict, address_required=False
            ),
        )

    # LOW PSS #
    pss_version = get_csp_subsystem_version(version, "pss")
    uri_pss = LOWCBF_CONFIGURESCAN_PREFIX + pss_version
    main_schema.add_opt_field(
        "pss",
        get_pss_config_schema(uri_pss, strict),
        description=cleandoc(
            "Section with parameters to configure the PSS sub-system"
        ),
    )

    # PST main schema
    pst_version = get_csp_subsystem_version(version, "pst")
    uri_pst = LOWCSP_CONFIGURE_PREFIX + pst_version
    pst_schema = TMSchema.new(
        "LOW PST configure",
        uri_pst,
        strict,
        description=cleandoc(
            """
            Main configuration for the Low
            CSP Pulsar timing sub-system
            """
        ),
        as_reference=True,
    )
    # Low PST schema
    pst_beam_schema = TMSchema.new(
        "LOWCSP configurescan pst beams",
        uri_pst,
        strict,
        description=cleandoc("Parameters to configure the PST sub-system"),
        # as_reference=True
    )
    pst_beam_schema.add_field(
        "beam_id",
        int,
        description=cleandoc("Configuration for a PST beam ID"),
    )
    # Retrieve the PST configuration schema for scan and beam and
    # add then to the CSP schema.

    pst_beam_schema.add_field(
        "scan",
        get_pst_scan_config_schema(uri_pst, strict, beamid_required=False),
        description=cleandoc("Parameters to configure the scan"),
    )
    pst_beam_schema.add_opt_field(
        "beam",
        get_pst_beam_config_schema(uri_pst, strict),
        description=cleandoc("Parameter to configure the beam"),
    )
    # PST beams
    pst_schema.add_field(
        "beams",
        [pst_beam_schema],
        description=cleandoc("List of PST Beams IDs to configure"),
    )

    desc = "Section with parameters to configure the PST sub-system."
    main_schema.add_opt_field("pst", pst_schema, description=cleandoc(desc))

    return main_schema


def get_low_csp_releaseresources_schema(
    version: str, strict: bool
) -> TMSchema:
    """
     Build the releaseresources JSON schema for the TM-CSP interface.

    :param version: Interface version
    :param strict: Strict mode - refuse even harmless schema
       violations (like extra keys). DO NOT USE FOR INPUT VALIDATION!
    :return: The JSON Schema for the CSP releaseresources command.
    :raise: `ValueError` exception on mismatch major version or invalid JSON
            Schema URI
    """

    items = TMSchema.new("LOWCSP release resources", version, strict)
    (major, minor) = split_interface_version(version)
    items.add_field(
        "interface",
        str,
        description=cleandoc(
            "URI of JSON schema for this command's" "JSON payload.."
        ),
    )
    if (major, minor) >= (3, 2):
        items.add_opt_field(
            "transaction_id",
            str,
            description="A transaction id specific to the command",
        )
    common_schema = TMSchema.new(
        "LOWCSP releaseresources description",
        version,
        strict,
        as_reference=True,
    )
    common_schema.add_field(
        "subarray_id", int, description=cleandoc("subarray id")
    )
    desc = "LOWCSP subarray id arguments"
    items.add_field("common", common_schema, description=cleandoc(desc))

    pst_schema = TMSchema.new(
        "LOWCSP releaseresources pst beams", version, strict
    )
    pst_schema.add_field(
        "beams_id",
        [
            int,
        ],
        description=cleandoc("Beams id list"),
    )
    desc = "List of PST Beams IDs"
    items.add_opt_field("pst", pst_schema, description=cleandoc(desc))

    pss_schema = TMSchema.new(
        "LOWCSP releaseresources pss beams",
        version,
        strict,
        as_reference=True,
    )
    pss_schema.add_field(
        "beams_id",
        [
            int,
        ],
        description=cleandoc("List of PSS Beams IDs"),
    )

    items.add_opt_field(
        "pss",
        pss_schema,
        description=cleandoc(
            """
            Section with the Pulsar Search resources
            to remove from a CSP Subarray
            """
        ),
    )

    lowcbf_version = get_csp_subsystem_version(version, "lowcbf")
    low_cbf_interface = LOWCBF_RELEASERESOURCES_PREFIX + lowcbf_version

    items.add_field(
        "lowcbf",
        _get_lowcbf_releaseresources_schema(low_cbf_interface, strict),
    )
    return items


def get_low_csp_scan_schema(version: str, strict: bool) -> TMSchema:
    """
    Build the scan JSON schema for the TMC-CSP interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema for configure command.
    :raise: `ValueError` exception on invalid JSON Schema URI.
    """

    items = TMSchema.new("LOWCSP scan description", version, strict)
    (major, minor) = split_interface_version(version)
    items.add_field(
        "interface", str, description=cleandoc("LOW CSP SCAN interface")
    )
    if (major, minor) >= (3, 2):
        items.add_opt_field(
            "transaction_id",
            str,
            description="A transaction id specific to the command",
        )
    common_schema = TMSchema.new("LOWCSP common section", version, strict)
    common_schema.add_field(
        "subarray_id", int, description=cleandoc("subarray id")
    )
    desc = "LOWCSP subarray id arguments"
    items.add_field("common", common_schema, description=desc)

    major, minor = split_interface_version(version)
    if (major, minor) < (4, 0):
        lowcbf_version = get_csp_subsystem_version(version, "lowcbf")
        low_cbf_interface = LOWCBF_SCAN_PREFIX + lowcbf_version
        cbf_schema = _get_lowcbf_scan_schema(low_cbf_interface, strict)
        items.add_field(
            "lowcbf", cbf_schema, description="LOW CBF scan schema"
        )
    else:
        items.add_field("scan_id", int, description="Scan ID")
    return items

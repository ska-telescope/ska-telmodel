"""
Unit tests for the ska_telmodel.skydirection module.
"""
import abc
import json
from contextlib import nullcontext as does_not_raise
from itertools import product

import pytest
from schema import SchemaError

from ska_telmodel.skydirection import (
    AltAzSkyDirection,
    GalacticSkyDirection,
    ICRSSkyDirection,
    SpecialSkyDirection,
    TLESkyDirection,
    get_skydirection,
)


def case_permutations(val: str) -> list[str]:
    """
    Return all possible permutations in case of an input value.

    Example:

        'ab' -> ['ab', 'aB', 'Ab', 'AB']
    """
    cases = zip(*[val, val.swapcase()])
    return ["".join(permutation) for permutation in product(*cases)]


class SkyDirectionTestTemplate:
    """
    Shared test implementations for SkyDirections.

    Most unit tests for SkyDirections vary in only a couple of aspects: the
    schema class, and the example JSON to be tested. This template class puts
    the common code in a template where it can be called in a parametrized
    fashion by the type-specific unit tests.
    """

    @abc.abstractmethod
    def get_schema_instance(self, strict: bool = True):
        raise NotImplementedError

    def _test_schema_validates_example(self, example: str):
        """
        Test that an ADR-63 JSON example is allowed by the schema when
        validating in strict mode.

        This function is intended to be parametrized with JSON examples.
        """
        schema = self.get_schema_instance()
        example = json.loads(example)
        schema.validate(example)

    def _test_case_insensitive_reference_frame(self, example, new_frame):
        """
        Test that reference_frame validation is case-insensitive.

        This function is intended to be parametrized with JSON examples.
        """
        schema = self.get_schema_instance()
        example = json.loads(example)
        example["reference_frame"] = new_frame
        schema.validate(example)

    def _test_c1_c2_limits(self, example, c1, c2, expectation):
        """
        Check that errors are raised when c1,c2 values exceed bounds.
        """
        schema = self.get_schema_instance()
        example = json.loads(example)
        example["attrs"]["c1"] = c1
        example["attrs"]["c2"] = c2

        with expectation:
            schema.validate(example)

    def _test_attrs_deletion(self, example, field, expectation):
        """
        Test whether deleting a value from the attrs dict does or does not
        raise an exception.
        """
        schema = self.get_schema_instance()
        example = json.loads(example)
        del example["attrs"][field]
        with expectation:
            schema.validate(example)


class TestSpecialSkyDirection(SkyDirectionTestTemplate):
    EXAMPLE = """
    {
        "target_name": "Venus",
        "reference_frame": "special"
    }
    """

    def get_schema_instance(self, strict: bool = True):
        return SpecialSkyDirection(
            name="test", version="foo/bar/1.0", strict=strict
        )

    @pytest.mark.parametrize("example", [EXAMPLE])
    def _test_schema_validates_example(self, example: str):
        super()._test_schema_validates_example(example)

    @pytest.mark.parametrize("new_frame", [*case_permutations("special")])
    def test_case_insensitive_reference_frame(self, new_frame: str):
        super()._test_case_insensitive_reference_frame(self.EXAMPLE, new_frame)


class TestICRSSkyDirection(SkyDirectionTestTemplate):
    FULL_EXAMPLE = """
        { 
            "target_name": "Cen-A",
            "reference_frame": "icrs",
            "attrs": {
                "c1": 201.365,
                "c2": -43.0191667,
                "pm_c1": 0.0,
                "pm_c2": 0.0,
                "epoch": 2000.0,
                "parallax": 0.0,
                "radial_velocity": 0.0
            }
        }
    """  # noqa: W291

    MINIMAL_EXAMPLE = """
        { 
            "target_name": "Cen-A",
            "reference_frame": "icrs",
            "attrs": {
                "c1": 201.365,
                "c2": -43.0191667
            }
        }
    """  # noqa: W291

    def get_schema_instance(self, strict: bool = True):
        return ICRSSkyDirection(
            name="test", version="foo/bar/1.0", strict=strict
        )

    @pytest.mark.parametrize("example", [FULL_EXAMPLE, MINIMAL_EXAMPLE])
    def _test_schema_validates_example(self, example: str):
        super()._test_schema_validates_example(example)

    @pytest.mark.parametrize("new_frame", [*case_permutations("icrs")])
    def test_case_insensitive_reference_frame(self, new_frame: str):
        super()._test_case_insensitive_reference_frame(
            self.FULL_EXAMPLE, new_frame
        )

    @pytest.mark.parametrize(
        "c1,c2,expectation",
        [
            pytest.param(
                0.0, -90.0, does_not_raise(), id="ra/dec at min limits"
            ),
            pytest.param(
                359.999, 90.0, does_not_raise(), id="ra/dec at max limits"
            ),
            pytest.param(-0.1, 0.0, pytest.raises(SchemaError), id="ra < 0.0"),
            pytest.param(
                360.0, 0.0, pytest.raises(SchemaError), id="ra >= 360.0"
            ),
            pytest.param(
                0.0, -90.1, pytest.raises(SchemaError), id="dec < -90.0"
            ),
            pytest.param(
                0.0, 90.1, pytest.raises(SchemaError), id="dec > 90.0"
            ),
        ],
    )
    def test_limits(self, c1, c2, expectation):
        super()._test_c1_c2_limits(self.FULL_EXAMPLE, c1, c2, expectation)

    @pytest.mark.parametrize(
        "field,expectation",
        [
            pytest.param("c1", pytest.raises(SchemaError), id="c1: mandatory"),
            pytest.param("c2", pytest.raises(SchemaError), id="c2: mandatory"),
            pytest.param("pm_c1", does_not_raise(), id="pm_c1: optional"),
            pytest.param("pm_c2", does_not_raise(), id="pm_c2: optional"),
            pytest.param("epoch", does_not_raise(), id="epoch: optional"),
            pytest.param(
                "parallax", does_not_raise(), id="parallax: optional"
            ),
            pytest.param(
                "radial_velocity",
                does_not_raise(),
                id="radial_velocity: optional",
            ),
        ],
    )
    def test_attrs_deletion(self, field, expectation):
        super()._test_attrs_deletion(self.FULL_EXAMPLE, field, expectation)


class TestGalacticSkyDirection(SkyDirectionTestTemplate):
    FULL_EXAMPLE = """
    { 
        "target_name": "Cass-A",
        "reference_frame": "galactic",
        "attrs": {
            "c1": 111.734745,
            "c2": -2.129570,
            "pm_c1": 0.0,
            "pm_c2": 0.0,
            "epoch": 2000.0,
            "parallax": 0.0,
            "radial_velocity": 0.0
        }
    }
    """  # noqa: W291

    MINIMAL_EXAMPLE = """
    { 
        "target_name": "Cass-A",
        "reference_frame": "galactic",
        "attrs": {
            "c1": 111.734745,
            "c2": -2.129570,
        }
    }
    """  # noqa: W291

    def get_schema_instance(self, strict: bool = True):
        return GalacticSkyDirection(
            name="test", version="foo/bar/1.0", strict=strict
        )

    @pytest.mark.parametrize("example", [FULL_EXAMPLE, MINIMAL_EXAMPLE])
    def _test_schema_validates_example(self, example: str):
        super()._test_schema_validates_example(example)

    @pytest.mark.parametrize("new_frame", [*case_permutations("galactic")])
    def test_reference_frame_validation_is_case_insensitive(
        self, new_frame: str
    ):
        super()._test_case_insensitive_reference_frame(
            self.FULL_EXAMPLE, new_frame
        )

    @pytest.mark.parametrize(
        "c1,c2,expectation",
        [
            pytest.param(0.0, -90.0, does_not_raise(), id="l/b at min limits"),
            pytest.param(
                359.999, 90.0, does_not_raise(), id="l/b at max limits"
            ),
            pytest.param(-0.1, 0.0, pytest.raises(SchemaError), id="l < 0.0"),
            pytest.param(
                360.0, 0.0, pytest.raises(SchemaError), id="l >= 360.0"
            ),
            pytest.param(
                0.0, -90.1, pytest.raises(SchemaError), id="b < -90.0"
            ),
            pytest.param(0.0, 90.1, pytest.raises(SchemaError), id="b > 90.0"),
        ],
    )
    def test_limits(self, c1, c2, expectation):
        super()._test_c1_c2_limits(self.FULL_EXAMPLE, c1, c2, expectation)

    @pytest.mark.parametrize(
        "field,expectation",
        [
            pytest.param("c1", pytest.raises(SchemaError), id="c1: mandatory"),
            pytest.param("c2", pytest.raises(SchemaError), id="c2: mandatory"),
            pytest.param("pm_c1", does_not_raise(), id="pm_c1: optional"),
            pytest.param("pm_c2", does_not_raise(), id="pm_c2: optional"),
            pytest.param("epoch", does_not_raise(), id="epoch: optional"),
            pytest.param(
                "parallax", does_not_raise(), id="parallax: optional"
            ),
            pytest.param(
                "radial_velocity",
                does_not_raise(),
                id="radial_velocity: optional",
            ),
        ],
    )
    def test_attrs_deletion(self, field, expectation):
        super()._test_attrs_deletion(self.FULL_EXAMPLE, field, expectation)


class TestTLESkyDirection(SkyDirectionTestTemplate):
    EXAMPLE = """
    { 
        "target_name": "ISS (ZARYA)",
        "reference_frame": "tle",
        "attrs": {
            "line1": "1 25544U 98067A   08264.51782528 -.00002182  00000-0 -11606-4 0  2927",
            "line2": "2 25544  51.6416 247.4627 0006703 130.5360 325.0288 15.72125391563537"
        }
    }
    """  # noqa: W291,E501

    def get_schema_instance(self, strict: bool = True):
        return TLESkyDirection(
            name="test", version="foo/bar/1.0", strict=strict
        )

    @pytest.mark.parametrize("example", [EXAMPLE])
    def _test_schema_validates_example(self, example: str):
        super()._test_schema_validates_example(example)

    @pytest.mark.parametrize("new_frame", [*case_permutations("tle")])
    def test_case_insensitive_reference_frame(self, new_frame: str):
        super()._test_case_insensitive_reference_frame(self.EXAMPLE, new_frame)

    @pytest.mark.parametrize(
        "field,expectation",
        [
            pytest.param(
                "line1", pytest.raises(SchemaError), id="line1: mandatory"
            ),
            pytest.param(
                "line2", pytest.raises(SchemaError), id="line2: mandatory"
            ),
        ],
    )
    def test_limits(self, field, expectation):
        super()._test_attrs_deletion(self.EXAMPLE, field, expectation)


class TestAltAzSkyDirection(SkyDirectionTestTemplate):
    EXAMPLE = """
    { 
        "target_name": "Zenith",
        "reference_frame": "altaz",
        "attrs": {
            "c1": 180.0,
            "c2": 90.0
        }
    }
    """  # noqa: W291

    def get_schema_instance(self, strict: bool = True):
        return AltAzSkyDirection(
            name="test", version="foo/bar/1.0", strict=strict
        )

    @pytest.mark.parametrize("example", [EXAMPLE])
    def _test_schema_validates_example(self, example: str):
        super()._test_schema_validates_example(example)

    @pytest.mark.parametrize("new_frame", [*case_permutations("altaz")])
    def test_case_insensitive_reference_frame(self, new_frame: str):
        super()._test_case_insensitive_reference_frame(self.EXAMPLE, new_frame)

    @pytest.mark.parametrize(
        "c1,c2,expectation",
        [
            pytest.param(0.0, 0.0, does_not_raise(), id="az/el at min limits"),
            pytest.param(
                359.999, 90.0, does_not_raise(), id="az/el at max limits"
            ),
            pytest.param(-0.1, 0.0, pytest.raises(SchemaError), id="az < 0.0"),
            pytest.param(
                360.0, 0.0, pytest.raises(SchemaError), id="az >= 360.0"
            ),
            pytest.param(0.0, -0.1, pytest.raises(SchemaError), id="el < 0.0"),
            pytest.param(
                0.0, 90.1, pytest.raises(SchemaError), id="el > 90.0"
            ),
        ],
    )
    def test_limits(self, c1, c2, expectation):
        super()._test_c1_c2_limits(self.EXAMPLE, c1, c2, expectation)

    @pytest.mark.parametrize(
        "field,expectation",
        [
            pytest.param("c1", pytest.raises(SchemaError), id="c2: mandatory"),
            pytest.param("c2", pytest.raises(SchemaError), id="c2: mandatory"),
        ],
    )
    def test_attrs_deletion(self, field, expectation):
        super()._test_attrs_deletion(self.EXAMPLE, field, expectation)


class TestSkyDirectionSchema:
    @pytest.mark.parametrize(
        "example",
        [
            TestICRSSkyDirection.FULL_EXAMPLE,
            TestAltAzSkyDirection.EXAMPLE,
            TestGalacticSkyDirection.FULL_EXAMPLE,
            TestSpecialSkyDirection.EXAMPLE,
            TestTLESkyDirection.EXAMPLE,
        ],
    )
    def test_schema_validates_adr63_examples(self, example: str):
        """
        Test that the ADR-63 examples are allowed by the schema when
        validating in strict mode.

        These tests are *not* an exhaustive test of the schema!
        """
        schema = get_skydirection("foo/bar/1.0", True)
        example = json.loads(example)
        schema.validate(example)

    @pytest.mark.parametrize(
        "example",
        [
            TestICRSSkyDirection.FULL_EXAMPLE,
            TestAltAzSkyDirection.EXAMPLE,
            TestGalacticSkyDirection.FULL_EXAMPLE,
            TestSpecialSkyDirection.EXAMPLE,
            TestTLESkyDirection.EXAMPLE,
        ],
    )
    def test_schema_validation_is_not_stateful(self, example):
        """
        Test that we haven't accidentally set only_one=True on the
        SkyDirection schema.

        Creating the SkyDirection Or() schema union must be done in a
        stateless manner, otherwise additional validations will fail. The
        Or() docstring say this on the matter:

         If one wants to make an xor, one can provide only_one=True optional
         argument to the constructor of this object. When a validation was
         performed for an xor-ish Or instance and one wants to use it another
         time, one needs to call reset() to put the match_count back to 0.

        As we can't subclass TMSchema, we can't decorate validate to do a
        'finally: self.reset()'.
        """
        schema = get_skydirection("foo/bar/1.0", True)
        example = json.loads(example)
        schema.validate(example)
        schema.validate(example)

############
Change Log
############

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

Development
***********

1.20.1
*******
* AT4-1719: The PSS schema now has independent versioning from the CSP versioning. The first valid version is 0.1 

1.20.0
******
* Added versions 0.5 and 1.0 of SDP command and receive addresses schemas
* Add "visibility_beam_id" as optional parameter in SDP schema version 0.4. Fixes SKB-672.
* Refactored SDP schemas so that different versions are defined in
  separate files. This should make long-term maintenance easier.
* Only allow strict validation (level 2) when ``PYTEST_VERSION`` or
  ``SKA_TELMODEL_ALLOW_STRICT_VALIDATION`` environment variables are
  set
* SDP AssignResources v1.0 added, where ``execution_block`` fields are specified
  using ADR-63 sky coordinates.

* SKB-749: Fix ``midcbf.pst_bf`` unit tests to check fewer examples for time saving
* Added new schema validation support without updating the tel model python library dependency,
  when ``jsonschema_fallback`` is true. Validation done using external documented URL.
  

1.19.8
******
  * CIP-2662: Mid.CSP configurescan 5.0 and Mid.CBF initsysparams 1.1 added:
  * initsysparams 1.1 - Improve descriptions for: dish_parameters.k
  * Adds midcbf.pst-bf parameter for PST beam-forming configuration
  * midcbf.correlation is now optional
  * Remove midcbf.correlation.processing_regions.output_port validation of
    increments of 20 (Mid.CBF.MCS to perform this validation)
  * Improve descriptions for - common.frequency_band, midcbf.correlation.processing_regions.integration_factor

1.19.7
******
* Relax station regex

1.19.6
******
* Bugfix: TMC-Mid Configure schema version 3.0 and above, changed reference_frame back to
  optional to allow partial configuration (BTN-2512)

1.19.5
******
* Add long list to cli.
* Added helper methods for common actions on large files
* CIP-2949 Fix circular dependency in ska_telmodel.csp package
* Added a skydirection schema to handle all sky directions (co-ordinates) supported by SKA.
* Corrected TMC-Mid Configure v4.1 to use the common ADR-63 compliant Field schema
* Modified PST beams to use the common ADR-63 compliant Field schema

1.19.4
******
* TMC-Mid Configure v4.1: Added pointing.groups to bring OSO/TMC-Mid pointing interface up to date with
  ADR-63 (sky coordinates), ADR-106 (tracking and mapping), and ADR-94 (holography).
* TMC-Mid Configure v4.1: Deprecated pointing.target
* SKB-462 resolved

1.19.3
******
* Added TMC Low configure schema 4.2 and examples
  * Added field block inside beam section
* Updated apperture and receptor keys value in Assignresuorces 4.0 example
* Fix SKB-398: Implement file locking when loading data into cache

1.19.2
******
* Fix SKB-477: OSO-TMC low scan v4.0 incorrectly contains subarray_id in Telescope Model
* Remove the subarray_id from oso-tmc scan schema generation and corrected the example json for scan v4.0

1.19.1
******
* Fix logo and favicon
* Fix CSP Mid ConfigureScan 4.0 PST flow through example displaying a 3.0
  example erroneously
* Added CSP Mid ConfigureScan 4.1 to fix validation

  * Remove array length upper bounds restriction on processing_regions
    output_port
  * Modify the range validation of processing_regions start_freq

1.19.0
******
* | **BREAKING CHANGE**: CLI arguments have a specific order now:
  | ``ska-telmodel <global arguments> <command> <sub-command arguments>``
  | For example ``ska-telmodel -U cp -R software``
* Rename CSP Mid example version from 2.6 to 2.3
* Fix get_pst_beam_descr_outer_for_oso_tmc() tmc interface validation
* Fix LOW_TMC_CONFIGURE_4_0_PST_SCAN_VOLTAGE_RECORDER example
* Updated CLI to include new ``upload`` comand.
* Added new libraries to allow for adding/updaing data in Telesope Model repositories.
* Added CSP Mid ConfigureScan 4.0 schema and examples

  * cbf parameter renamed to midcbf
  * Added correlation parameter
  * Added processing_regions for correlation

    * Defines a region (a range of frequencies) and parameters needed to
      process that region. Replaces the parameters specified for each
      individual FSP.
    * Added parameter fsp_ids
    * Added parameter sdp_start_channel_id
    * Added parameter start_freq
    * Added parameter channel_count
    * Added parameter channel_width
    * Changed validation range for output_host
    * Changed validation range for output_port
    * Changed validation range for output_link_map

  * Removed fsp parameter, moved the following parameter to processing_regions

    * receptors
    * integration_factor
    * output_host
    * output_port
    * output_link_map

  * Added OSO-TMC configure schema v4.0 with example as per ADR-99

    * Removed subarray_id from csp common
    * Update key cbf to midcbf
    * Update channel_offset to sdp_start_channel_id
    * Removed fsp from cbf
    * Utilise changes from CIP-2252, to add correlation and processing_regions for midcbf
    * Added positive and negative tests for OSO-TMC v4.0 validations
    * Added documentation for v4.0

1.18.2
******
* Fix the make_csp_config to set the default value of visibility_beam_id to 0,
  if not provided

1.18.1
******
* Enable the commented code in csp section which will enable the
  make_low_csp_config function.
* The enables the extraction of host, port, mac feilds from sdp json, which are
  required by low csp.

1.18.0
******
* BREAKING CHANGE: in ska-tmc-configure 3.0, reference_frame is now a
  mandatory attribute of a pointing.target, and must have a value of either
  'special' or 'ICRS'.
* Made sections/descriptions specific to Mid
* Added links to AA0.5 Mid CBF limitations described in Confluence
* Refactored CSP examples to reduce file length
* (MID) CSP Delay Model start_validity_sec description updated
* Added (MID) CSP config 3.0:

  * channel_averaging_map description updated
  * channel_offset description updated and default value added
  * config_id changed to mandatory field
  * delay_model_subscription_point changed to mandatory field
  * doppler_phase_corr_subscription_point removed
  * frequency_band description and validation pattern updated
  * frequency_band_offset_stream1|2 description updated and validation added
  * integration_factor range validation added
  * output_host description updated and validation added
  * output_link_map now mandatory with additional validation added
  * output_mac removed
  * output_port description updated and validation added
  * subarray removed
  * Transient Data Capture fields removed
  * zoom_factor removed
  * zoom_window_tuning removed

* Added (Mid) CSP assignresources 3.0:

  * subarray_id range validation added

* Added (Mid) CSP releaseresources 3.0:

  * subarray_id range validation added
  * Split improper example into two correct examples

* Added examples for the above added (MID) CSP command versions
* Modified existing and added new (MID) CSP schema tests for new validation
* Modified (MID) CSP \<\-\-\> SDP receive address tests for (MID) CSP Config
  3.0 example

1.17.1
******
* Removed astropy and simpleeval from dependencies and updated to use poetry 1.8+

1.17.0
******
* BREAKING CHANGE: Major update to MCCS allocation section of ska-low-tmc-assignresources to align with ska-low-mccs-controller-allocate/3.0
* BREAKING CHANGE: Major update to CSP configuration section of ska-low-tmc-configure to include PST schema updates
* Extend make_csp_config() to be used also for Low schema
* Add transaction_id as optional field in CSP Mid and Low schema
* Rework CSP documentation internal structure

1.16.0
******
* Decoupling of Semantic Validation and OSD functionalities from ska-telmodel
* These have been added into a new repo ska-ost-osd and exposed as a service

1.15.1
******
* Created Configure schema for Low OSO-TMC.
* Unit test cases to verify the validations for added Low OSO-TMC Configure schema.
* Updated documentation for Low OSO-TMC latest schema.
* fix get_low_csp_common() function to allow TMC to get proper schema

1.15.0
******
* Update Mid Delay Model format to comply with ADR-88

1.14.1
******
* Allow the same k value be used by multiple receptors in Mid CBF InitSysParams.

1.14.0
******
* Created schema for Low CSP.
* Moved PST schema in a separate folder and created its own URI.
* Nakshatra changes implemented in Low CSP/CBF.
* Reorganization of the documentation structure.
* Added CSP LOW delaymodel json schema as per ADR-88.
* Unit test cases to verify the validations for added CSP LOW delay model schema.
* Updated documentation for CSP LOW delaymodel.

1.13.0
******
*  Added initial version of Observatory Static Data.
*  Integrated Observatory Static Data (OSD) with existing semantic validation framework.

1.12.0
******
* Extended support of semantic validation for Scheduling Block Definition.
* Updated existing semantic validation test-cases.
* Updated documentation for CSP LOW assignresources command.

1.11.2
******
* Updated datatype of epoch in delayModel to float

1.11.1
******
* Updated PST Flow Through configuration

1.11.0
******
* Added new schema section for TMC LOW commands
* Added initial Semantic Validation for LOW observing setup

1.10.0
******
* Added new schema section for midcbf InitSysParams command
* Added schema and test cases for midcbf InitSysParams command

1.9.2
*****
* Added new field z_pos to antenna_geojson structure
* Added documentation for station and antenna geojson

1.9.1
*****
* Mid cbf scan_id changed from string to integer (SKB-254)
* Added tutorials to restructured documentation
* Support shortened "car:" URI scheme (defaults to "gitlab.com/ska-telescope/"
  prefix and "#tmdata" segment)

1.9.0
*****
* Adds support for partial configuration with Target-offset parameters to enable
  5-point calibration scans. (BTN-2052)
* Adds a new module for semantic validation of Low telescope
  configuration. (NAK-673)

1.8.2
*****
* Bugfix: 'simpleeval' and 'astropy' are required in production, not only
  as development dependencies.

1.8.1
*****
* Update receptor validation and example values to match ADR-32
* Added optional eb_id to CSP common schema

1.8.0
*****
* Fixed semantic validation issue on receptor_id and fsp_id for AA0.5 schema.
* Add new "car://" backend type that behaves like "nexus://", but enforces
  that data comes from artefact repository
* Fix handling of the CAR_RAW_REPOSITORY_URL to fix behaviour in CI pipelines
* Added station_id to version 1.1 of the receptor schema
* Renamed station_name to station_label in version 1.1 of the receptor schema
* Added the various changes required to the update scripts
* Add documentation for Mid.CBF command schema

1.7.0
*****
* Added new semantic validation support for AA0.5 schema

1.6.0
*****
* Add schemas for Low CBF configuration commands
* Add receptor_id to version 2.1 of TMC release resource schema

1.5.0
*****
* Add version 2.1 of SKA-MID assign,configure,release,scan schema to support standardised keys.

1.4.1
*****
* PST schema updates following review

1.4.0
*****

* Added telescope model data interface to query sources of truth on
  matters of static telescope information
* Added layout schemas in support to provide data for delay modelling.
  Including schemas for geocentric, geodectic and local positions, and
  fixed delays.
* Added delay model schema to CSP
* Refactored CSP version code for config to use common version check function

1.3.3
*****
* SDP schema refactoring
* Implement SDP scan metadata required for multi-scan support (version
  0.4, including new ReleaseResources schema)
* Add receive address propagation support for PSS & PST
* Introduced CSP schemas and examples: assignresources, scan, endscan, and releaseresources

1.3.2
*****
* Using standard SKAO CI stages now
* Substantial internal code refactoring - build schemas incrementally
* Add PST (Pulsar Timing) configuration schemas to CSP
* Add PSS (Pulsar Search) configuration schemas to CSP

1.3.1
*****
* Update values in example file for CSP Configure schema
* Enhance CSP Schema version check logic

1.3.0
*****
* Add version 2.0 of CSP Configure schema to support standardised keys (ADR-35)
* Add version 2.0 of TMC schemas for SKA-Low to support standardised keys (ADR-35)

1.2.0
*****
* Add version 0.3 of SDP schemas to support standardised keys (ADR-35)

1.1.0
*****
* Introduce TMC configuration to the TMC SubArrayNode.Configure schema

1.0.0
*****
* Introduced schema for TMC CentralNode and TMC SubArrayNode, currently
  just for SKA LOW.
* Introduced schema for MCCSController and MCCSSubarray

0.3.0
*****
* Generate schema description into Sphinx documentation instead of
  using bootprint
* Replaces specialised validation routines by a general one that
  selects the schema by the URI.

0.2.0
*****
* Implementation of changes in CSP configuration string according ADR-18
* Especially add stubs for PSS and PST configuration
* Rework version handling to use URIs as suggested by ADR-22

0.1.4
*****
* Accept raw dictionaries instead of strings

0.1.3
*****
* Added SDP schema verifications

0.1.2
*****
* Added CSP schema verification

0.1.1
*****
* Renamed `outputChannelOffset` to `fspChannelOffset`

0.1.0
*****
* Initial release
* Added CSP interface generation

"""
Refactoring script for going from

  a = {
   'x': bla,
   'y': blub,
   # ... long ...
  }

to

  a = {}
  a['x'] = bla
  a['y'] = blub,
  # ... long ...

"""

import sys

from bowler import Query
from fissix import fixer_util, pgen2, pygram, pytree


def large_dicts_to_assign(node, cap, fn):
    # Check that it is long enough
    if "elems" not in cap:
        return
    lines_covered = cap["elems"].children[-1].get_lineno() - node.get_lineno()
    if lines_covered < 1:
        return

    # Go through list
    names = []
    vals = []
    i = 0
    while i + 3 <= len(cap["elems"].children):
        names.append(cap["elems"].children[i])
        vals.append(cap["elems"].children[i + 2])
        i += 4

    # Get index of statment in parent so we can start adding elements
    parent = node.parent
    for stmt_ix, stmt in enumerate(parent.children):
        if stmt is node:
            break

    # Start moving elements (back to front)
    for n, v in zip(reversed(names), reversed(vals)):
        # Remove name + value
        n.remove()
        v.remove()

        # Construct 'name[{n}] = {v}'
        indent = fixer_util.find_indentation(node)
        assign = pytree.Node(
            pygram.python_symbols.simple_stmt,
            [
                fixer_util.Assign(
                    pytree.Node(
                        pygram.python_symbols.power,
                        [
                            fixer_util.Name(cap["name"].value),
                            fixer_util.ArgList(
                                [n],
                                pytree.Leaf(pgen2.token.LSQB, "["),
                                pytree.Leaf(pgen2.token.RSQB, "]"),
                            ),
                        ],
                    ),
                    fixer_util.parenthesize(v),
                ),
                fixer_util.Newline(),
            ],
            prefix=indent,
        )

        # Add after original expression
        parent.insert_child(stmt_ix + 1, assign)

    # Remove residual ','/':'
    while cap["elems"].children:
        cap["elems"].children[0].remove()

    return node


Query(sys.argv[1]).select(
    """
    simple_stmt< expr_stmt<
        name=NAME '=' atom< '{' elems=dictsetmaker<any*> '}'>
    > any >
    """
).modify(large_dicts_to_assign).execute()

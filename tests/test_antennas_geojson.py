import pytest

from ska_telmodel.mccs.antennas_geojson import (
    ANTENNA_FEATURES_PREFIX,
    ANTENNA_FEATURES_VERSIONS,
    ANTENNA_GEOMETRY_PREFIX,
    ANTENNA_GEOMETRY_VERSIONS,
    ANTENNA_PREFIX,
    ANTENNA_PROPERTIES_PREFIX,
    ANTENNA_PROPERTIES_VERSIONS,
    ANTENNA_VERSIONS,
    check_antenna_interface_version,
)
from ska_telmodel.schema import example_by_uri, validate


def test_antenna_versions(caplog):
    assert (
        check_antenna_interface_version(ANTENNA_PREFIX + "0.7", ANTENNA_PREFIX)
        == "0.7"
    )

    invalid_prefix = "http://schexma.example.org/invalid"
    with pytest.raises(
        ValueError,
        match=f"ANTENNA interface URI '{invalid_prefix}' not allowed",
    ):
        check_antenna_interface_version(invalid_prefix)


def test_properties():
    for properties_uri in ANTENNA_PROPERTIES_VERSIONS:
        # Check normal case validates
        validate(properties_uri, example_by_uri(properties_uri), 2)

        # Check that an error is raised if 'receptors' is omitted
        location_erronious = example_by_uri(properties_uri)
        del location_erronious["station_id"]
        with pytest.raises(ValueError):
            validate(properties_uri, location_erronious, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(properties_uri), 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(ANTENNA_PROPERTIES_PREFIX + "9.9")


def test_geometry():
    for geometry_uri in ANTENNA_GEOMETRY_VERSIONS:
        # Check normal case validates
        validate(geometry_uri, example_by_uri(geometry_uri), 2)

        # Check that an error is raised if 'receptors' is omitted
        geometry_erronious = example_by_uri(geometry_uri)
        del geometry_erronious["type"]
        with pytest.raises(ValueError):
            validate(geometry_uri, geometry_erronious, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(geometry_uri), 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(ANTENNA_GEOMETRY_PREFIX + "9.9")


def test_features():
    for features_uri in ANTENNA_FEATURES_VERSIONS:
        # Check normal case validates
        validate(features_uri, example_by_uri(features_uri), 2)

        # Check that an error is raised if 'receptors' is omitted
        features_erronious = example_by_uri(features_uri)
        del features_erronious["type"]
        with pytest.raises(ValueError):
            validate(features_uri, features_erronious, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(features_uri), 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(ANTENNA_FEATURES_PREFIX + "9.9")


def test_antenna():
    for antenna_uri in ANTENNA_VERSIONS:
        # Check normal case validates
        validate(antenna_uri, example_by_uri(antenna_uri), 2)

        # Check that an error is raised if 'receptors' is omitted
        antenna_erronious = example_by_uri(antenna_uri)
        del antenna_erronious["type"]
        with pytest.raises(ValueError):
            validate(antenna_uri, antenna_erronious, 2)

        # Check validation when interface version is given as part of JSON
        validate(None, example_by_uri(antenna_uri), 1)

        # Check invalid version raises error
        with pytest.raises(ValueError, match=r"Could not generate example"):
            example_by_uri(ANTENNA_PREFIX + "9.9")

CSP scan 2.3
============

JSON schema and example for CSP Mid scan

.. ska-schema:: https://schema.skao.int/ska-csp-scan/2.3
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-csp-scan/2.3

       Example JSON

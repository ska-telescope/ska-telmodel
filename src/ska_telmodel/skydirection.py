"""
The ska_telmodel.skydirection module defines the SKA model for sky directions
in a variety of reference frames accepted by SKA.
"""
from __future__ import annotations

from inspect import cleandoc

from schema import And, Or, Schema

from ._common import TMSchema, mk_if


def _add_target_name(schema: TMSchema, description: str) -> None:
    """
    Add the target_name field to a Sky Direction schema.

    :param schema: The schema to mutate.
    """
    schema.add_field(
        name="target_name",
        check=str,
        description=cleandoc(description),
    )


def _add_reference_frame(
    schema: TMSchema, reference_frame: str, description: str
) -> None:
    """
    Add the reference_frame field to an ADR-63 Sky Direction schema.

    The field will be added with case-insensitive validation.

    :param schema: The schema to mutate.
    :param reference_frame: The reference frame (icrs, tle, etc.)
    :param description: description of reference frame
    """
    description += """
    
    Validation is case-insensitive.
    """  # noqa: W293

    schema.add_field(
        name="reference_frame",
        check=str,
        # ADR-63 says that schema should be lenient when parsing
        check_strict=And(
            str,
            # schema has a bug that prevents use of re.IGNORECASE, so compare
            # via casefold.
            lambda s: str.casefold(s) == reference_frame.casefold(),
        ),
        description=cleandoc(description),
    )


class ICRSSkyDirection:
    """
    A sky direction defined in the ICRS reference frame.
    """

    # We override __new__ rather than inherit from TMSchema as inheriting
    # directly from TMSchema causes issues with the sky direction union
    # schema. The symptoms are, when inheriting from TMSchema, that schema.py
    # runs this line...
    #
    #     data = Schema(dict, error=e).validate(data, **kwargs)
    #
    # which then raises an exception in TMSchema at the line below because the
    # dict type is not indexable (i.e., TMSchema expected _schema to be a dict
    # instance, not the dict type).
    #
    #      self._schema[name] = check
    #
    # As a workaround, we use TMSchema.new() to create the desired TMSchema
    # instance rather than calling super().__init__(). This workaround is
    # applied to all Sky Direction classes.

    def __new__(
        cls,
        *,
        name: str,
        version: str,
        strict: bool,
        **kwargs,
    ) -> TMSchema:
        schema = TMSchema.new(
            name=name,
            version=version,
            strict=strict,
            description="A sky direction defined in the ICRS reference frame.",
            **kwargs,
        )

        _add_target_name(
            schema,
            """
            A name for the target. If reference_frame is 'special' or 'tle'
            then the name might be of a solar system body or a satellite in
            Earth orbit.
            """,
        )

        _add_reference_frame(
            schema,
            "icrs",
            """
            Must be "icrs" for a sky direction defined in the ICRS reference
            frame.
            """,
        )

        schema.add_field(
            "attrs",
            cls.Attrs(name="", version=version, strict=strict),
        )

        return schema

    class Attrs:
        """
        Attributes for an ICRS sky direction.
        """

        def __new__(
            cls,
            *,
            name: str,
            version: str,
            strict: bool,
            **kwargs,
        ) -> TMSchema:
            schema = TMSchema.new(
                name=name,
                version=version,
                strict=strict,
                description="Attributes for an ICRS sky direction",
                **kwargs,
            )

            if_strict = mk_if(strict)

            schema.add_field(
                "c1",
                float,
                And(
                    float,
                    if_strict(lambda c1: 0 <= c1 < 360),
                ),
                description=cleandoc(
                    """
                    The longitude coordinate, specifying degrees right
                    ascension in the ICRS reference frame.

                    Value must be in the range 0 <= c1 < 360.
                    """
                ),
            )
            schema.add_field(
                "c2",
                float,
                And(
                    float,
                    if_strict(lambda c2: -90 <= c2 <= 90),
                ),
                description=cleandoc(
                    """
                    The latitude coordinate, specifying degrees declination
                    in the ICRS reference frame.

                    Value must be in the range -90 <= c2 <= 90.
                    """
                ),
            )
            schema.add_opt_field(
                "pm_c1",
                float,
                description=cleandoc(
                    """
                    Longitudinal proper motion of target, measured in
                    arcseconds per year.
                    """
                ),
                default=0.0,
            )
            schema.add_opt_field(
                "pm_c2",
                float,
                description=cleandoc(
                    """
                    Latitudinal proper motion of target, measured in
                    arcseconds per year.
                    """
                ),
                default=0.0,
            )
            schema.add_opt_field(
                "epoch",
                float,
                description=cleandoc(
                    """
                    Epoch of proper motion, giving the date when the proper
                    motion offset was zero.
                    """
                ),
                default=2000.0,
            )
            schema.add_opt_field(
                "parallax",
                float,
                description="The parallax in seconds of arc.",
                default=0.0,
            )
            schema.add_opt_field(
                "radial_velocity",
                float,
                description="Radial velocity along the line of sight in m/s.",
                default=0.0,
            )
            return schema


class AltAzSkyDirection:
    """
    A sky direction defined in the AltAz reference frame.
    """

    def __new__(
        cls,
        *,
        name: str,
        version: str,
        strict: bool,
        **kwargs,
    ) -> TMSchema:
        schema = TMSchema.new(
            name=name,
            version=version,
            strict=strict,
            description="A sky direction defined in the AltAz reference frame",
            **kwargs,
        )

        _add_target_name(
            schema,
            """
            A name for the target. If reference_frame is 'special' or 'tle'
            then the name might be of a solar system body or a satellite in
            Earth orbit.
            """,
        )

        _add_reference_frame(
            schema,
            "altaz",
            """
            Fixed as "altaz" for a sky direction defined in the AltAz
            reference frame.
            """,
        )

        schema.add_field(
            "attrs",
            cls.Attrs(name="", version=version, strict=strict),
        )

        return schema

    class Attrs:
        """
        Attributes for an AltAz sky direction.
        """

        def __new__(
            cls,
            *,
            name: str,
            version: str,
            strict: bool,
            **kwargs,
        ) -> TMSchema:
            schema = TMSchema.new(
                name=name,
                version=version,
                strict=strict,
                description="Allowed attributes for an AltAz sky direction",
                **kwargs,
            )
            if_strict = mk_if(strict)

            schema.add_field(
                "c1",
                float,
                And(
                    float,
                    if_strict(lambda c1: 0 <= c1 < 360),
                ),
                description=cleandoc(
                    """
                    The azimuth coordinate in degrees.

                    Value must be in the range 0 <= c1 < 360.
                    """
                ),
            )
            schema.add_field(
                "c2",
                float,
                And(
                    float,
                    if_strict(lambda c2: 0 <= c2 <= 90),
                ),
                description=cleandoc(
                    """
                    The elevation coordinate in degrees.

                    Value must be in the range 0 <= c2 <= 90.
                    """
                ),
            )
            return schema


class GalacticSkyDirection:
    """
    A sky direction defined in the Galactic reference frame.
    """

    def __new__(
        cls,
        *,
        name: str,
        version: str,
        strict: bool,
        **kwargs,
    ) -> TMSchema:
        schema = TMSchema.new(
            name=name,
            version=version,
            strict=strict,
            description=(
                "A sky direction defined in the Galactic reference frame."
            ),
            **kwargs,
        )

        _add_target_name(
            schema,
            """
            A name for the target. If reference_frame is 'special' or 'tle'
            then the name might be of a solar system body or a satellite in
            Earth orbit.
            """,
        )

        _add_reference_frame(
            schema,
            "galactic",
            """
            Must be "galactic" for a sky direction defined in the Galactic
            reference frame.
            """,
        )

        schema.add_field(
            "attrs",
            cls.Attrs(name="", version=version, strict=strict),
        )

        return schema

    class Attrs:
        """
        Attributes for a Galactic sky direction.
        """

        def __new__(
            cls,
            *,
            name: str,
            version: str,
            strict: bool,
            **kwargs,
        ) -> TMSchema:
            schema = TMSchema.new(
                name=name,
                version=version,
                strict=strict,
                description="Attributes for a Galactic sky direction",
                **kwargs,
            )
            if_strict = mk_if(strict)

            schema.add_field(
                "c1",
                float,
                And(
                    float,
                    if_strict(lambda c1: 0 <= c1 < 360),
                ),
                description=cleandoc(
                    """
                    The longitude coordinate, specifying degrees right
                    ascension in the Galactic reference frame.

                    Value must be in the range 0 <= c1 < 360.
                    """
                ),
            )
            schema.add_field(
                "c2",
                float,
                And(
                    float,
                    if_strict(lambda c2: -90 <= c2 <= 90),
                ),
                description=cleandoc(
                    """
                    The latitude coordinate, specifying degrees declination
                    in the Galactic reference frame.

                    Value must be in the range -90 <= c2 <= 90.
                    """
                ),
            )
            schema.add_opt_field(
                "pm_c1",
                float,
                description=cleandoc(
                    """
                    Longitudinal proper motion of target, measured in
                    arcseconds per year.
                    """
                ),
                default=0.0,
            )
            schema.add_opt_field(
                "pm_c2",
                float,
                description=cleandoc(
                    """
                    Latitudinal proper motion of target, measured in
                    arcseconds per year.
                    """
                ),
                default=0.0,
            )
            schema.add_opt_field(
                "epoch",
                float,
                description=cleandoc(
                    """
                    Epoch of proper motion, giving the date when the proper
                    motion offset was zero.
                    """
                ),
                default=2000.0,
            )
            schema.add_opt_field(
                "parallax",
                float,
                description="The parallax in seconds of arc.",
                default=0.0,
            )
            schema.add_opt_field(
                "radial_velocity",
                float,
                description="Radial velocity along the line of sight in m/s.",
                default=0.0,
            )
            return schema


class SpecialSkyDirection:
    """
    A sky direction whose coordinates are resolved at runtime via a Katpoint
    lookup.
    """

    def __new__(
        cls,
        *,
        name: str,
        version: str,
        strict: bool,
        **kwargs,
    ) -> TMSchema:
        schema = TMSchema.new(
            name=name,
            version=version,
            strict=strict,
            description=(
                "A sky direction whose coordinates are defined in Katpoint."
            ),
            **kwargs,
        )

        _add_target_name(
            schema,
            """
            A name for the target. This should be the name of a resolvable
            entity in Katpoint, for example, the name of a solar system body.
            """,
        )

        _add_reference_frame(
            schema,
            "special",
            """
            Must be "special" for a catalogue sky direction.
            """,
        )

        return schema


class TLESkyDirection:
    """
    A sky direction defined by a Two-Line Element (TLE) set.
    """

    def __new__(
        cls,
        *,
        name: str,
        version: str,
        strict: bool,
        **kwargs,
    ) -> TMSchema:
        schema = TMSchema.new(
            name=name,
            version=version,
            strict=strict,
            description=(
                "A sky direction defined by a Two-Line Element (TLE) set."
            ),
            **kwargs,
        )

        _add_target_name(
            schema,
            """
            A name for the target. For a TLE sky direction, this could be the
            name of a satellite in orbit.
            """,
        )

        _add_reference_frame(
            schema,
            "tle",
            """
            Must be "tle" for a sky direction defined in the TLE reference
            frame.
            """,
        )

        schema.add_field(
            "attrs",
            cls.Attrs(name="", version=version, strict=strict),
        )

        return schema

    class Attrs:
        """
        Attributes for a TLE sky direction.
        """

        def __new__(
            cls,
            *,
            name: str,
            version: str,
            strict: bool,
        ) -> TMSchema:
            schema = TMSchema.new(
                name=name,
                version=version,
                strict=strict,
                description="Attributes for a TLE sky direction.",
            )

            schema.add_field(
                "line1",
                str,
                description=(
                    "First line of a two-line element (TLE) sky direction."
                ),
            )
            schema.add_field(
                "line2",
                str,
                description=(
                    "Second line of a two-line element (TLE) sky direction."
                ),
            )

            return schema


def get_skydirection(
    version: str, strict: bool, as_reference: bool = False
) -> Schema:
    """
    Get the schema for a JSON object representing a union of the sky direction
    formats supported by SKA.

    This schema is intended to be compliant with ADR-63 and can be reused
    wherever an SKA-standard sky direction is required.

    Note that this is a generic interface. Subsystem documentation should be
    consulted to determine which reference frames and optional fields are
    supported by the implementing subsystem.

    :param version: interface version URI
    :param strict: schema strictness
    :param as_reference: define schema as a subschema. Defaults to False.
    """
    kwargs = dict(version=version, strict=strict, as_reference=as_reference)

    return Schema(
        name="Sky direction",
        description=cleandoc(
            """
            A union of the various standard sky direction representations
            supported by SKA, as defined in ADR-63.
            """
        ),
        schema=Or(
            ICRSSkyDirection(name="ICRS sky direction", **kwargs),
            AltAzSkyDirection(name="AltAz sky direction", **kwargs),
            GalacticSkyDirection(name="Galactic sky direction", **kwargs),
            SpecialSkyDirection(name="Special sky direction", **kwargs),
            TLESkyDirection(name="Two-Line Element sky direction", **kwargs),
            # we cannot use XOR validation as it would require a
            # schema.reset() after validation. If we can resolve the problems
            # with subclassing TMSchema then this might become an option again.
            # only_one=True,
        ),
        as_reference=as_reference,
    )

CSP scan 3.2
============

JSON schema and example for CSP Low scan

.. ska-schema:: https://schema.skao.int/ska-low-csp-scan/3.2
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-scan/3.2

        Example JSON (LOW CSP scan JSON v. 3.2)
"""Description of delay model for an SKA antennas configuration in
    geojson format.

    All information required to determine the configuration of an antenna in
    geojson format should be held in this JSON structure.

 """
import copy
from typing import List, Union

from ska_telmodel._common import TMSchema, split_interface_version

STATION_PREFIX = "https://schema.skao.int/ska-telmodel-station/"
STATION_FEATURES_PREFIX = (
    "https://schema.skao.int/ska-telmodel-station-features/"
)
STATION_PROPERTIES_PREFIX = (
    "https://schema.skao.int/ska-telmodel-station-features-properties/"
)
STATION_GEOMETRY_PREFIX = (
    "https://schema.skao.int/ska-telmodel-station-features-geometry/"
)

# first version with minimal functionality for basic testing
STATION_VER1_0 = STATION_PREFIX + "1.0"
STATION_FEATURES_VER1_0 = STATION_FEATURES_PREFIX + "1.0"
STATION_PROPERTIES_VER1_0 = STATION_PROPERTIES_PREFIX + "1.0"
STATION_GEOMETRY_VER1_0 = STATION_GEOMETRY_PREFIX + "1.0"

STATION_VERSIONS = sorted(
    [STATION_VER1_0],
    key=split_interface_version,
)
STATION_FEATURES_VERSIONS = sorted(
    [STATION_FEATURES_VER1_0],
    key=split_interface_version,
)

STATION_PROPERTIES_VERSIONS = sorted(
    [STATION_PROPERTIES_VER1_0],
    key=split_interface_version,
)

STATION_GEOMETRY_VERSIONS = sorted(
    [STATION_GEOMETRY_VER1_0],
    key=split_interface_version,
)


_ALLOWED_URI_PREFIXES = [
    STATION_PREFIX,
    STATION_FEATURES_PREFIX,
    STATION_PROPERTIES_PREFIX,
    STATION_GEOMETRY_PREFIX,
]

# examples
STATION_PROPERTIES_EXAMPLE_1_0 = {
    "interface": (
        "https://schema.skao.int/ska-telmodel-station-features-properties/1.0"
    ),
    "name": "Station 1",
    "nof_antennas": 256,
    "antenna_type": "EDA2",
    "tpms": {
        "0": 1,
        "1": 2,
        "2": 3,
        "3": 4,
    },
    "station_num": 1,
}

STATION_GEOMETRY_EXAMPLE_1_0 = {
    "interface": (
        "https://schema.skao.int/ska-telmodel-station-features-geometry/1.0"
    ),
    "type": "Point",
    "coordinates": [1.5, 6.2],
}

STATION_FEATURES_EXAMPLE_1_0 = {
    "interface": ("https://schema.skao.int/ska-telmodel-station-features/1.0"),
    "type": "Feature",
    "properties": STATION_PROPERTIES_EXAMPLE_1_0,
    "geometry": STATION_GEOMETRY_EXAMPLE_1_0,
}

STATION_EXAMPLE_1_0 = {
    "interface": ("https://schema.skao.int/ska-telmodel-station/1.0"),
    "type": "FeatureCollection",
    "name": "station_export_w2",
    "features": [STATION_FEATURES_EXAMPLE_1_0, STATION_FEATURES_EXAMPLE_1_0],
}


def check_station_interface_version(
    version: str,
    allowed_prefixes: Union[str, List[str]] = _ALLOWED_URI_PREFIXES,
) -> str:
    """
    Check station interface version.

    Checks that the interface URI has one of the allowed prefixes. If it does,
    the version number is returned. If not, a ValueError exception is raised.

    :param version: station interface URI
    :param allowed_prefixes: allowed URI prefix(es)
    :returns: version number

    """
    if not isinstance(allowed_prefixes, list):
        allowed_prefixes = [allowed_prefixes]

    # Valid?
    for prefix in allowed_prefixes:
        if version.startswith(prefix):
            number = version[len(prefix) :]
            return number

    raise ValueError(f"STATION interface URI '{version}' not allowed")


def get_station_properties_schema(version: str, strict: bool):
    """
    Properties of the station
    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the station properties.
    """
    check_station_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Properties",
        version,
        strict,
        description="The properties of the station",
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field("name", str, description="name of station")
    schema.add_field(
        "nof_antennas", int, description="number of antennas on station"
    )
    schema.add_field("antenna_type", str, description="type of antenna")
    schema.add_field("tpms", dict[int], description="tiles")
    schema.add_field("station_num", int, description="station number")

    return schema


def get_station_geometry_schema(version: str, strict: bool):
    """

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the geometry for the station.
    """
    check_station_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Geometry - type, coordinates",
        version,
        strict,
        description="Postion of the station.",
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field("type", str, description="Coordinate type")
    schema.add_field(
        "coordinates", [float], description="Array of coordinates"
    )

    return schema


def get_station_features_schema(version: str, strict: bool):
    """

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for features.

    """
    check_station_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Features",
        version,
        strict,
        description="Features of the station.",
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field("type", str, description="Type")
    schema.add_field(
        "properties",
        get_station_properties_schema(version, strict),
        description="station properties",
    )
    schema.add_field(
        "geometry",
        get_station_geometry_schema(version, strict),
        description="station geometry",
    )

    return schema


def get_station_schema(version: str, strict: bool):
    """

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for station.

    """
    check_station_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "stations",
        version,
        strict,
        description="Configuration data for stations stored in geojson format",
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field("type", str, description="Type")
    schema.add_field("name", str, description="Name")
    schema.add_field(
        "features",
        [get_station_features_schema(version, strict)],
        description="Features",
    )

    return schema


def get_station_example(version: str) -> dict:
    """Generate example of station argument

    :param version: Version URI of configuration format
    """

    version_number = check_station_interface_version(version, STATION_PREFIX)

    if version_number == "1.0":
        scan_example = copy.deepcopy(STATION_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_station_features_example(version: str) -> dict:
    """Generate example of the features argument

    :param version: Version URI of configuration format
    """

    version_number = check_station_interface_version(
        version, STATION_FEATURES_PREFIX
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(STATION_FEATURES_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_station_properties_example(version: str) -> dict:
    """Generate example of the properties argument

    :param version: Version URI of configuration format
    """

    version_number = check_station_interface_version(
        version, STATION_PROPERTIES_PREFIX
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(STATION_PROPERTIES_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_station_geometry_example(version: str) -> dict:
    """Generate example of a valid geometry argument

    :param version: Version URI of configuration format
    """

    version_number = check_station_interface_version(
        version, STATION_GEOMETRY_PREFIX
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(STATION_GEOMETRY_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")

import pytest

from ska_telmodel.lowcbf.version import (
    LOWCBF_ASSIGNRESOURCES_PREFIX,
    LOWCBF_CONFIGURESCAN_PREFIX,
    LOWCBF_RELEASERESOURCES_PREFIX,
    LOWCBF_SCAN_PREFIX,
)
from ska_telmodel.schema import example_by_uri, validate

STRICT_ERRS = 2
PERMISSIVE_ERRS = 1
PERMISSIVE_WARN = 0

# list of all defined versions of the LowCBF schemas
IFACE_VERSIONS = ["0.1", "0.2", "0.3"]


def test_lowcbf_assignresources():
    # all versions of example 'assignresources' JSON should pass schema checks
    for version in IFACE_VERSIONS:
        assign_uri = LOWCBF_ASSIGNRESOURCES_PREFIX + version
        validate(assign_uri, example_by_uri(assign_uri), STRICT_ERRS)
    # example 'assignresources' JSON with missing field should raise error
    for version in IFACE_VERSIONS:
        assign_uri = LOWCBF_ASSIGNRESOURCES_PREFIX + version
        bad_example = example_by_uri(assign_uri)
        del bad_example["lowcbf"]
        with pytest.raises(ValueError):
            validate(assign_uri, bad_example, STRICT_ERRS)
    # Check invalid schema version raises error
    # (v0.0 will not exist, first recommended semantic version is v0.1)
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LOWCBF_ASSIGNRESOURCES_PREFIX + "0.0")


def test_lowcbf_configurescan():
    # all versions of example 'configurescan' JSON should pass schema checks
    # added test for version 1.0 (nakshatra changes)
    for version in IFACE_VERSIONS + ["1.0"]:
        configure_uri = LOWCBF_CONFIGURESCAN_PREFIX + version
        validate(configure_uri, example_by_uri(configure_uri), STRICT_ERRS)
    # example 'configurescan' JSON with missing field should raise error
    for version in IFACE_VERSIONS:
        configure_uri = LOWCBF_CONFIGURESCAN_PREFIX + version
        bad_example = example_by_uri(configure_uri)
        del bad_example["lowcbf"]
        with pytest.raises(ValueError):
            validate(configure_uri, bad_example, STRICT_ERRS)
    # Check invalid schema version raises error (v0.0 will not exist)
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LOWCBF_CONFIGURESCAN_PREFIX + "0.0")


def test_lowcbf_releaseresources():
    # all versions of example 'releaseresources' JSON should pass schema checks
    for version in IFACE_VERSIONS:
        release_uri = LOWCBF_RELEASERESOURCES_PREFIX + version
        validate(release_uri, example_by_uri(release_uri), STRICT_ERRS)
    # example JSON with missing field should raise error
    for version in IFACE_VERSIONS:
        release_uri = LOWCBF_RELEASERESOURCES_PREFIX + version
        bad_example = example_by_uri(release_uri)
        del bad_example["lowcbf"]
        with pytest.raises(ValueError):
            validate(release_uri, bad_example, STRICT_ERRS)
    # Check invalid schema version raises error (v0.0 will not exist)
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LOWCBF_RELEASERESOURCES_PREFIX + "0.0")


def test_lowcbf_scan():
    # all versions of example 'scan' JSON should pass their schema checks
    for version in IFACE_VERSIONS:
        scan_uri = LOWCBF_SCAN_PREFIX + version
        validate(scan_uri, example_by_uri(scan_uri), STRICT_ERRS)
    # example 'scan' JSON with missing field should raise error
    for version in IFACE_VERSIONS:
        scan_uri = LOWCBF_SCAN_PREFIX + version
        bad_example = example_by_uri(scan_uri)
        del bad_example["lowcbf"]
        with pytest.raises(ValueError):
            validate(scan_uri, bad_example, STRICT_ERRS)
    # Check invalid schema version raises error (v0.0 will not exist)
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LOWCBF_SCAN_PREFIX + "0.0")

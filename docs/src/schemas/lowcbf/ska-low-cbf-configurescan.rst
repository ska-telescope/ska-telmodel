ska-low-cbf-configurescan
=========================
.. ska-schema:: https://schema.skao.int/ska-low-cbf-configurescan/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-cbf-configurescan/1.0

       Example (Low CBF Configuration JSON including Nakshatra work)

.. ska-schema:: https://schema.skao.int/ska-low-cbf-configurescan/0.2
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-cbf-configurescan/0.2

       Example JSON

.. ska-schema:: https://schema.skao.int/ska-low-cbf-configurescan/0.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:


   .. ska-schema-example:: https://schema.skao.int/ska-low-cbf-configurescan/0.1

       Example JSON


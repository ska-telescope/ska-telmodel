import os
import shutil
import tempfile
from collections import namedtuple
from pathlib import Path
from unittest.mock import MagicMock, call, patch

from pytest import fixture, raises

from ska_telmodel.data.cache import cache_path
from ska_telmodel.data.new_data_backend import GitBackend

# pylint: disable=no-member

SKA_HOME_DIR = cache_path(name="git_repos", env=None)


@fixture(autouse=True)
def always_do_cleanup():
    os.makedirs(SKA_HOME_DIR / "tmdata", exist_ok=True)
    yield
    try:
        shutil.rmtree(SKA_HOME_DIR)
    except Exception:
        pass


class FakeHead:
    def __init__(self, name="main"):
        self.called = False
        self.name = name

    def __str__(self):
        return self.name

    def checkout(self):
        self.called = True


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_init_ssh_no_location_exists(mock_repo):
    mock_repo_obj = MagicMock()
    mock_repo.clone_from.return_value = mock_repo_obj
    mock_repo_obj.heads = [FakeHead()]
    backend = GitBackend("ssh://host.z:repo")

    assert backend._uri == "host.z:repo"
    assert (
        str(backend._checkout_location.relative_to(SKA_HOME_DIR))
        == "bcbf3f89-d76a-0c16-db08-7e6518b5b00a"
    )

    mock_repo.clone_from.assert_called_once()
    mock_repo_obj.remotes.origin.fetch.assert_called_once()
    mock_repo_obj.remotes.origin.pull.assert_called_once()
    assert mock_repo_obj.heads[0].called is True


@patch("ska_telmodel.data.new_data_backend.cache_path")
@patch("ska_telmodel.data.new_data_backend.Repo")
def test_init_https_given_location_exists(mock_repo, mock_cache):
    with tempfile.TemporaryDirectory() as tmp_dir:
        mock_cache.return_value = Path(tmp_dir) / "git_repos"
        repo_path = (
            Path(tmp_dir)
            / "git_repos"
            / "a4079355-1e56-7ff0-160e-f39b05dbd547"
        )

        repo_path.mkdir(parents=True, exist_ok=True)
        mock_repo_obj = MagicMock()
        mock_repo.return_value = mock_repo_obj
        mock_repo_obj.heads = [FakeHead()]

        backend = GitBackend(
            "https://host.z/repo.git",
        )

    assert backend._uri == "https://host.z/repo.git"
    assert str(backend.local_location()) == str(
        Path(tmp_dir) / "git_repos" / "a4079355-1e56-7ff0-160e-f39b05dbd547"
    )

    assert mock_repo.mock_calls == [
        call(repo_path),
        call().remotes.origin.fetch(),
        call().remotes.origin.pull(),
    ]

    assert mock_repo.clone_from.called is False
    assert mock_repo_obj.heads[0].called is True


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_init_no_schema_no_location_not_exist(mock_repo):
    mock_repo_obj = MagicMock()
    mock_repo.clone_from.return_value = mock_repo_obj
    mock_repo_obj.heads = [FakeHead("master")]
    backend = GitBackend("repo")

    assert backend._uri == "git@gitlab.com:repo.git"
    assert (
        str(backend._checkout_location.relative_to(SKA_HOME_DIR))
        == "725948d3-6382-4b15-4afe-ea6837b6a883"
    )

    mock_repo.clone_from.assert_called_once()
    mock_repo_obj.remotes.origin.fetch.assert_called_once()
    mock_repo_obj.remotes.origin.pull.assert_called_once()
    assert mock_repo_obj.heads[0].called is True


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_start(mock_repo):
    branch = "branch_name"
    master, new_branch = FakeHead("master"), FakeHead(branch)
    mock_repo.clone_from.return_value.heads = [master, new_branch]
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    backend.start_transaction(branch)

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().remotes.origin.pull(),
        # Create new branch
        call.clone_from().create_head(branch),
        call.clone_from().git.push("--set-upstream", "origin", branch),
        call.clone_from().remotes.origin.pull(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_commit(mock_repo):
    backend = GitBackend("repo")
    mock_repo.clone_from.return_value.is_dirty.return_value = False
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    backend.commit_transaction()

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
        # Check dirty state
        call.clone_from().is_dirty(),
        call.clone_from().index.diff(),
        call.clone_from().index.diff().__iter__(),
        call.clone_from().index.diff("HEAD"),
        call.clone_from().index.diff().__iter__(),
        call.clone_from().untracked_files.__len__(),
        # Push the branch
        call.clone_from().remotes.origin.push(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_commit_dirty_repo(mock_repo):
    backend = GitBackend("repo")

    backend._repo.is_dirty.return_value = True
    backend._repo.untracked_files = ["a/uncommitted/file.json"]
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"

    with raises(ValueError) as err:
        backend.commit_transaction()

    assert str(err.value) == "Uncommitted files: a/uncommitted/file.json"

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
        # Check dirty state
        call.clone_from().is_dirty(),
        call.clone_from().index.diff(),
        call.clone_from().index.diff().__iter__(),
        call.clone_from().index.diff("HEAD"),
        call.clone_from().index.diff().__iter__(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_add_directory(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve() / "tests" / "upload_backend_test_data" / "dir_test"
    )

    with raises(ValueError) as err:
        backend.add_data(path_dir)

    assert str(err.value) == "Validation Error"
    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_add_file_no_key(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "dir_test"
        / "parent1"
        / "parent2"
        / "file.json"
    )

    with raises(ValueError) as err:
        backend.add_data(path_dir)

    assert str(err.value) == "`key` must be specified with a file"
    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_add_file_validate_fail(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "dir_test"
        / "parent1"
        / "parent2"
        / "file.json"
    )

    with raises(ValueError) as err:
        backend.add_data(path_dir, "new/path/file.json")

    assert str(err.value) == "Validation Error"
    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_add_file_file_doesnt_exist(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "missing.json"
    )

    with raises(ValueError) as err:
        backend.add_data(path_dir, "new/path/file.json")

    assert str(err.value) == "Path doesn't exist"
    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_add_file_sucess(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "mid-layout.json"
    )

    backend.add_data(path_dir, "new/path/file.json")

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
        call.clone_from().index.add(
            [Path("tmdata") / "new" / "path" / "file.json"]
        ),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_add_file_dir_success(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "dir_test_valid"
    )

    backend.add_data(path_dir)

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
        call.clone_from().index.add(
            [Path("tmdata") / "parent1" / "parent2" / "mid-layout.json"]
        ),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_add_file_dir_partial_success(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "dir_test_partial"
    )

    backend.add_data(path_dir, "service/component")

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
        call.clone_from().index.add(
            [Path("tmdata") / "service" / "component" / "mid-layout.json"]
        ),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_status(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"

    File = namedtuple("File", "a_path")

    def dirty_function(inp: str = None):
        if inp is None:
            return [File("a/uncommitted/file.json")]
        else:
            return [File("a/staged/file.yaml")]

    backend._repo.is_dirty.return_value = True
    backend._repo.index.diff = MagicMock(side_effect=dirty_function)
    backend._repo.untracked_files = ["a/new/file.json"]

    state = backend.status()

    assert state == {
        "is_dirty": True,
        "uncommitted": ["a/uncommitted/file.json"],
        "staged": ["a/staged/file.yaml"],
        "untracked": ["a/new/file.json"],
    }

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
        call.clone_from().is_dirty(),
        call.clone_from().index.diff(),
        call.clone_from().index.diff("HEAD"),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_validate_no_interface(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "no_interface.json"
    )

    output = backend.validate(path_dir)

    assert output is True
    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_validate_failed(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "invalid_interface.json"
    )

    output = backend.validate(path_dir)

    assert output is False
    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_validate_sucess(mock_repo):
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "mid-layout.json"
    )

    output = backend.validate(path_dir)

    assert output is True
    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().heads.__iter__(),
        call.clone_from().remotes.origin.pull(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_basic_flow(mock_repo):
    master, new_branch = FakeHead("master"), FakeHead("new_temp_data")
    mock_repo.clone_from.return_value.heads = [master, new_branch]
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "dir_test_valid"
    )
    backend._repo.is_dirty.return_value = False

    backend.start_transaction("New Temp Data")
    backend.add_data(path_dir)
    backend.commit("New data")
    backend.commit_transaction()

    assert master.called
    assert new_branch.called

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().remotes.origin.pull(),
        call.clone_from().create_head("new_temp_data"),
        call.clone_from().git.push(
            "--set-upstream", "origin", "new_temp_data"
        ),
        call.clone_from().remotes.origin.pull(),
        call.clone_from().index.add(
            [Path("tmdata") / "parent1" / "parent2" / "mid-layout.json"]
        ),
        call.clone_from().index.commit("New data"),
        # is dirty checks
        call.clone_from().is_dirty(),
        call.clone_from().index.diff(),
        call.clone_from().index.diff().__iter__(),
        call.clone_from().index.diff("HEAD"),
        call.clone_from().index.diff().__iter__(),
        call.clone_from().untracked_files.__len__(),
        call.clone_from().remotes.origin.push(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_complicated_flow(mock_repo):
    branch = "branch_name"
    master, new_branch = FakeHead("master"), FakeHead(branch)
    mock_repo.clone_from.return_value.heads = [master, new_branch]
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    path_dir = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "dir_test_valid"
    )
    path_partial = (
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "dir_test_partial"
    )
    backend._repo.is_dirty.return_value = False

    backend.start_transaction(branch)
    backend.add_data(path_dir)
    backend.add_data(path_partial, "service/component")
    backend.add_data(
        Path(".").resolve()
        / "tests"
        / "upload_backend_test_data"
        / "mid-layout.json",
        "service/thing/name.json",
    )
    backend.commit("New data")
    backend.commit_transaction()

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().remotes.origin.pull(),
        call.clone_from().create_head(branch),
        call.clone_from().git.push("--set-upstream", "origin", branch),
        call.clone_from().remotes.origin.pull(),
        call.clone_from().index.add(
            [Path("tmdata") / "parent1" / "parent2" / "mid-layout.json"]
        ),
        call.clone_from().index.add(
            [Path("tmdata") / "service" / "component" / "mid-layout.json"]
        ),
        call.clone_from().index.add(
            [Path("tmdata") / "service" / "thing" / "name.json"]
        ),
        call.clone_from().index.commit("New data"),
        # is dirty checks
        call.clone_from().is_dirty(),
        call.clone_from().index.diff(),
        call.clone_from().index.diff().__iter__(),
        call.clone_from().index.diff("HEAD"),
        call.clone_from().index.diff().__iter__(),
        call.clone_from().untracked_files.__len__(),
        call.clone_from().remotes.origin.push(),
    ]


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_branch_not_existing(mock_repo):
    master = FakeHead("master")
    mock_repo.clone_from.return_value.heads = [master]
    backend = GitBackend("repo")
    with raises(ValueError) as err:
        backend.checkout_branch("unknown")

    assert str(err.value) == "No branch to checkout to could be found"


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_start_transaction_branch_exists(mock_repo):
    master, new_branch = FakeHead("master"), FakeHead("new_branch")
    mock_repo.clone_from.return_value.heads = [master, new_branch]
    mock_repo.clone_from.return_value.create_head.side_effect = OSError(
        "'new_branch' does already exist"
    )
    backend = GitBackend("repo")
    with raises(ValueError) as err:
        backend.start_transaction("new_branch")

    assert str(err.value) == "Branch Already Exists"


@patch("ska_telmodel.data.new_data_backend.Repo")
def test_start_transaction_use_existing_branch(mock_repo):
    branch = "branch_name"
    master, new_branch = FakeHead("master"), FakeHead(branch)
    mock_repo.clone_from.return_value.heads = [master, new_branch]
    backend = GitBackend("repo")
    path = SKA_HOME_DIR / "725948d3-6382-4b15-4afe-ea6837b6a883"
    backend.start_transaction(branch, create_new_branch=False)

    assert mock_repo.mock_calls == [
        # Create checkout
        call.clone_from("git@gitlab.com:repo.git", path),
        # Checkout main (from checkout)
        call.clone_from().remotes.origin.fetch(),
        call.clone_from().remotes.origin.pull(),
        # Create new branch
        call.clone_from().git.push("--set-upstream", "origin", branch),
        call.clone_from().remotes.origin.pull(),
    ]

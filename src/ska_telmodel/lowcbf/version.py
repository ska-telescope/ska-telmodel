"""
This file defines the URI stem for each LOWCBF subarray command schema
and the functions to append major, minor version indicators to the schema URI
given, as a list, the [major, minor] version integers
"""

from functools import partial

from .._common import interface_uri

LOWCBF_ASSIGNRESOURCES_PREFIX = (
    "https://schema.skao.int/ska-low-cbf-assignresources/"
)
LOWCBF_CONFIGURESCAN_PREFIX = (
    "https://schema.skao.int/ska-low-cbf-configurescan/"
)
LOWCBF_SCAN_PREFIX = "https://schema.skao.int/ska-low-cbf-scan/"
LOWCBF_RELEASERESOURCES_PREFIX = (
    "https://schema.skao.int/ska-low-cbf-releaseresources/"
)


# Functions to create interface URI for each subarray command, each function
# taking a list argument containing major,minor version integers to be
# incorporated into the URI
lowcbf_assignresources_uri = partial(
    interface_uri, LOWCBF_ASSIGNRESOURCES_PREFIX
)
lowcbf_configurescan_uri = partial(interface_uri, LOWCBF_CONFIGURESCAN_PREFIX)
lowcbf_releaseresources_uri = partial(
    interface_uri, LOWCBF_RELEASERESOURCES_PREFIX
)
lowcbf_scan_uri = partial(interface_uri, LOWCBF_SCAN_PREFIX)

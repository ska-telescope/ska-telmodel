"""SDP schema 0.4 examples."""

import copy

from .v0_3 import get_sdp_configure, get_sdp_scan

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_recvaddrs",
    "get_sdp_releaseres",
    "get_sdp_scan",
]

SDP_ASSIGNRES = {
    "execution_block": {
        "eb_id": "eb-mvp01-20210623-00000",
        "max_length": 100.0,
        "context": {},
        "beams": [
            {
                "beam_id": "vis0",
                "function": "visibilities",
            },
            {
                "beam_id": "pss1",
                "search_beam_id": 1,
                "function": "pulsar search",
            },
            {
                "beam_id": "pss2",
                "search_beam_id": 2,
                "function": "pulsar search",
            },
            {
                "beam_id": "pst1",
                "timing_beam_id": 1,
                "function": "pulsar timing",
            },
            {
                "beam_id": "pst2",
                "timing_beam_id": 2,
                "function": "pulsar timing",
            },
            {
                "beam_id": "vlbi1",
                "vlbi_beam_id": 1,
                "function": "vlbi",
            },
        ],
        "scan_types": [
            {
                "scan_type_id": ".default",
                "beams": {
                    "vis0": {
                        "channels_id": "vis_channels",
                        "polarisations_id": "all",
                    },
                    "pss1": {
                        "field_id": "pss_field_0",
                        "channels_id": "pulsar_channels",
                        "polarisations_id": "all",
                    },
                    "pss2": {
                        "field_id": "pss_field_1",
                        "channels_id": "pulsar_channels",
                        "polarisations_id": "all",
                    },
                    "pst1": {
                        "field_id": "pst_field_0",
                        "channels_id": "pulsar_channels",
                        "polarisations_id": "all",
                    },
                    "pst2": {
                        "field_id": "pst_field_1",
                        "channels_id": "pulsar_channels",
                        "polarisations_id": "all",
                    },
                    "vlbi": {
                        "field_id": "vlbi_field",
                        "channels_id": "vlbi_channels",
                        "polarisations_id": "all",
                    },
                },
            },
            {
                "scan_type_id": "target:a",
                "derive_from": ".default",
                "beams": {"vis0": {"field_id": "field_a"}},
            },
        ],
        "channels": [
            {
                "channels_id": "vis_channels",
                "spectral_windows": [
                    {
                        "spectral_window_id": "fsp_1_channels",
                        "count": 744,
                        "start": 0,
                        "stride": 2,
                        "freq_min": 350000000.0,
                        "freq_max": 368000000.0,
                        "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                    },
                    {
                        "spectral_window_id": "fsp_2_channels",
                        "count": 744,
                        "start": 2000,
                        "stride": 1,
                        "freq_min": 360000000.0,
                        "freq_max": 368000000.0,
                        "link_map": [[2000, 4], [2200, 5]],
                    },
                    {
                        "spectral_window_id": "zoom_window_1",
                        "count": 744,
                        "start": 4000,
                        "stride": 1,
                        "freq_min": 360000000.0,
                        "freq_max": 361000000.0,
                        "link_map": [[4000, 6], [4200, 7]],
                    },
                ],
            },
            {
                "channels_id": "pulsar_channels",
                "spectral_windows": [
                    {
                        "spectral_window_id": "pulsar_fsp_channels",
                        "count": 744,
                        "start": 0,
                        "freq_min": 350000000.0,
                        "freq_max": 368000000.0,
                    }
                ],
            },
        ],
        "polarisations": [
            {"polarisations_id": "all", "corr_type": ["XX", "XY", "YY", "YX"]}
        ],
        "fields": [
            {
                "field_id": "field_a",
                "phase_dir": {
                    "ra": [123, 0.1],
                    "dec": [80, 0.1],
                    "reference_time": "...",
                    "reference_frame": "ICRF3",
                },
                "pointing_fqdn": "low-tmc/telstate/0/pointing",
            }
        ],
    },
    "processing_blocks": [
        {
            "pb_id": "pb-mvp01-20210623-00000",
            "sbi_ids": ["sbi-mvp01-20200325-00001"],
            "script": {
                "kind": "realtime",
                "name": "vis_receive",
                "version": "0.1.0",
            },
            "parameters": {},
        },
        {
            "pb_id": "pb-mvp01-20210623-00001",
            "sbi_ids": ["sbi-mvp01-20200325-00001"],
            "script": {
                "kind": "realtime",
                "name": "test_realtime",
                "version": "0.1.0",
            },
            "parameters": {},
        },
        {
            "pb_id": "pb-mvp01-20210623-00002",
            "sbi_ids": ["sbi-mvp01-20200325-00002"],
            "script": {"kind": "batch", "name": "ical", "version": "0.1.0"},
            "parameters": {},
            "dependencies": [
                {"pb_id": "pb-mvp01-20210623-00000", "kind": ["visibilities"]}
            ],
        },
        {
            "pb_id": "pb-mvp01-20210623-00003",
            "sbi_ids": [
                "sbi-mvp01-20200325-00001",
                "sbi-mvp01-20200325-00002",
            ],
            "script": {"kind": "batch", "name": "dpreb", "version": "0.1.0"},
            "parameters": {},
            "dependencies": [
                {"pb_id": "pb-mvp01-20210623-00002", "kind": ["calibration"]}
            ],
        },
    ],
}

EXTERNAL_RESOURCES = {
    # All permanent resources are grouped under a "resources" key
    # to make clear that they are permanent
    "resources": {
        # List of receptors (Low [sub]stations / Mid dishes). The example given
        # here is the four dishes making up Mid AA0.5.
        "receptors": ["SKA001", "SKA036", "SKA063", "SKA100"],
    }
}

SDP_ASSIGNRES = SDP_ASSIGNRES | EXTERNAL_RESOURCES
SDP_RELEASERES = EXTERNAL_RESOURCES

SDP_RECVADDRS = {
    "science": {
        "vis0": {
            "function": "visibilities",
            "host": [
                [0, "192.168.0.1"],
                [400, "192.168.0.2"],
                [744, "192.168.0.3"],
                [1144, "192.168.0.4"],
            ],
            "port": [
                [0, 9000, 1],
                [400, 9000, 1],
                [744, 9000, 1],
                [1144, 9000, 1],
            ],
            "mac": [[0, "06-00-00-00-00-00"], [744, "06-00-00-00-00-01"]],
            "delay_cal": [
                [0, "low-sdp/telstate/rcal0/delay0"],
                [400, "low-sdp/telstate/rcal0/delay1"],
                [744, "low-sdp/telstate/rcal0/delay2"],
                [1144, "low-sdp/telstate/rcal0/delay2"],
            ],
        },
        "pss1": {
            "function": "pulsar search",
            "search_beam_id": 1,
            "host": [[0, "192.168.60.0"]],
            "port": [[0, 8000]],
            "jones_cal": [
                [0, "low-sdp/telstate/rcal0/jones0"],
                [400, "low-sdp/telstate/rcal0/jones1"],
                [744, "low-sdp/telstate/rcal0/jones2"],
                [1144, "low-sdp/telstate/rcal0/jones2"],
            ],
        },
        "pss2": {
            "function": "pulsar search",
            "search_beam_id": 2,
            "host": [[0, "192.168.60.1"]],
            "port": [[0, 8000]],
            "jones_cal": [
                [0, "low-sdp/telstate/rcal0/jones0"],
                [400, "low-sdp/telstate/rcal0/jones1"],
                [744, "low-sdp/telstate/rcal0/jones2"],
                [1144, "low-sdp/telstate/rcal0/jones2"],
            ],
        },
        "pst1": {
            "function": "pulsar timing",
            "timing_beam_id": 1,
            "host": [[0, "192.168.60.2"]],
            "port": [[0, 8001]],
            "jones_cal": [
                [0, "low-sdp/telstate/rcal0/jones0"],
                [400, "low-sdp/telstate/rcal0/jones1"],
                [744, "low-sdp/telstate/rcal0/jones2"],
                [1144, "low-sdp/telstate/rcal0/jones2"],
            ],
        },
        "pst2": {
            "function": "pulsar timing",
            "timing_beam_id": 2,
            "host": [[0, "192.168.60.3"]],
            "port": [[0, 8002]],
            "jones_cal": [
                [0, "low-sdp/telstate/rcal0/jones0"],
                [400, "low-sdp/telstate/rcal0/jones1"],
                [744, "low-sdp/telstate/rcal0/jones2"],
                [1144, "low-sdp/telstate/rcal0/jones2"],
            ],
        },
    },
    "calibration": {
        "vis0": {
            "function": "visibilities",
            "host": [
                [0, "192.168.1.1"],
            ],
            "port": [
                [0, 9000, 1],
            ],
            "delay_cal": [
                [0, "low-sdp/telstate/rcal0/delay0"],
            ],
        },
        "pss1": {
            "function": "pulsar search",
            "search_beam_id": 1,
            "host": [[0, "192.168.60.0"]],
            "port": [[0, 8003]],
            "jones_cal": [
                [0, "low-sdp/telstate/rcal0/jones0"],
            ],
        },
        "pss2": {
            "function": "pulsar search",
            "search_beam_id": 2,
            "host": [[0, "192.168.60.1"]],
            "port": [[0, 8002]],
            "jones_cal": [
                [0, "low-sdp/telstate/rcal0/jones0"],
            ],
        },
        "pst1": {
            "function": "pulsar timing",
            "timing_beam_id": 0,
            "host": [[0, "192.168.60.2"]],
            "port": [[0, 8001]],
            "jones_cal": [
                [0, "low-sdp/telstate/rcal0/jones0"],
            ],
        },
        "pst2": {
            "function": "pulsar timing",
            "timing_beam_id": 1,
            "host": [[0, "192.168.60.3"]],
            "port": [[0, 8000]],
            "jones_cal": [
                [0, "low-sdp/telstate/rcal0/jones0"],
            ],
        },
    },
}


def get_sdp_assignres(version: str):
    return copy.deepcopy(SDP_ASSIGNRES)


def get_sdp_releaseres(version: str):
    return copy.deepcopy(SDP_RELEASERES)


def get_sdp_recvaddrs(version: str):
    return copy.deepcopy(SDP_RECVADDRS)

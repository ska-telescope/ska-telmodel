SKA Telescope Model
===================

.. image:: https://readthedocs.org/projects/ska-telescope-ska-telmodel/badge/?version=latest
   :target: https://ska-telescope-ska-telmodel.readthedocs.io/en/latest/?badge=latest
.. image:: https://gitlab.com/ska-telescope/telescope-model/badges/master/pipeline.svg
   :target: https://gitlab.com/ska-telescope/ska-telmodel/-/pipelines
.. image:: https://gitlab.com/ska-telescope/ska-telmodel/-/jobs/artifacts/master/raw/build/badges/tests_total.svg?job=create-ci-metrics
.. image:: https://gitlab.com/ska-telescope/ska-telmodel/-/jobs/artifacts/master/raw/build/badges/coverage.svg?job=create-ci-metrics

Library for retrieving and working with SKA Telescope Model
information.  What we are concerned with is enabling different SKA
sub-systems to agree about information - such as shared assumptions
about:

  * the physical location of telescope receptors (i.e. dishes or
    stations), or
  * configuration of the correlator and its connections to links, or
  * internal configuration templates for sub-systems

This sort of information evolves relatively slowly and is in many
cases too voluminous to be exchanged between systems in real time. On
the other hand, especially for information characterising knowledge
about the telescope, we will need to evolve it independently of the
software development lifecycle.

For this purpose, this library provides:

 * Means to access versioned telescope model data
 * Schemas to check whether telescope model data is valid
 * Ways for interpret and transform telescope model information

Installation
------------

Install using ``pip`` from the SKAO central artefact repository::

    pip install --extra-index-url https://artefact.skao.int/repository/pypi-internal/simple ska-telmodel

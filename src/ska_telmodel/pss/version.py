from ska_telmodel._common import split_interface_version

PSS_CONFIGURE_PREFIX = "https://schema.skao.int/ska-pss-configure/"

PSS_CONFIG_VER0_0 = PSS_CONFIGURE_PREFIX + "0.0"
PSS_CONFIG_VER0_1 = PSS_CONFIGURE_PREFIX + "0.1"
PSS_CONFIG_VER1_0 = PSS_CONFIGURE_PREFIX + "1.0"

PSS_CONFIG_VERSIONS = sorted(
    [PSS_CONFIG_VER0_1, PSS_CONFIG_VER1_0],
    key=split_interface_version,
)

JSON schema and example for Configure version 0.1
=================================================
.. ska-schema:: https://schema.skao.int/ska-pss-configure/0.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-pss-configure/0.1 pss_schema_example

      Example (PSS configuration for scan 0.1)
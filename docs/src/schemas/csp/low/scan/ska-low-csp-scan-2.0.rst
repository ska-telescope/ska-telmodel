CSP scan 2.0
============

JSON schema and example for CSP Low scan

.. ska-schema:: https://schema.skao.int/ska-low-csp-scan/2.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-scan/2.0

        Example JSON (LOW CSP scan JSON v. 2.0)
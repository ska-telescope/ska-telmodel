"""Description of delay model for an SKA receptor location.

    All information required to determine the delay of an antenna/station/dish
    should be held in this JSON structure.

    The information content is the location of the receptor in
    a known frame, any fixed delay known to be associated with the antenna
    due to cable lengths or electronics, and any fixed delay within the antenna
    associated with the feed geometry

    Other information is required to determine the delay. Namely the target,
    atmospheric delay and the Earth Orientation Parameters.
    These are currently held outside this
    structure and accessed independently of these aspects of the delay model.

 """
import copy
import logging
from inspect import cleandoc
from typing import List, Union

from ska_telmodel._common import TMSchema, split_interface_version

LAYOUT_PREFIX = "https://schema.skao.int/ska-telmodel-layout/"
RECEPTOR_PREFIX = "https://schema.skao.int/ska-telmodel-layout-receptor/"
LOCATION_PREFIX = "https://schema.skao.int/ska-telmodel-layout-location/"
LOCAL_LOCATION_PREFIX = (
    "https://schema.skao.int/ska-telmodel-layout-location-local/"
)
GEODETIC_LOCATION_PREFIX = (
    "https://schema.skao.int/ska-telmodel-layout-location-geodetic/"
)
GEOCENTRIC_LOCATION_PREFIX = (
    "https://schema.skao.int/ska-telmodel-layout-location-geocentric/"
)
FIXED_DELAY_PREFIX = (
    "https://schema.skao.int/ska-telmodel-layout-receptor-fixed-delay/"
)

# first version with minimal functionality for basic testing
LAYOUT_VER1_0 = LAYOUT_PREFIX + "1.0"
LAYOUT_VER1_1 = LAYOUT_PREFIX + "1.1"
FIXED_DELAY_VER1_0 = FIXED_DELAY_PREFIX + "1.0"
LOCATION_VER1_0 = LOCATION_PREFIX + "1.0"
LOCAL_LOCATION_VER1_0 = LOCAL_LOCATION_PREFIX + "1.0"
GEODETIC_LOCATION_VER1_0 = GEODETIC_LOCATION_PREFIX + "1.0"
GEOCENTRIC_LOCATION_VER1_0 = GEOCENTRIC_LOCATION_PREFIX + "1.0"
RECEPTOR_VER1_0 = RECEPTOR_PREFIX + "1.0"
RECEPTOR_VER1_1 = RECEPTOR_PREFIX + "1.1"

LAYOUT_VERSIONS = sorted(
    [LAYOUT_VER1_0, LAYOUT_VER1_1],
    key=split_interface_version,
)
FIXED_DELAY_VERSIONS = sorted(
    [FIXED_DELAY_VER1_0],
    key=split_interface_version,
)

LOCATION_VERSIONS = sorted(
    [LOCATION_VER1_0],
    key=split_interface_version,
)

LOCAL_LOCATION_VERSIONS = sorted(
    [LOCAL_LOCATION_VER1_0],
    key=split_interface_version,
)
GEODETIC_LOCATION_VERSIONS = sorted(
    [GEODETIC_LOCATION_VER1_0],
    key=split_interface_version,
)
GEOCENTRIC_LOCATION_VERSIONS = sorted(
    [GEOCENTRIC_LOCATION_VER1_0],
    key=split_interface_version,
)
RECEPTOR_VERSIONS = sorted(
    [RECEPTOR_VER1_0, RECEPTOR_VER1_1],
    key=split_interface_version,
)

_ALLOWED_URI_PREFIXES = [
    LAYOUT_PREFIX,
    LOCATION_PREFIX,
    LOCAL_LOCATION_PREFIX,
    GEODETIC_LOCATION_PREFIX,
    GEOCENTRIC_LOCATION_PREFIX,
    FIXED_DELAY_PREFIX,
    RECEPTOR_PREFIX,
]

_LOGGER = logging.getLogger(__name__)

# examples
FIXED_DELAY_EXAMPLE_1_0 = {
    "interface": FIXED_DELAY_VER1_0,
    "fixed_delay_id": "FIX_H",
    "polarisation": 0,
    "units": "m",
    "delay": 100.0,
}
GEOCENTRIC_EXAMPLE_1_0 = {
    "interface": GEOCENTRIC_LOCATION_VER1_0,
    "coordinate_frame": "ITRF",
    "x": -2563226.960308,
    "y": 5081884.949807,
    "z": -2878357.951618,
}
GEODETIC_EXAMPLE_1_0 = {
    "interface": GEODETIC_LOCATION_VER1_0,
    "coordinate_frame": "WGS84",
    "lat": 0.01,
    "lon": 0.01,
    "h": 1.0,
}
LOCAL_EXAMPLE_1_0 = {
    "interface": LOCAL_LOCATION_VER1_0,
    "coordinate_frame": "local",
    "east": 100.0,
    "north": 10.0,
    "up": 1.0,
    "reference": GEODETIC_EXAMPLE_1_0,
}
LOCATION_EXAMPLE_1_0 = {
    "interface": LOCATION_VER1_0,
    "geocentric": GEOCENTRIC_EXAMPLE_1_0,
    "geodetic": GEODETIC_EXAMPLE_1_0,
    "local": LOCAL_EXAMPLE_1_0,
}

RECEPTOR_EXAMPLE_1_0 = {
    "interface": RECEPTOR_VER1_0,
    "station_name": "FS001",
    "diameter": 38.0,
    "location": LOCATION_EXAMPLE_1_0,
    "fixed_delays": [FIXED_DELAY_EXAMPLE_1_0, FIXED_DELAY_EXAMPLE_1_0],
    "niao": 0.0,
}

RECEPTOR_EXAMPLE_1_1 = {
    "interface": RECEPTOR_VER1_1,
    "station_label": "FS001",
    "station_id": 1,
    "diameter": 38.0,
    "location": LOCATION_EXAMPLE_1_0,
    "fixed_delays": [FIXED_DELAY_EXAMPLE_1_0, FIXED_DELAY_EXAMPLE_1_0],
    "niao": 0.0,
}

LAYOUT_EXAMPLE_1_0 = {
    "interface": LAYOUT_VER1_0,
    "telescope": "ska1_low",
    "receptors": [RECEPTOR_EXAMPLE_1_0, RECEPTOR_EXAMPLE_1_0],
}

LAYOUT_EXAMPLE_1_1 = {
    "interface": LAYOUT_VER1_1,
    "telescope": "ska1_low",
    "receptors": [RECEPTOR_EXAMPLE_1_1, RECEPTOR_EXAMPLE_1_1],
}


def check_layout_interface_version(
    version: str,
    allowed_prefixes: Union[str, List[str]] = _ALLOWED_URI_PREFIXES,
) -> str:
    """
    Check Layout interface version.

    Checks that the interface URI has one of the allowed prefixes. If it does,
    the version number is returned. If not, a ValueError exception is raised.

    :param version: Layout interface URI
    :param allowed_prefixes: allowed URI prefix(es)
    :returns: version number

    """
    if not isinstance(allowed_prefixes, list):
        allowed_prefixes = [allowed_prefixes]

    # Valid?
    for prefix in allowed_prefixes:
        if version.startswith(prefix):
            number = version[len(prefix) :]
            return number

    raise ValueError(f"LAYOUT interface URI '{version}' not allowed")


def get_geodetic_position_schema(version: str, strict: bool):
    """
    Geodetic coordinate systems are based on a reference ellipsoid
    Typically the coordinates are geodetic latitude, longitude and height
    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the position in Geodetic coordinates.
    """
    check_layout_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Geodetic - lat,lon,h",
        version,
        strict,
        description=cleandoc(
            """
            Global Geodetic position schema, Geodetic coordinate systems
            are based on a reference ellipsoid the coordinates are
            geodetic latitude (rad), longitude (rad) and height (m).
            """
        ),
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field(
        "coordinate_frame",
        str,
        description="Coordinate frame or datum (e.g. ITRF or WGS84)",
    )
    schema.add_field("lat", float, description="Geodetic latitude (rad)")
    schema.add_field("lon", float, description="Geodetic longitude (rad)")
    schema.add_field("h", float, description="height (m)")

    return schema


def get_local_position_schema(version: str, strict: bool):
    """
    Local Geodetic coordinate systems are based on a reference ellipsoid
    and a geodetic reference position. They are generally specified in
    East (E), North (N), and Up (U)

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the position in Geodetic coordinates.
    """
    check_layout_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Local Geodetic - east, north, up",
        version,
        strict,
        description=cleandoc(
            """
            Local Geodetic position schema. Local Geodetic coordinate
            systems are based on a reference ellipsoid and a
            geodetic reference position. They are generally specified
            in East (E), North (N), and Up (U) in meters
            """
        ),
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field(
        "coordinate_frame",
        str,
        description="Coordinate frame or datum (e.g. ITRF or WGS84)",
    )
    schema.add_field("east", float, description="Local Geodetic East (m)")
    schema.add_field("north", float, description="Local Geodetic North (m)")
    schema.add_field("up", float, description="Local Geodetic Height (m)")
    schema.add_field(
        "reference",
        get_geodetic_position_schema(version, strict),
        description="The geodetic reference position",
    )
    return schema


def get_geocentric_position_schema(version: str, strict: bool):
    """
    A Earth Centred Earth Fixed 3 coordinate (geocentric) position
    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the position in ECEF.

    """
    check_layout_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "ECEF_XYZ",
        version,
        strict,
        description=cleandoc(
            """
            Earth Centred Earth Fixed - Geocentric position (x,y,z) in
            meters. The centre of the Earth is defined by a given frame,
            usually a particular realisation of ITRF.
            """
        ),
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field(
        "coordinate_frame",
        str,
        description="Coordinate frame for positions (ITRF)",
    )
    schema.add_field("x", float, description="ECEF X coordinate (m)")
    schema.add_field("y", float, description="ECEF Y coordinate (m)")
    schema.add_field("z", float, description="ECEF Z coordinate (m)")

    return schema


def get_location_schema(version: str, strict: bool):
    """
    This schema holds multiple possible position
    representations. Geocentric, Geodetic, and Local

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the position
    """
    check_layout_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Coordinate Locations",
        version,
        strict,
        description=cleandoc(
            """
            A representation of the receptor position. Multiple
            representations are supported.
            """
        ),
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field(
        "geocentric",
        get_geocentric_position_schema(version, strict),
        description="Geocentric Location",
    )
    schema.add_opt_field(
        "geodetic",
        get_geodetic_position_schema(version, strict),
        description="Geodetic location",
    )
    schema.add_opt_field(
        "local",
        get_local_position_schema(version, strict),
        description="Local Geodetic location",
    )

    return schema


def get_fixed_delay_schema(version: str, strict: bool):
    """
    A schema to represent a fixed delay
    in time or distance

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the delay
    """
    check_layout_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Fixed Delay",
        version,
        strict,
        description=cleandoc(
            """
            A fixed delay representation, these are delays that are
            fixed to the station, such as cable lengths, electronic
            delays. This is configured to be per polarisation and the
            delay model can contain multiple delays and they can be
            stored in length or time.
            """
        ),
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field(
        "fixed_delay_id", str, description="Identification for the delay"
    )
    schema.add_field(
        "polarisation",
        int,
        description="Which polarisation this delay is applied to",
    )
    schema.add_field(
        "units", str, description="Units for the delay (seconds, metres)"
    )

    schema.add_field("delay", float, description="The delay")

    return schema


def get_receptor_schema(version: str, strict: bool):
    """The receptor specific details
    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for calculating the delay.
    """
    check_layout_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Receptor",
        version,
        strict,
        description=cleandoc(
            """
            Identification, location and delay information
            for a receptor
            """
        ),
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")

    if split_interface_version(version) > (1, 0):
        schema.add_field(
            "station_label", str, description="Receptor or station label"
        )
        schema.add_field(
            "station_id", int, description="Receptor or station identifier"
        )
    else:
        schema.add_field(
            "station_name", str, description="Receptor or station label"
        )

    schema.add_field(
        "diameter",
        float,
        description="Receptor or station nominal diameter (m)",
    )

    schema.add_field(
        "location",
        get_location_schema(version, strict),
        description="Location of receptors coordinates",
    )

    schema.add_field(
        "fixed_delays",
        [get_fixed_delay_schema(version, strict)],
        description="Fixed delays",
    )
    schema.add_field(
        "niao",
        float,
        description="non-intersecting axis offset - between az and el axes",
    )

    return schema


def get_layout_schema(version: str, strict: bool):
    """Telescope layout schema with delay model specific items

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for calculating the delay.
    """
    check_layout_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Telescope Layout",
        version,
        strict,
        description=cleandoc(
            """
            Contains information required to populate a delay model
            used determine the relative delay between stations. Includes
            information such as station location, and fixed delays such
            as cable lengths.
            """
        ),
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field("telescope", str, description="SKA Telescope")

    schema.add_field(
        "receptors",
        [get_receptor_schema(version, strict)],
        description="Receptors",
    )

    return schema


def get_layout_example(version: str) -> dict:
    """Generate example of CSP scan argument

    :param version: Version URI of configuration format
    """

    version_number = check_layout_interface_version(version, LAYOUT_PREFIX)

    if version_number == "1.0":
        scan_example = copy.deepcopy(LAYOUT_EXAMPLE_1_0)
        return scan_example

    elif version_number == "1.1":
        scan_example = copy.deepcopy(LAYOUT_EXAMPLE_1_1)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_receptor_example(version: str) -> dict:
    """Generate example of the receptor argument

    :param version: Version URI of configuration format
    """

    version_number = check_layout_interface_version(
        version, _ALLOWED_URI_PREFIXES
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(RECEPTOR_EXAMPLE_1_0)
        return scan_example

    elif version_number == "1.1":
        scan_example = copy.deepcopy(RECEPTOR_EXAMPLE_1_1)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_fixed_delay_example(version: str) -> dict:
    """Generate example of the fixed delay argument

    :param version: Version URI of configuration format
    """

    version_number = check_layout_interface_version(
        version, _ALLOWED_URI_PREFIXES
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(FIXED_DELAY_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_location_example(version: str) -> dict:
    """Generate example of the local location argument

    :param version: Version URI of configuration format
    """

    version_number = check_layout_interface_version(version, LOCATION_PREFIX)

    if version_number == "1.0":
        scan_example = copy.deepcopy(LOCATION_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_local_position_example(version: str) -> dict:
    """Generate example of the local location argument

    :param version: Version URI of configuration format
    """

    version_number = check_layout_interface_version(
        version, _ALLOWED_URI_PREFIXES
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(LOCAL_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_geocentric_position_example(version: str) -> dict:
    """Generate example of the local location argument

    :param version: Version URI of configuration format
    """

    version_number = check_layout_interface_version(
        version, _ALLOWED_URI_PREFIXES
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(GEOCENTRIC_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_geodetic_position_example(version: str) -> dict:
    """Generate example of the local location argument

    :param version: Version URI of configuration format
    """

    version_number = check_layout_interface_version(
        version, _ALLOWED_URI_PREFIXES
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(GEODETIC_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


ska-telmodel-layout
===================

.. ska-schema:: https://schema.skao.int/ska-telmodel-layout/1.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-telmodel-layout/1.1

      Example


.. ska-schema:: https://schema.skao.int/ska-telmodel-layout/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-telmodel-layout/1.0

      Example


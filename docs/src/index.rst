

.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

SKA Telescope Model
===================

.. include:: ../../README.rst
   :start-line: 3

.. toctree::
   :maxdepth: 0

   ../../README.md
   ../../CHANGELOG.rst

.. toctree::
   :maxdepth: 1
   :caption: Usage

   ska-telmodel/tutorial
   ska-telmodel/usage
   ska-telmodel/cli
   ska-telmodel/environment_variables

.. toctree::
  :maxdepth: 1
  :caption: Development

  ska-telmodel/development
  ska-telmodel/guide
  ska-telmodel/internals

.. toctree::
  :maxdepth: 1
  :caption: Schemas


  schemas/csp/ska-csp
  schemas/pss/ska-pss
  schemas/pst/ska-pst
  schemas/lowcbf/ska-low-cbf
  schemas/mccs/ska-low-mccs
  schemas/midcbf/ska-mid-cbf
  schemas/sdp/ska-sdp
  schemas/tmc/ska-tmc
  schemas/ska-telmodel


"""Common elements of SDP schemas."""

from schema import Or, Regex, Schema

from .._common import get_unique_id_schema

# Regex for a station name in a target platform;
# e.g. the Low telescope, the Low-ITF, one of the Low PSIs,
# the Mid telescope, the Mid-ITF, one of the Mid PSIs, etc.
ALL_RECEPTORS = Regex(r"[a-zA-Z0-9]([-a-zA-Z0-9]*[a-zA-Z0-9])?")


def get_receptor_schema(strict: bool) -> Schema:
    """
    Return schema for receptors.

    :param strict: check names if set
    :return: schema
    """
    return ALL_RECEPTORS if strict else Schema(str)


def get_beam_function_pattern(strict: bool):
    """Get pattern for SDP beam functions

    As used for SDP configuration - i.e. basically a kind of data
    that the SKA SDP needs to receive.

    :return: A string pattern suitable for use in schemas
    """
    if strict:
        return Or(
            "visibilities",
            "pulsar search",
            "pulsar timing",
            "vlbi",
            "transient buffer",
        )
    else:
        return str


def get_sbi_name_schema(version: str, strict: bool) -> Schema:
    return get_unique_id_schema(strict, r"sbi")


def get_txn_name_schema(version: str, strict: bool) -> Schema:
    return get_unique_id_schema(strict, r"txn")


def get_eb_name_schema(version: str, strict: bool) -> Schema:
    return get_unique_id_schema(strict, r"eb")


def get_pb_name_schema(version: str, strict: bool) -> Schema:
    return get_unique_id_schema(strict, r"pb")

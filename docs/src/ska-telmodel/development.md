

Schema Development
------------------

The Telescope Model is developed jointly by all teams working on the
SKA telescope. To make this work, all changes will have to be tested
thoroughly and pass a code review via merge request.

Testing should ensure that all code paths are checked, i.e. we want to
reach 100% coverage. We also aim to minimise regressions of any
kind. This means that most code and data should be versioned *within*
the Telescope Model, with old behaviour staying supported until a
sufficient depreciation period has passed.

### Adding a new schema (version)

To add a new interface, you will have to adjust a number of places in
the library. For a new SKA interface `<interface>` with `<elem>` as
the leading sub-system, do the following steps:

1. Add this:
   ```
     <ELEM>_<INTERFACE>_PREFIX = "https://schema.skao.int/ska-<elem>-<interface>/"`
   ```
   to `src/ska_telmodel/<elem>/version.py`. This is the interface namespace URI.

2. Add a `get_<elem>_<interface>_schema(version: str, strict: bool)`
   function to `src/ska_telmodel/<elem>/schema.py`, returning an
   appropriate `Schema` object. Consult
   https://pypi.org/project/schema/ for how to write such
   schemas. Please add documentation as far as possible, this will be
   put both into the JSON schema as well as the documentation.

3. Adjust `schema_by_uri` in `src/ska_telmodel/schema.py` to call
   `get_<elem>_<interface>_schema` for schemas starting with
   `<ELEM>_<INTERFACE>_PREFIX` so that your schema can be found.

4. Add a documentation file `docs/src/ska_<elem>_<interface>.rst` with
   a line along the lines of
   ```
   .. ska-schema:: https://schema.skao.int/ska-<elem>-<interface>/<ver>
   ```
   to ensure documentation is generated

If you just want to add new schema version, skip steps (1) and (3) and
extend existing definitions in the remaining steps.

### Adding a new example

It is a good idea to always provide an up-to-date example for every
schema version. Assuming the schema is defined, the steps are fairly
similar:

2. Add a `get_<elem>_<interface>_example(version :str)` function to
   `src/ska_telmodel/<elem>/examples.py`, returning an `dict`. If you
   have multiple examples, you can add a `str` parameter to select the
   appropriate one.

3. Adjust `example_by_uri` in `src/ska_telmodel/schema.py` to call
   `get_<elem>_<interface>_example` for schemas starting with
   `<ELEM>_<INTERFACE>_PREFIX` so that your example can be found.

4. Add your example to `docs/src/ska_<elem>_<interface>.rst` by adding a line
   like
   ```
   .. ska-schema-example:: https://schema.skao.int/ska-<elem>-<interface>/<ver>
   ```
   inside the `.. ska-schema` block of the appropriate version

### Last steps

5. Import the newly added `<ELEM>_<INTERFACE>_PREFIX` from version,`get_<elem>_<interface>_schema`from schema and `get_<elem>_<interface>_example` from examples into `src/ska_telmodel/<elem>/__init__.py` file.

6. Finally add tests in `test_<elem>_schemas.py` to ensure test
   coverage. This is especially easy if you add an example to the
   schema (see above sub-section).


### Code Style

This project uses automated code formatting using the [Black Code
Formatter](https://github.com/psf/black),
[isort](https://pypi.org/project/isort/) as well as custom [bowler
refactoring rules](https://pybowler.io/docs/basics-setup) .

To ensure that all code is
formatted as required, run the following before you commit:

```
$ pip install black isort bowler # if needed
$ make python-format
```


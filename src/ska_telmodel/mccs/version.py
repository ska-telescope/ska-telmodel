from functools import partial

from .._common import interface_uri

MCCS_ASSIGNEDRES_PREFIX = (
    "https://schema.skatelescope.org/ska-low-mccs-assignedresources/"
)
MCCS_ASSIGNRES_PREFIX = (
    "https://schema.skatelescope.org/ska-low-mccs-assignresources/"
)
MCCS_CONFIGURE_PREFIX = (
    "https://schema.skatelescope.org/ska-low-mccs-configure/"
)
MCCS_SCAN_PREFIX = "https://schema.skatelescope.org/ska-low-mccs-scan/"
MCCS_RELEASERES_PREFIX = (
    "https://schema.skatelescope.org/ska-low-mccs-releaseresources/"
)

mccs_assignedres_uri = partial(interface_uri, MCCS_ASSIGNEDRES_PREFIX)
mccs_assignres_uri = partial(interface_uri, MCCS_ASSIGNRES_PREFIX)
mccs_configure_uri = partial(interface_uri, MCCS_CONFIGURE_PREFIX)
mccs_scan_uri = partial(interface_uri, MCCS_SCAN_PREFIX)
mccs_releaseres_uri = partial(interface_uri, MCCS_RELEASERES_PREFIX)

"""Description of delay model for an SKA antennas configuration in
    geojson format.

    All information required to determine the configuration of an antenna in
    geojson format should be held in this JSON structure.

 """
import copy
from typing import List, Union

from ska_telmodel._common import TMSchema, split_interface_version

ANTENNA_PREFIX = "https://schema.skao.int/ska-telmodel-antenna/"
ANTENNA_FEATURES_PREFIX = (
    "https://schema.skao.int/ska-telmodel-antenna-features/"
)
ANTENNA_PROPERTIES_PREFIX = (
    "https://schema.skao.int/ska-telmodel-antenna-features-properties/"
)
ANTENNA_GEOMETRY_PREFIX = (
    "https://schema.skao.int/ska-telmodel-antenna-features-geometry/"
)

# first version with minimal functionality for basic testing
ANTENNA_VER1_0 = ANTENNA_PREFIX + "1.0"
ANTENNA_FEATURES_VER1_0 = ANTENNA_FEATURES_PREFIX + "1.0"
ANTENNA_PROPERTIES_VER1_0 = ANTENNA_PROPERTIES_PREFIX + "1.0"
ANTENNA_GEOMETRY_VER1_0 = ANTENNA_GEOMETRY_PREFIX + "1.0"

ANTENNA_VERSIONS = sorted(
    [ANTENNA_VER1_0],
    key=split_interface_version,
)
ANTENNA_FEATURES_VERSIONS = sorted(
    [ANTENNA_FEATURES_VER1_0],
    key=split_interface_version,
)

ANTENNA_PROPERTIES_VERSIONS = sorted(
    [ANTENNA_PROPERTIES_VER1_0],
    key=split_interface_version,
)

ANTENNA_GEOMETRY_VERSIONS = sorted(
    [ANTENNA_GEOMETRY_VER1_0],
    key=split_interface_version,
)


_ALLOWED_URI_PREFIXES = [
    ANTENNA_PREFIX,
    ANTENNA_FEATURES_PREFIX,
    ANTENNA_PROPERTIES_PREFIX,
    ANTENNA_GEOMETRY_PREFIX,
]

# examples
ANTENNA_PROPERTIES_EXAMPLE_1_0 = {
    "interface": (
        "https://schema.skao.int/ska-telmodel-antenna-features-properties/1.0"
    ),
    "antenna_station_id": 0,
    "station_id": "object(534nfhwh2)",
    "x_pos": 6.1,
    "y_pos": 6.1,
    "z_pos": 6.1,
    "base_id": 1,
    "tpm_id": 1,
    "tpm_rx": 1,
    "status_x": "some status",
    "status_y": "some status",
    "tpm_name": "Tpm 1",
    "delay_x": 5,
    "delay_y": 5,
    "station_num": 1,
}

ANTENNA_GEOMETRY_EXAMPLE_1_0 = {
    "interface": (
        "https://schema.skao.int/ska-telmodel-antenna-features-geometry/1.0"
    ),
    "type": "Point",
    "coordinates": [1.5, 6.2],
}

ANTENNA_FEATURE_EXAMPLE_1_0 = {
    "interface": ("https://schema.skao.int/ska-telmodel-antenna-features/1.0"),
    "type": "Feature",
    "properties": ANTENNA_PROPERTIES_EXAMPLE_1_0,
    "geometry": ANTENNA_GEOMETRY_EXAMPLE_1_0,
}

ANTENNA_EXAMPLE_1_0 = {
    "interface": ("https://schema.skao.int/ska-telmodel-antenna/1.0"),
    "type": "FeatureCollection",
    "name": "antenna_export_w2",
    "features": [ANTENNA_FEATURE_EXAMPLE_1_0, ANTENNA_FEATURE_EXAMPLE_1_0],
}


def check_antenna_interface_version(
    version: str,
    allowed_prefixes: Union[str, List[str]] = _ALLOWED_URI_PREFIXES,
) -> str:
    """
    Check antenna interface version.

    Checks that the interface URI has one of the allowed prefixes. If it does,
    the version number is returned. If not, a ValueError exception is raised.

    :param version: antenna interface URI
    :param allowed_prefixes: allowed URI prefix(es)
    :returns: version number

    """
    if not isinstance(allowed_prefixes, list):
        allowed_prefixes = [allowed_prefixes]

    # Valid?
    for prefix in allowed_prefixes:
        if version.startswith(prefix):
            number = version[len(prefix) :]
            return number

    raise ValueError(f"ANTENNA interface URI '{version}' not allowed")


def get_antenna_properties_schema(version: str, strict: bool):
    """
    Properties of the antenna
    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the antenna properties.
    """
    check_antenna_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Properties",
        version,
        strict,
        description="The properties of the antenna",
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field(
        "antenna_station_id", int, description="Id of the antenna station"
    )
    schema.add_field("station_id", str, description="Id of the station")
    schema.add_field("x_pos", float, description="x position of the antenna")
    schema.add_field("y_pos", float, description="y position of the antenna")
    schema.add_field("z_pos", float, description="z position of the antenna")
    schema.add_field("base_id", int, description="base id")
    schema.add_field("tpm_id", int, description="Id of the TPM")
    schema.add_field("tpm_rx", int, description="TPM receiver")
    schema.add_field("status_x", str, description="Status x")
    schema.add_field("status_y", str, description="status y")
    schema.add_field("tpm_name", str, description="TPM name")
    schema.add_field("delay_x", int, description="delay in the x direction")
    schema.add_field("delay_y", int, description="delay in the y direction")
    schema.add_field("station_num", int, description="station number")

    return schema


def get_antenna_geometry_schema(version: str, strict: bool):
    """

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the geometry for the antenna.
    """
    check_antenna_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Geometry - type, coordinates",
        version,
        strict,
        description="Postion of the antenna.",
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field("type", str, description="Coordinate type")
    schema.add_field(
        "coordinates", [float], description="Array of coordinates"
    )

    return schema


def get_antenna_features_schema(version: str, strict: bool):
    """

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for features.

    """
    check_antenna_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Features",
        version,
        strict,
        description="Features of the antenna.",
        as_reference=True,
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field("type", str, description="Type")
    schema.add_field(
        "properties",
        get_antenna_properties_schema(version, strict),
        description="Antenna properties",
    )
    schema.add_field(
        "geometry",
        get_antenna_geometry_schema(version, strict),
        description="Antenna geometry",
    )

    return schema


def get_antenna_schema(version: str, strict: bool):
    """

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for antenna.

    """
    check_antenna_interface_version(version, _ALLOWED_URI_PREFIXES)
    schema = TMSchema.new(
        "Antennas",
        version,
        strict,
        description="Configuration data for antennas stored in geojson format",
    )
    schema.add_field("interface", str, description="Interface version")
    schema.add_field("type", str, description="Type")
    schema.add_field("name", str, description="Name")
    schema.add_field(
        "features",
        [get_antenna_features_schema(version, strict)],
        description="Features",
    )

    return schema


def get_antenna_example(version: str) -> dict:
    """Generate example of antenna argument

    :param version: Version URI of configuration format
    """

    version_number = check_antenna_interface_version(version, ANTENNA_PREFIX)

    if version_number == "1.0":
        scan_example = copy.deepcopy(ANTENNA_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_antenna_features_example(version: str) -> dict:
    """Generate example of the features argument

    :param version: Version URI of configuration format
    """

    version_number = check_antenna_interface_version(
        version, ANTENNA_FEATURES_PREFIX
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(ANTENNA_FEATURE_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_antenna_properties_example(version: str) -> dict:
    """Generate example of the properties argument

    :param version: Version URI of configuration format
    """

    version_number = check_antenna_interface_version(
        version, ANTENNA_PROPERTIES_PREFIX
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(ANTENNA_PROPERTIES_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")


def get_antenna_geometry_example(version: str) -> dict:
    """Generate example of a valid geometry argument

    :param version: Version URI of configuration format
    """

    version_number = check_antenna_interface_version(
        version, ANTENNA_GEOMETRY_PREFIX
    )

    if version_number == "1.0":
        scan_example = copy.deepcopy(ANTENNA_GEOMETRY_EXAMPLE_1_0)
        return scan_example

    raise ValueError(f"Could not generate example for schema {version}!")

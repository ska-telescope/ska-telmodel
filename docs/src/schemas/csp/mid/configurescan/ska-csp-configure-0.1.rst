CSP configurescan 0.1
=====================

JSON schema and example for CSP Mid configure

.. ska-schema:: https://schema.skatelescope.org/ska-csp-configure/0.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skatelescope.org/ska-csp-configure/0.1

       Example JSON (TMC input)

   .. ska-schema-example:: https://schema.skatelescope.org/ska-csp-configure/0.1 science_a

       Example JSON (CSP configuration for science_a scan)

   .. ska-schema-example:: https://schema.skatelescope.org/ska-csp-configure/0.1 cal_a

       Example JSON (CSP configuration for cal_a scan)


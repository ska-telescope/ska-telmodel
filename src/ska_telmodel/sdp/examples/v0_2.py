"""SDP schema 0.2 examples."""

import copy

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_recvaddrs",
    "get_sdp_scan",
]

SDP_ASSIGNRES = {
    "id": "sbi-mvp01-20200325-00001",
    "max_length": 100.0,
    "scan_types": [
        {
            "id": "science",
            "coordinate_system": "ICRS",
            "ra": "02:42:40.771",
            "dec": "-00:00:47.84",
            "channels": [
                {
                    "count": 744,
                    "start": 0,
                    "stride": 2,
                    "freq_min": 0.35e9,
                    "freq_max": 0.368e9,
                    "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                },
                {
                    "count": 744,
                    "start": 2000,
                    "stride": 1,
                    "freq_min": 0.36e9,
                    "freq_max": 0.368e9,
                    "link_map": [[2000, 4], [2200, 5]],
                },
            ],
        },
        {
            "id": "calibration",
            "coordinate_system": "ICRS",
            "ra": "12:29:06.699",
            "dec": "02:03:08.598",
            "channels": [
                {
                    "count": 744,
                    "start": 0,
                    "stride": 2,
                    "freq_min": 0.35e9,
                    "freq_max": 0.368e9,
                    "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                },
                {
                    "count": 744,
                    "start": 2000,
                    "stride": 1,
                    "freq_min": 0.36e9,
                    "freq_max": 0.368e9,
                    "link_map": [[2000, 4], [2200, 5]],
                },
            ],
        },
    ],
    "processing_blocks": [
        {
            "id": "pb-mvp01-20200325-00001",
            "workflow": {
                "type": "realtime",
                "id": "vis_receive",
                "version": "0.1.0",
            },
            "parameters": {},
        },
        {
            "id": "pb-mvp01-20200325-00002",
            "workflow": {
                "type": "realtime",
                "id": "test_realtime",
                "version": "0.1.0",
            },
            "parameters": {},
        },
        {
            "id": "pb-mvp01-20200325-00003",
            "workflow": {"type": "batch", "id": "ical", "version": "0.1.0"},
            "parameters": {},
            "dependencies": [
                {"pb_id": "pb-mvp01-20200325-00001", "type": ["visibilities"]}
            ],
        },
        {
            "id": "pb-mvp01-20200325-00004",
            "workflow": {"type": "batch", "id": "dpreb", "version": "0.1.0"},
            "parameters": {},
            "dependencies": [
                {"pb_id": "pb-mvp01-20200325-00003", "type": ["calibration"]}
            ],
        },
    ],
}

SDP_CONFIGURE = {"scan_type": "science"}

SDP_CONFIGURE_NEW_SCAN = {
    "new_scan_types": [
        {
            "id": "new_calibration",
            "channels": [
                {
                    "count": 372,
                    "start": 0,
                    "stride": 2,
                    "freq_min": 0.35e9,
                    "freq_max": 0.358e9,
                    "link_map": [[0, 0], [200, 1]],
                }
            ],
        }
    ],
    "scan_type": "new_calibration",
}

SDP_SCAN = {"id": 1}

SDP_RECVADDRS = {
    "science": {
        "host": [
            [0, "192.168.0.1"],
            [400, "192.168.0.2"],
            [744, "192.168.0.3"],
            [1144, "192.168.0.4"],
        ],
        "mac": [[0, "06-00-00-00-00-00"], [744, "06-00-00-00-00-01"]],
        "port": [
            [0, 9000, 1],
            [400, 9000, 1],
            [744, 9000, 1],
            [1144, 9000, 1],
        ],
    },
    "calibration": {"host": [[0, "192.168.1.1"]], "port": [[0, 9000, 1]]},
}


def get_sdp_assignres(version: str):
    return copy.deepcopy(SDP_ASSIGNRES)


def get_sdp_configure(version: str, scan_type: str = "science"):
    if scan_type == "new_calibration":
        return copy.deepcopy(SDP_CONFIGURE_NEW_SCAN)
    else:
        config = copy.deepcopy(SDP_CONFIGURE)
        config["scan_type"] = scan_type
        return config


def get_sdp_scan(version: str, scan_id: int = 1):
    scan = copy.deepcopy(SDP_SCAN)
    scan["id"] = scan_id
    return scan


def get_sdp_recvaddrs(version: str):
    return copy.deepcopy(SDP_RECVADDRS)

from .._common import split_interface_version

PSS_CONFIGURE_SCAN_0_1 = {
    "interface": "https://schema.skao.int/ska-pss-configure/0.1",
    "beam": [
        {
            "beam_id": 1,
            "reference_frame": "ICRS",
            "ra": 82.7500,
            "dec": 21.0000,
            "centre_frequency": 1400.0,
            "beam_delay_centre": 0.0,
            "dest_host": "192.168.178.25",
            "dest_port": 9021,
        },
        {
            "beam_id": 2,
            "reference_frame": "ICRS",
            "ra": 84.2500,
            "dec": 21.5000,
            "centre_frequency": 1400.0,
            "beam_delay_centre": 0.0,
            "dest_host": "192.168.178.26",
            "dest_port": 9021,
        },
    ],
}


PSS_CONFIGURE_SCAN_1_0 = {
    "interface": "https://schema.skao.int/ska-pss-configure/1.0",
    "beam": [
        {
            "beam_id": 1,
            "reference_frame": "ICRS",
            "ra": 82.7500,
            "dec": 21.0000,
            "centre_frequency": 1400.0,
            "beam_delay_centre": 0.0,
            "dest_host": "192.168.178.25",
            "dest_port": 9021,
        },
        {
            "beam_id": 2,
            "reference_frame": "ICRS",
            "ra": 84.2500,
            "dec": 21.5000,
            "centre_frequency": 1400.0,
            "beam_delay_centre": 0.0,
            "dest_host": "192.168.178.26",
            "dest_port": 9021,
        },
    ],
    "beams": [
        {
            "beam": {
                "active": True,
                "sinks": {
                    "channels": {
                        "sps_events": {
                            "active": True,
                            "sink": [
                                {"id": "spccl_files"},
                                {"id": "candidate_files"},
                            ],
                        }
                    },
                    "sink_configs": {
                        "spccl_files": {
                            "extension": ".spccl",
                            "dir": "/tmp",
                            "id": "spccl_files",
                        },
                        "spccl_sigproc_files": {
                            "spectra_per_file": 0,
                            "dir": "/tmp",
                            "extension": ".fil",
                            "candidate_window": {
                                "ms_before": 500.0,
                                "ms_after": 1000.0,
                            },
                            "id": "candidate_files",
                        },
                    },
                },
                "source": {
                    "sigproc": {
                        "file": """/raid/tvcache//SPS-MID_747e95f
                                _0.125_0.00125_2000.0_0.0_
                                Gaussian_50.0_0000_123123123.fil""",
                        "chunk_samples": 1024,
                        "default-nbits": 8,
                    }
                },
                "id": 1,
            }
        }
    ],
    "ddtr": {
        "astroaccelerate": {
            "active": False,
            "copy_dmtrials_to_host": True,
        },
        "cpu": {"active": False},
        "fpga": {"active": False},
        "gpu_bruteforce": {
            "active": False,
            "copy_dmtrials_to_host": True,
        },
        "klotski": {"active": False},
        "klotski_bruteforce": {"active": False},
        "dedispersion": [
            {"start": 0.0, "end": 100.0, "step": 0.1},
            {"start": 100.0, "end": 300.0, "step": 0.2},
            {"start": 300.0, "end": 700.0, "step": 0.4},
            {"start": 700.0, "end": 1500.0, "step": 0.8},
            {"start": 1500.0, "end": 3100.0, "step": 1.6},
        ],
    },
    "id": 1,
    "sps": {
        "cpu": {
            "active": False,
            "samples_per_iteration": 1,
            "number_of_widths": 1,
        },
        "threshold": 8.0,
        "klotski": {
            "active": False,
            "pulse_widths": [
                1,
                2,
                4,
                8,
                16,
                32,
                64,
                128,
                256,
                512,
                1024,
                2048,
                4096,
                8192,
                15000,
            ],
        },
        "klotski_bruteforce": {
            "active": False,
            "pulse_widths": [
                1,
                2,
                4,
                8,
                16,
                32,
                64,
                128,
                256,
                512,
                1024,
                2048,
                4096,
                8192,
                15000,
            ],
        },
    },
}


def get_pss_config_example(version: str, scan_type: str = None) -> dict:
    """Generate examples for PSS configuration strings"""

    (major, minor) = split_interface_version(version)

    if (major, minor) == (0, 1):
        return PSS_CONFIGURE_SCAN_0_1
    elif (major, minor) == (1, 0):
        return PSS_CONFIGURE_SCAN_1_0
    else:
        raise ValueError(
            f"Could not generate example for schema {version}"
            + f" and {scan_type=}!"
        )

Central Signal Processor Schemas
================================

Schemas used for commands for Mid and Low CSP LMC.

Some of these schemas are also used by Mid.CBF. See
:doc:`../midcbf/ska-mid-cbf` for details.

.. toctree::
  :maxdepth: 1
  :caption: Mid CSP Schemas

  ska-mid-csp <mid/ska-mid-csp>

.. toctree::
  :maxdepth: 1
  :caption: Low CSP Schemas

  ska-low-csp <low/ska-low-csp>
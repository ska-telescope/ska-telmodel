"""
SKA Low TMC schemas (beginning)
SKA Mid TMC schemas (bottom)
"""

from inspect import cleandoc

from schema import And, Literal, Schema

from ska_telmodel.csp.low_schema import (
    get_low_csp_assignresources_schema,
    get_low_csp_configure_schema,
)
from ska_telmodel.csp.low_version import (
    LOWCSP_ASSIGNRESOURCES_PREFIX,
    LOWCSP_CONFIGURE_PREFIX,
)
from ska_telmodel.csp.schema import get_csp_config_schema
from ska_telmodel.csp.version import CSP_CONFIG_PREFIX, CSP_CONFIGSCAN_PREFIX
from ska_telmodel.mccs.schema import get_mccs_assignres_schema

# first renamed this file to import
from ska_telmodel.sdp.schema import (
    get_sdp_assignres_schema,
    get_sdp_configure_schema,
)
from ska_telmodel.sdp.version import SDP_ASSIGNRES_PREFIX, SDP_CONFIGURE_PREFIX

from .._common import TMSchema, mk_if, split_interface_version
from ..mccs import validators as mccs_validators
from ..skydirection import get_skydirection
from .version import LOW_TMC_CONFIGURE_PREFIX, check_tmc_interface_version


def add_mccs_target_subschema(
    target_schema: TMSchema, version: str, strict: bool
) -> dict:
    """
    Get the mccs.target section of the Configure schema.

    v1.0 and v2.0 Configure schemas differ only in the JSON key names for the
    mccs.target field. This function factors out that difference, allowing the
    rest of the schema, which is identifical between v1 and v2, to be declared
    with a single definition. This function is intended to be called by
    get_tmc_confiure_schema.

    :param version: schema version
    :param strict: strictness level
    :return: dict suitable for insertion in a Schema
    """
    default_keynames = dict(
        reference_frame="reference_frame", target_name="target_name"
    )
    v1_0_keynames = dict(reference_frame="system", target_name="name")

    number = check_tmc_interface_version(version, LOW_TMC_CONFIGURE_PREFIX)
    if number == "1.0":
        keynames = v1_0_keynames
    else:
        keynames = default_keynames

    target_items = TMSchema.new("MCCS target", version=version, strict=strict)
    target_items.add_field(
        keynames["reference_frame"],
        str,
        description=cleandoc(
            """
            Co-ordinate system.

            Must be HORIZON for drift scan.
            """
        ),
    )
    target_items.add_field(
        keynames["reference_frame"],
        str,
        description=cleandoc(
            """
            Co-ordinate system.

            Must be HORIZON for drift scan.
            """
        ),
    )
    target_items.add_field(
        keynames["target_name"], str, description="Name of target."
    )
    target_items.add_field(
        "az", float, description="Pointing azimuth in degrees."
    )
    target_items.add_field(
        "el", float, description="Pointing elevation in degrees."
    )

    target_schema.add_opt_field(
        "target",
        target_items,
        description=cleandoc(
            """
            Target position for the sub-array beam.

            Only drift scan targets are currently implemented by MCCS,
            hence only azimuth and elevation are specified.
            """
        ),
    )


def get_low_csp_assignres_schema(version: str, strict: bool) -> TMSchema:
    """
    Build the CSP block of assignresources JSON schema for the OSO-TMC
    interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema for assignresources command.
    """
    csp_schema = TMSchema.new("LOWCSP assign resources", version, strict)
    # PST schema
    pst_schema = TMSchema.new("LOWCSP PST beams", version, strict)
    pst_schema.add_field(
        "pst_beam_ids",
        [int],
        check_strict=lambda lst: len(lst) <= 16,
        description="List of PST beam Ids to assign to the subarray.",
    )
    csp_schema.add_opt_field(
        "pst", pst_schema, description="Assign section for PST sub-system"
    )
    # PSS schema
    pss_schema = TMSchema.new("LOWCSP PSS beams", version, strict)
    pss_schema.add_field(
        "pss_beam_ids",
        [int],
        check_strict=lambda lst: len(lst) <= 500,
        description="List of PSS beam Ids to assign to the subarray.",
    )
    csp_schema.add_opt_field(
        "pss",
        pss_schema,
        description=cleandoc("Assign section for PSS sub-system"),
    )
    return csp_schema


def get_low_tmc_assignresources_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control assign resources schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    if_strict = mk_if(strict)

    items = TMSchema.new("Low TMC assign resources", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_opt_field(
        "transaction_id",
        str,
        description="A transaction id specific to the command",
    )
    items.add_field(
        "subarray_id",
        int,
        check_strict=lambda n: mccs_validators.validate_subarray_id(n),
        description="""
            ID of sub-array targeted by this resource allocation request
            """,
    )

    mccs_elems = TMSchema.new("MCCS assign resources", version, strict)
    if split_interface_version(version) <= (3, 2):
        mccs_elems.add_field(
            "subarray_beam_ids",
            [
                And(
                    int,
                    if_strict(
                        lambda n: mccs_validators.validate_subarray_beam_id(n)
                    ),
                ),
            ],
            check_strict=lambda x: mccs_validators.validate_subarray_beam_ids(
                x
            ),
            description=cleandoc(
                """
                IDs of the MCCS sub-array beams to allocate to this subarray.

                Each ID must be between 1 and 48, the maximum number of
                sub-array beams.

                As of PI10, only one MCCS sub-array beam can be configured
                per allocation request. Multiple beams must be allocated
                via multiple allocation requests.
                """
            ),
        )
        mccs_elems.add_field(
            "station_ids",
            [
                [
                    And(
                        int,
                        if_strict(
                            lambda n: mccs_validators.validate_station_id(n)
                        ),
                    )
                ]
            ],
            description=cleandoc(
                """
                IDs of MCCS stations to allocate to this sub-array beam.

                Each ID must be between 1 and 512, the maximum number of
                stations.
                """
            ),
        )
        mccs_elems.add_field(
            "channel_blocks",
            [int],
            check_strict=lambda n: mccs_validators.validate_channel_blocks(n),
            description=cleandoc(
                """
                Number of channel blocks to allocate to this sub-array beam.

                Maximum number of channel blocks = 48.
                """
            ),
        )
    if split_interface_version(version) == (4, 0):
        mccs_elems = get_mccs_assignres_schema(
            "https://schema.skao.int/ska-low-mccs-controller-allocate/3.0",
            strict,
        )
    items.add_field(
        "mccs",
        mccs_elems,
        description="MCCS specification for resource allocation.",
    )
    if split_interface_version(version) >= (3, 0):
        sdp_assignres_ver = SDP_ASSIGNRES_PREFIX + "0.4"
        sdpschema = get_sdp_assignres_schema(sdp_assignres_ver, 1)
        items.add_field(
            "sdp",
            sdpschema,
            description="SDP configuration specification",
        )
    tmc_version_num = split_interface_version(version)
    if tmc_version_num >= (3, 0):
        if tmc_version_num == (3, 0):
            csp_version = f"{LOWCSP_ASSIGNRESOURCES_PREFIX}2.0"
            cspschema = get_low_csp_assignresources_schema(csp_version, strict)
            items.add_opt_field(
                "csp", cspschema, description="CSP configuration specification"
            )
        if tmc_version_num == (4, 0):
            csp_version = f"{LOWCSP_ASSIGNRESOURCES_PREFIX}3.2"
            cspschema = get_low_csp_assignres_schema(csp_version, strict)
            items.add_field(
                "csp", cspschema, description="CSP configuration specification"
            )

    return items


def get_low_tmc_configure_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control configuration schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    if_strict = mk_if(strict)
    items = TMSchema.new("Low TMC configure", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_opt_field(
        "transaction_id",
        str,
        description="A transaction id specific to the command",
    )
    tmc_version_num = split_interface_version(version)
    mccsschema = get_mccs_schema(version, strict, tmc_schema_uri=version)
    items.add_field(
        "mccs", mccsschema, description="MCCS configuration specification."
    )
    if tmc_version_num >= (3, 0):
        if tmc_version_num == (3, 0):
            csp_version = f"{LOWCSP_CONFIGURE_PREFIX}2.0"
        elif tmc_version_num == (3, 1):
            csp_version = f"{LOWCSP_CONFIGURE_PREFIX}4.0"
        elif tmc_version_num == (3, 2):
            csp_version = f"{LOWCSP_CONFIGURE_PREFIX}3.1"
        elif tmc_version_num > (3, 2):
            csp_version = f"{LOWCSP_CONFIGURE_PREFIX}3.2"

        cspschema = get_low_csp_configure_schema(
            csp_version,  # pylint: disable=possibly-used-before-assignment
            strict,
            tmc_schema_uri=version,
        )
        items.add_field(
            "csp", cspschema, description="CSP configuration specification."
        )
        sdp_version = f"{SDP_CONFIGURE_PREFIX}0.4"
        sdpschema = get_sdp_configure_3_2_schema(sdp_version, strict)
        items.add_field(
            "sdp", sdpschema, description="SDP configuration specification."
        )
    items.add_opt_field(
        "tmc",
        {
            Literal(
                "scan_duration",
                description=cleandoc(
                    """
                    Scan duration in seconds.

                    Value must be >= 0.0
                    """
                ),
            ): And(float, if_strict(lambda n: n >= 0.0))
        },
        description="TMC configuration specification.",
    )
    return items


def get_low_tmc_scan_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control scan schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    items = TMSchema.new("Low TMC scan", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_opt_field(
        "transaction_id",
        str,
        description="A transaction id specific to the command",
    )
    items.add_field(
        "scan_id",
        int,
        description=cleandoc(
            """
            Scan ID to associate with the data.

            The scan ID and SBI ID are used together to uniquely associate
            the data taken with the telescope configuration in effect at
            the moment of observation.
            """
        ),
    )
    return items


def get_low_tmc_releaseresources_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control resources release schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    items = TMSchema.new("Low TMC resource release", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    if split_interface_version(version) >= (2, 0):
        items.add_opt_field(
            "transaction_id",
            str,
            description="A transaction id specific to the command",
        )
    items.add_field(
        "subarray_id",
        int,
        check_strict=lambda n: mccs_validators.validate_subarray_id(n),
        description="ID of the sub-array which should release resources.",
    )
    items.add_field(
        "release_all",
        bool,
        description=cleandoc(
            """
            true to release all resources, false to release only the
            resources defined in this payload.

            Note: partial resource release for SKA LOW is not implemented
            and the identification of the resources to release is not yet
            part of the schema.
            """
        ),
    )

    return items


def get_tmc_assignedres_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control assigned resources schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    if_strict = mk_if(strict)

    items = TMSchema.new("Low TMC assigned resources", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )

    mccs_items = TMSchema.new("MCCS assigned resources", version, strict)
    mccs_items.add_field(
        "subarray_beam_ids",
        [
            And(
                int,
                if_strict(
                    lambda n: mccs_validators.validate_subarray_beam_id(n)
                ),
            ),
        ],
        check_strict=lambda x: mccs_validators.validate_subarray_beam_ids(x),
        description=cleandoc(
            """
            IDs of the MCCS sub-array beams allocated to this subarray.

            Each ID must be between 1 and 48, the maximum number of
            sub-array beams.
            """
        ),
    )
    mccs_items.add_field(
        "station_ids",
        [
            [
                And(
                    int,
                    if_strict(
                        lambda n: mccs_validators.validate_station_id(n)
                    ),
                )
            ]
        ],
        description=cleandoc(
            """
            IDs of MCCS stations allocated to each MCCS sub-array beam.

            Each ID must be between 1 and 512, the maximum number of
            stations.
            """
        ),
    )
    mccs_items.add_field(
        "channel_blocks",
        [int],
        check_strict=lambda n: mccs_validators.validate_channel_blocks(n),
        description=cleandoc(
            """
            Number of channel blocks allocated per sub-array beam.

            Maximum number of channel blocks = 48.
            """
        ),
    )
    items.add_field(
        "mccs",
        mccs_items,
        description="""
            Specification of the MCCS resources allocated to this sub-array.
            """,
    )

    return items


"""SKA Mid TMC schemas PI 16 onwards"""


def get_tmc_scan_schema(version: str, strict: bool) -> Schema:
    """
     OET to TMC SubArrayNode.scan schema
    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    items = TMSchema.new("Mid TMC scan", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_opt_field(
        "transaction_id",
        str,
        description="A transaction id specific to the command",
    )
    items.add_field(
        "scan_id",
        int,
        description="Scan ID to associate with the data.",
    )
    return items


def get_tmc_assignresources_schema(version: str, strict: bool) -> Schema:
    """SKA Mid Monitoring and Control assign resources schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    items = TMSchema.new("Mid TMC assign resources", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_opt_field(
        "transaction_id",
        str,
        description="A transaction id specific to the command",
    )
    items.add_field(
        "subarray_id",
        int,
        description="""
            ID of sub-array targeted by this resource allocation request
            """,
    )
    # add dish
    dish_elems = TMSchema.new("dish", version, strict)
    dish_elems.add_field(
        "receptor_ids",
        [str],
        description="Receptor ids of dishes",
    )
    items.add_field(
        "dish",
        dish_elems,
        description="Mid Telescope specification for Dish allocation.",
    )
    # add sdp
    assignres_ver = SDP_ASSIGNRES_PREFIX + "0.4"
    sdpschema = get_sdp_assignres_schema(assignres_ver, 1)
    items.add_field(
        "sdp",
        sdpschema,
        description="sdp block for assignres version 0.4",
    )
    return items


"""SKA Mid TMC schemas PI 16 onwards"""

"""
SKA Mid TMC schemas
"""


def get_tmc_configure_schema(version: str, strict: bool) -> Schema:
    """SKA Mid Monitoring and Control configuration schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    if_strict = mk_if(strict)
    major, minor = split_interface_version(version)

    items = TMSchema.new("Mid TMC configure", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_opt_field(
        "transaction_id",
        str,
        description="A transaction id specific to the command",
    )
    pointing_items = TMSchema.new(
        "Pointing Mid TMC configure", version, strict
    )
    target_items = TMSchema.new("Target TMC configure", version, strict)

    if (major, minor) >= (3, 0):
        target_items.add_opt_field(
            "reference_frame",
            And(
                # 'ICRS' was selected over 'icrs' for consistency with the
                # TMC-Low schema
                str,
                if_strict(lambda s: s in ("special", "ICRS")),
            ),
            description=cleandoc(
                """
                Target coordinates reference frame.

                Validation of reference_frame is case sensitive. Allowed
                values are:

                - ICRS: target coordinates are specified using the
                  International Celestial Reference System (ICRS).

                - special: target coordinates are to be determined at runtime
                  via a KatPoint lookup. Setting reference_frame='special'
                  requires that the target name be a valid object in the
                  KatPoint catalogue. Any target RA and dec provided will be
                  ignored when reference_frame='special'.
                """
            ),
        )
    else:
        # prior to v3.0, reference_frame values were not validated
        target_items.add_opt_field(
            "reference_frame",
            str,
            description="standard celestial reference system such as ICRS",
        )

    target_items.add_opt_field(
        "target_name", str, description="celestial source"
    )
    target_items.add_opt_field(
        "ra", str, description="Pointing Right Ascension coordinates."
    )
    target_items.add_opt_field(
        "dec", str, description="Pointing Declination coordinates."
    )

    if (major, minor) >= (2, 2):
        target_items.add_opt_field(
            "ca_offset_arcsec",
            float,
            description=cleandoc(
                """
                Cross-elevation offset in arcseconds from the central
                pointing pointing defined by target's ra+dec.

                This is an optional field; if omitted, an offset of 0
                arcseconds can be assumed.
                """
            ),
        )
        target_items.add_opt_field(
            "ie_offset_arcsec",
            float,
            description=cleandoc(
                """
                Elevation offset in arcseconds from the central pointing
                position defined by the ra+dec pair.

                This is an optional field; if omitted, an offset of 0
                arcseconds can be assumed.
                """
            ),
        )

    # The implementation of SS-120 in PI22 requires a new attribute to control
    # the application of pointing correction models. It was also agreed to
    # make the top-level pointing object and pointing.target objects optional,
    # to allow, for example, an SDP-only configuration, and a configuration
    # that only requests update or reset of the pointing corrections.
    #
    # These changes are introduced in TMC MID schema greater than v2.2
    schema_gt_22 = (major, minor) > (2, 2)

    # pointing.target becomes optional from v2.3 onwards
    pointing_add = (
        pointing_items.add_opt_field
        if schema_gt_22
        else pointing_items.add_field
    )

    # pointing.groups is a new field introduced with ADR-106, defining the
    # target coordinates and tracking/mapping pattern for each receptor group
    # This field will replace the pointing.target field.
    if (major, minor) > (4, 0):
        pointing_add(
            "groups",
            [
                get_pointing_group_schema(version, strict),
            ],
            description=cleandoc(
                """
                Receptor groups definitions.

                Receptors assigned to a receptor group can be steered as a
                whole around the sky. Objects and attributes within the
                receptor group define the pointing coordinate and
                tracking/mapping strategy to use for the group.
                """
            ),
        )

    pointing_add(
        "target",
        target_items,
        description=cleandoc(
            """
            **DEPRECATED**

            Target coordinates

            This field has been superseded by pointing.groups and is
            deprecated from schema version 4.1.

            If pointing.groups is present, any value provided in
            pointing.target will be ignored.
            """
        ),
    )

    # pointing.correction is the new field added for SS-120
    if schema_gt_22:
        pointing_items.add_opt_field(
            "correction",
            # decided on simple check rather than creating an enumeration for
            # allowed values, on the basis that this matches the existing
            # style and updating this in a future PI will be easier than
            # creating additional, versioned enumeration classes.
            And(
                str, if_strict(lambda s: s in ("MAINTAIN", "UPDATE", "RESET"))
            ),
            description=cleandoc(
                """
                Optional operation to apply to the pointing correction model.
                Allowed values are MAINTAIN, UPDATE, and RESET, which have the
                following meaning:

                 - MAINTAIN: continue applying the current pointing correction
                   model
                 - UPDATE: wait for (if necessary) and apply new pointing
                   calibration solution
                 - RESET: reset the applied pointing correction to the pointing
                   model defaults

                Validation of correction values is case sensitive.

                If pointing.correction is not specified, the default operation
                is to make no change to the pointing correction model,
                equivalent to setting correction=MAINTAIN.
                """
            ),
        )

    # pointing becomes optional from v2.3 onwards
    items_add = items.add_opt_field if schema_gt_22 else items.add_field
    items_add(
        "pointing",
        pointing_items,
        description="Pointing configuration specification.",
    )

    items.add_opt_field(
        "dish",
        {
            Literal(
                "receiver_band",
                description="Dish Receiver band configuration",
            ): And(str)
        },
        description="Dish band configuration",
    )
    if (major, minor) >= (4, 0):
        conf_version = CSP_CONFIGSCAN_PREFIX + "4.0"
    else:
        conf_version = CSP_CONFIG_PREFIX + "2.0"
    csp_schema = get_csp_config_schema(conf_version, 1, tmc_schema_uri=version)

    items.add_opt_field(
        "csp",
        csp_schema,
        description="CSP configuration specification.",
    )
    sdp_items = get_sdp_configure_schema(SDP_CONFIGURE_PREFIX + "0.4", strict)
    items.add_opt_field(
        "sdp", sdp_items, description="SDP configuration specification."
    )
    tmc_items = TMSchema.new("TMC configure", version, strict)
    tmc_items.add_opt_field(
        "scan_duration",
        And(float, if_strict(lambda n: n >= 0.0)),
        description=cleandoc(
            """
            Scan duration in seconds.

            Value must be >= 0.0
            """
        ),
    )
    if (major, minor) > (2, 1):
        tmc_items.add_opt_field(
            "partial_configuration",
            bool,
            description=cleandoc(
                """
                Partial Configuration Flag.

                Partial configurations assume that previously set state is
                maintained, and undergo less strict JSON validation.
                """
            ),
        )
    items.add_opt_field(
        "tmc",
        tmc_items,
        description="TMC Mid TMC configuration specification.",
    )

    return items


def get_pointing_group_schema(version: str, strict: bool) -> Schema:
    """Create a schema for the TMC-Mid pointing.group JSON object.

    This function creates a new TMSchema object for the pointing.group
    JSON object introduced in ADR-106.

    :param version: interface version URI
    :param strict: schema strictness
    """
    schema = TMSchema.new("", version, strict)

    schema.add_opt_field(
        "receptors",
        [str],
        description=cleandoc(
            """
            List of receptor IDs to assign to the receptor group for the
            upcoming scan. List values must be valid SKA receptor IDs, e.g.,
            ["SKA001", "SKA063"].
            """
        ),
    )
    schema.add_opt_field(
        "field", get_skydirection(version, strict, as_reference=True)
    )
    schema.add_opt_field(
        "trajectory", get_pointing_group_trajectory(version, strict)
    )
    schema.add_opt_field(
        "projection",
        get_pointing_group_projection(version, strict),
        default="SIN",
    )

    return schema


def get_pointing_group_trajectory(version: str, strict: bool) -> Schema:
    """Create a schema for the TMC-Mid pointing.group.trajectory JSON object.

    :param version: interface version URI
    :param strict: schema strictness
    """
    trajectory_schema = TMSchema.new("", version, strict)
    trajectory_schema.add_field(
        "name",
        str,
        description=cleandoc(
            """
            A trajectory refers to the path that a telescope or antenna
            follows while observing a celestial object. This path can be a
            simple scan across the sky or a more complex pattern designed to
            optimize data collection.

            Example: 'mosaic', 'fixed', 'raster', 'spiral', etc.
            """
        ),
    )

    trajectory_attrs_schema = TMSchema.new(
        "trajectory_attrs_schema", version, strict
    )
    trajectory_attrs_schema.add_opt_field(
        "x_offsets",
        [float],
        description="Fixed array x of offsets from field positions",
    )
    trajectory_attrs_schema.add_opt_field(
        "y_offsets",
        [float],
        description="Fixed array of y offsets from field positions",
    )
    trajectory_attrs_schema.add_opt_field(
        "x", float, description="Fixed x offset from field positions"
    )
    trajectory_attrs_schema.add_opt_field(
        "y", float, description="Fixed y offset from field positions"
    )
    trajectory_schema.add_field(
        "attrs",
        trajectory_attrs_schema,
        description="Trajectory fields for the group",
    )
    return trajectory_schema


def get_pointing_group_projection(version: str, strict: bool) -> Schema:
    """Get the schema for the TMC-Mid pointing.group.projection JSON object.

    :param version: interface version URI
    :param strict: schema strictness
    """
    projection_schema = TMSchema.new("", version, strict)
    if_strict = mk_if(strict)

    projection_schema.add_field(
        "name",
        And(
            # ADR-63 says that schema should be lenient when parsing
            str,
            if_strict(
                lambda s: str.upper(s)
                in ("SIN", "TAN", "ARC", "STG", "CAR", "SSN")
            ),
        ),
        description=cleandoc(
            """
            Projection type. Must be one of 'SIN', 'TAN', 'ARC', 'STG', 'CAR',
            or 'SSN', though values are not case sensitive. Defaults to 'SIN'.

            The following projections are implemented:

            - Orthographic (**SIN**): This is the standard projection in
              aperture synthesis radio astronomy, as it ties in closely with
              the 2-D Fourier imaging equation and the resultant (l, m)
              coordinate system. It is the simple orthographic projection of
              AIPS.

            - Gnomonic (**TAN**): This is commonly used in optical astronomy.
              Great circles are projected as straight lines, so that the
              shortest distance between two points on the sphere is
              represented as a straight line interval (non-uniformly divided
              though).

            - Zenithal equidistant (**ARC**): This is commonly used for
              single-dish maps, and is obtained if relative (cross-el, el)
              coordinates are directly plotted (cross-elevation is azimuth
              scaled by the cosine of elevation).It preserves angular
              distances from the reference point.

            - Stereographic (**STG**): This is useful to represent polar
              regions and large fields. It preserves angles and circles.

            - Plate carree (**CAR**): This is a very simple cylindrical
              projection that directly maps azimuth and elevation to a
              rectangular (*x*, *y*) grid, and returns offsets from the
              reference point on this plane. The *x* offset is therefore equal
              to the azimuth offset, while the *y* offset is equal to the
              elevation offset. It does not preserve angles, distances or
              circles.

            - Swapped orthographic (**SSN**): This is the standard SIN
              projection with the roles of reference and target points
              reversed. It is useful for holography and other beam pattern
              measurements where a dish moves relative to a fixed beacon but
              the beam pattern is referenced to the boresight of the moving
              dish.
            """
        ),
        default="SIN",
    )
    projection_schema.add_field(
        "alignment", str, description="Projection alignment", default="icrs"
    )

    return projection_schema


def get_tmc_releaseresources_schema(version: str, strict: bool) -> Schema:
    """SKA Mid Monitoring and Control resources release schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    items = TMSchema.new("Mid TMC resource release", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_opt_field(
        "transaction_id",
        str,
        description="A transaction id specific to the command",
    )
    items.add_field(
        "subarray_id",
        int,
        check_strict=lambda n: mccs_validators.validate_subarray_id(n),
        description="ID of the sub-array which should release resources.",
    )
    items.add_field(
        "release_all",
        bool,
        description=cleandoc(
            """
            Scan ID to associate with the data.
            true to release all resources, false to release only the
            resources defined in this payload.

            Note: partial resource release for SKA Mid is not implemented
            and the identification of the resources to release is not yet
            part of the schema.
            """
        ),
    )
    items.add_opt_field(
        "receptor_ids",
        [str],
        description="empty list of receptor_ids when release_all is true",
    )
    return items


def get_mccs_schema(version, strict, tmc_schema_uri: str = "") -> TMSchema:
    """
    Build the configure JSON schema for the TMC-MCCS interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :param tmc_schema_uri: tmc URI version. Uused to diffirentiate the schema
        according to the TMC interface version

    :return: Schema for configure command.
    """
    if_strict = mk_if(version)
    mccs_items = TMSchema.new("MCCS configure", version, strict)

    if tmc_schema_uri:
        tmc_version = split_interface_version(tmc_schema_uri)

    # pylint: disable-next=possibly-used-before-assignment
    if not tmc_schema_uri or tmc_version <= (3, 1):
        mccs_items.add_opt_field(
            "stations",
            [
                {
                    Literal(
                        "station_id",
                        description=cleandoc(
                            """
                            MCCS Station ID.

                            Each ID must be between 1 and 512.
                            """
                        ),
                    ): And(
                        int,
                        if_strict(
                            lambda n: mccs_validators.validate_station_id(n)
                        ),
                    )
                }
            ],
            check_strict=lambda x: mccs_validators.validate_stations(x),
            description=cleandoc(
                """
                IDs of the MCCS stations to configure.

                Maximum array size = 512, the maximum number of MCCS
                stations.
                """
            ),
        )
    mccs_subarray_beams_items = TMSchema.new(
        "MCCS subarray beams", version, strict
    )
    mccs_subarray_beams_items.add_field(
        "subarray_beam_id",
        int,
        check_strict=lambda n: mccs_validators.validate_subarray_beam_id(n),
        description=cleandoc(
            """
            ID of MCCS sub-array beam to configure.

            ID must be an integer between 1 and 48.
            """
        ),
    )
    mccs_subarray_beams_items.add_field(
        "update_rate",
        float,
        check_strict=lambda n: mccs_validators.validate_update_rate(n),
        description=cleandoc(
            """
            Update rate for pointing information.

            Value must be 0.0 or greater.

            TODO: clarify whether this is specified as a
            frequency or as a cadence, plus units.
            """
        ),
    )
    # if tmc_schema_uri:
    #     tmc_version = split_interface_version(tmc_schema_uri)
    if not tmc_schema_uri or tmc_version >= (3, 2):
        logical_band_items = TMSchema.new("MCCS Logical Band", version, strict)
        logical_band_items.add_field(
            "start_channel",
            int,
            check_strict=lambda n: mccs_validators.validate_start_channel(n),
            description=cleandoc(
                """
                Channel block configurations.

                Constraints are:
                2 < start channel < 504
                """
            ),
        )
        logical_band_items.add_field(
            "number_of_channels",
            int,
            check_strict=lambda n: mccs_validators.validate_number_of_channel(
                n
            ),
            description=cleandoc(
                """
                Channel block configurations.

                Constraints are:
                8 < number_of_channels < 384
                """
            ),
        )
        mccs_subarray_beams_items.add_field(
            "logical_bands",
            And(
                [logical_band_items],
                lambda n: len(n) <= mccs_validators.MAX_NO_LOGICAL_BANDS,
            ),
            description="MCCS logical bands configuration.",
        )

        apertures_items = TMSchema.new("MCCS Aperture ID", version, strict)
        apertures_items.add_field(
            "aperture_id",
            str,
            check_strict=lambda n: mccs_validators.validate_aperture_id(n),
            description=cleandoc(
                """
                Aperture ID configurations.
                Aperture ID, of the form APXXX.YY
                XXX=station YY=substation.
                """
            ),
        )
        apertures_items.add_field(
            "weighting_key_ref",
            str,
            description="""
            Descriptive ID for the aperture weights
            in the aperture database.
            """,
        )
        mccs_subarray_beams_items.add_field(
            "apertures",
            And(
                [apertures_items],
                lambda n: len(n) <= mccs_validators.MAX_NO_APERATURES,
            ),
            description="MCCS apertures configuration.",
        )
        sky_coordinates_items = TMSchema.new(
            "MCCS SKY Co ordinates", version, strict
        )
        sky_coordinates_items.add_field(
            "reference_frame",
            str,
            check_strict=lambda n: mccs_validators.validate_reference_frame(n),
            description="""
                        Must be one of: ["topocentric", "ICRS", "galactic"]
                        """,
        )
        sky_coordinates_items.add_field(
            "c1",
            float,
            check_strict=lambda n: mccs_validators.validate_first_coordinate(
                n
            ),
            description=cleandoc(
                """
                c1 (number, required): first coordinate,
                RA or azimuth, in degrees.
                Minimum: 0.0. Maximum: 360.0
                """
            ),
        )
        sky_coordinates_items.add_field(
            "c2",
            float,
            check_strict=lambda n: mccs_validators.validate_second_coordinate(
                n
            ),
            description=cleandoc(
                """
                c2 (number, required): second coordinate, dec or elevation,
                in degrees.
                Minimum: -90.0. Maximum: 90.0
                """
            ),
        )
        mccs_subarray_beams_items.add_field(
            "sky_coordinates", sky_coordinates_items
        )
        mccs_items.add_field(
            "subarray_beams",
            And(
                [mccs_subarray_beams_items],
                lambda n: len(n) <= mccs_validators.MAX_NO_SUBARRAY_BEAMS,
            ),
            description="MCCS sub-array beam configuration.",
        )
    elif not tmc_schema_uri or tmc_version <= (3, 1):
        mccs_subarray_beams_items.add_opt_field(
            "station_ids",
            [
                And(
                    int,
                    if_strict(
                        lambda n: mccs_validators.validate_station_id(n)
                    ),
                ),
            ],
            description=cleandoc(
                """
                IDs of MCCS stations within this sub-array
                beamto configure.

                Array size must be less than 512, the maximum
                number of MCCS stations.

                Each item in the list must be an integer
                between 1 and 512.
                """
            ),
        )

        mccs_subarray_beams_items.add_opt_field(
            "channels",
            [
                And(
                    [int],
                    if_strict(lambda n: mccs_validators.validate_channel(n)),
                )
            ],
            check_strict=lambda x: mccs_validators.validate_channels(x),
            description=cleandoc(
                """
                Channel block configurations.

                Each item in the list is a channel block
                configuration, each specified as a list of 4
                numbers as follows:
                [start channel, number of channels,
                beam index, sub-station index]
                Constraints are:
                0 < start channel < 376
                start channel must be a multiple of 8
                8 < number of channels < 48
                1 < beam index < 48
                1 < sub-station index < 8
                """
            ),
        )
        mccs_subarray_beams_items.add_opt_field(
            "antenna_weights",
            [
                And(
                    float,
                    if_strict(
                        lambda n: mccs_validators.validate_antenna_weight(n)
                    ),
                )
            ],
            check_strict=lambda x: mccs_validators.validate_antenna_weights(x),
            description=cleandoc(
                """
                Antenna weights.

                Maximum array size = 512 (=256 antennas x2 pols per
                sub-array beam).

                Antennas signals can be weighted to modify the station
                beam, varying from 0.0 for full exclusion to potentially
                256.0 for an antenna contribution compensated for the
                number of antennas in the beam. This value is an
                amplitude multiplier added to that antenna signal before
                adding into the sum.

                Weights apply to all channels assigned to a beam.
                """
            ),
        )
        mccs_subarray_beams_items.add_opt_field(
            "phase_centre",
            [
                And(
                    float,
                    if_strict(
                        lambda n: mccs_validators.validate_phase_centre_value(
                            n
                        )
                    ),
                )
            ],
            check_strict=lambda x: mccs_validators.validate_phase_centre(x),
            description=cleandoc(
                """
                Phase centre offset for the station beam, in
                metres.

                The reference position for station phase must be
                modified to reflect antenna weighting and their
                contribution to the station beam. This offset can
                be can considered the desired centre of mass for the
                station.

                Constraints:
                array size = 2
                -20 < phase centre value < 20
                """
            ),
        )

        add_mccs_target_subschema(mccs_subarray_beams_items, version, strict)
        mccs_items.add_field(
            "subarray_beams",
            And(
                [mccs_subarray_beams_items],
                lambda n: len(n) <= mccs_validators.MAX_NO_SUBARRAY_BEAMS,
            ),
            description="MCCS sub-array beam configuration.",
        )
    return mccs_items


def get_sdp_configure_3_2_schema(version, strict):
    """
    Build the configure JSON schema for the OET-TMC 3.2 interface.

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema for configure command.
    """
    main_schema = TMSchema.new(
        "LOWSDP configure",
        version,
        strict,
        description=cleandoc(
            """
            Low SDP specific parameters. This section
            contains the parameters relevant to configure the Low SDP
            sub-system.
            """
        ),
    )
    main_schema.add_field(
        "interface",
        str,
        description=("URI of JSON schema for this command's" "JSON payload.."),
    )
    main_schema.add_field(
        "scan_type", str, description=("Scan type string needed on SDP")
    )
    return main_schema

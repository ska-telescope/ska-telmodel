# The functions imported below are used to populate two dictionaries in
# src/ska_telmodel/schema.py that use the schema string as key to lookup
# appropriate functions that will create schemas or examples. The functions
# expect major, minor intgers to specify desired version of schema or example

# functions returning examples of JSON strings accepted by
# LOWCBF subarray commands
from .examples import (
    get_lowcbf_assignresources_example,
    get_lowcbf_configurescan_example,
    get_lowcbf_releaseresources_example,
    get_lowcbf_scan_example,
)

# functions to create LOWCBF schemas for checking JSON parameters
# of each subarray command
from .schema import (
    get_lowcbf_assignresources_schema,
    get_lowcbf_configurescan_schema,
    get_lowcbf_releaseresources_schema,
    get_lowcbf_scan_schema,
)

# Strings encoding the LOWCBF schema URI names
from .version import (
    LOWCBF_ASSIGNRESOURCES_PREFIX,
    LOWCBF_CONFIGURESCAN_PREFIX,
    LOWCBF_RELEASERESOURCES_PREFIX,
    LOWCBF_SCAN_PREFIX,
)

# Default import: all 18 items from above
__all__ = [
    "get_lowcbf_assignresources_example",
    "get_lowcbf_configurescan_example",
    "get_lowcbf_scan_example",
    "get_lowcbf_releaseresources_example",
    "get_lowcbf_assignresources_schema",
    "get_lowcbf_configurescan_schema",
    "get_lowcbf_scan_schema",
    "get_lowcbf_releaseresources_schema",
    "LOWCBF_ASSIGNRESOURCES_PREFIX",
    "LOWCBF_CONFIGURESCAN_PREFIX",
    "LOWCBF_SCAN_PREFIX",
    "LOWCBF_RELEASERESOURCES_PREFIX",
]

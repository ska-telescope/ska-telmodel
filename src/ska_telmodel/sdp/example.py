"""SDP schema examples."""

from ska_telmodel._common import lookup_example, split_interface_version
from ska_telmodel.csp.examples import expand_output_port

from . import examples


def get_sdp_assignres_example(version: str):
    return lookup_example(examples, version, "get_sdp_assignres")


def get_sdp_releaseres_example(version: str):
    return lookup_example(examples, version, "get_sdp_releaseres")


def get_sdp_configure_example(version: str, *args):
    return lookup_example(examples, version, "get_sdp_configure", *args)


def get_sdp_scan_example(version: str, *args):
    return lookup_example(examples, version, "get_sdp_scan", *args)


def get_sdp_recvaddrs_example(
    version: str, for_csp_config_version: str | None = None
):
    recvaddrs_example = lookup_example(examples, version, "get_sdp_recvaddrs")
    return _modify_recvaddrs_for_csp_config_version(
        recvaddrs_example, for_csp_config_version
    )


def _modify_recvaddrs_for_csp_config_version(
    recvaddrs_example: dict, for_csp_config_version: str | None
):
    if for_csp_config_version is None:
        return recvaddrs_example

    csp_major, csp_minor = split_interface_version(for_csp_config_version)

    if (csp_major, csp_minor) >= (3, 0):
        # need to modify ports to be an expanded format to better match what
        # mid.CBF expects
        if "science" in recvaddrs_example:
            if "port" in recvaddrs_example["science"]:
                recvaddrs_example["science"]["port"] = expand_output_port(
                    [[0, 9000], [400, 9000], [740, 9000], [1140, 9000]], 14880
                )
            else:
                recvaddrs_example["science"]["vis0"][
                    "port"
                ] = expand_output_port(
                    [[0, 9000], [400, 9000], [740, 9000], [1140, 9000]], 14880
                )
            if "host" in recvaddrs_example["science"]:
                recvaddrs_example["science"]["host"] = [
                    [0, "192.168.0.1"],
                    [400, "192.168.0.2"],
                    [740, "192.168.0.3"],
                    [1140, "192.168.0.4"],
                ]
            else:
                recvaddrs_example["science"]["vis0"]["host"] = [
                    [0, "192.168.0.1"],
                    [400, "192.168.0.2"],
                    [740, "192.168.0.3"],
                    [1140, "192.168.0.4"],
                ]
        if "calibration" in recvaddrs_example:
            if "port" in recvaddrs_example["calibration"]:
                recvaddrs_example["calibration"]["port"] = expand_output_port(
                    [[0, 9000, 1]], 14880
                )
            else:
                recvaddrs_example["calibration"]["vis0"][
                    "port"
                ] = expand_output_port([[0, 9000, 1]], 14880)

    return recvaddrs_example

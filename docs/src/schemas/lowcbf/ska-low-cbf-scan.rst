ska-low-cbf-scan
================

.. ska-schema:: https://schema.skao.int/ska-low-cbf-scan/0.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-cbf-scan/0.1

       Example JSON

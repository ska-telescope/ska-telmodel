.. ska-schema:: https://schema.skao.int/ska-tmc-configure/4.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-tmc-configure/4.1

       Example JSON.

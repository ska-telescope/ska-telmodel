

.. _Environment Variables:

Environment Variables
=====================

The Library and CLI uses a few environment variables to simplify some access.


.. list-table::
  :widths: auto
  :header-rows: 1

  * - Environment Variable
    - Default Value
    - Description
  * - ``SKA_TELMODEL_CACHE``
    - ``None``
    - Specify the location of the cache directory. By default this uses your user's cache directory
  * - ``SKA_TELMODEL_ALLOW_STRICT_VALIDATION``
    - ``None``
    - Specifies whether strict validation is allowed to raise errors
      about superflous fields. This is typically only a good idea in
      unit and integration tests.
  * - ``CAR_REST_PATH``
    - ``"https://artefact.skao.int/service/rest"``
    - The URL that is used to access the CAR's REST API.
  * - ``CAR_REPO``
    - ``"sts-608"``
    - The repository that large files will be added to
  * - ``CAR_URL``
    - ``"https://artefact.skao.int/repository"``
    - The link used as the base path for downloading the large files.
  * - ``CAR_TMDATA_USERNAME``
    - ``None``
    - The username to be used for uploading new data. (If left as ``None`` there will be an interactive input)
  * - ``CAR_TMDATA_PASSWORD``
    - ``None``
    - The password to be used for uploading new data. (If left as ``None`` there will be an interactive input)
  * - ``SKA_TELMODEL_NEXUS``
    - ``None``
    - A possible repository URL
  * - ``SKA_TELMODEL_SOURCES``
    - ``None``
    - The list of sources to search through. Refer to :ref:`Data Sources`
  * - ``CAR_TMDATA_REPOSITORY_URL``
    - ``None``
    - Used for compatibility with the CI environment
  * - ``CAR_RAW_REPOSITORY_URL``
    - ``None``
    - Used for compatibility with the CI environment

The variables ``CAR_TMDATA_REPOSITORY_URL`` and ``CAR_RAW_REPOSITORY_URL`` are
used as alternatives to ``SKA_TELMODEL_SOURCES`` but only in the CI environment,
they are not intended to be used in most cases.

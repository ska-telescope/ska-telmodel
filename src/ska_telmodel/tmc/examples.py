import copy

from .version import (
    low_tmc_assignresources_uri,
    low_tmc_configure_uri,
    low_tmc_releaseresources_uri,
    low_tmc_scan_uri,
    tmc_assignedres_uri,
    tmc_assignresources_uri,
    tmc_configure_uri,
    tmc_releaseresources_uri,
    tmc_scan_uri,
)


def _recursive_merge(dict1, dict2):
    """
    Recursively merges two dictionaries.

    This function modifies dict1, merging in values from dict 2.

    @param dict1: dictionary to modify
    @param dict2: dictionary to merge into dict1
    """
    for key, value in dict2.items():
        if (
            key in dict1
            and isinstance(dict1[key], dict)
            and isinstance(value, dict)
        ):
            # Recursively merge nested dictionaries
            dict1[key] = _recursive_merge(dict1[key], value)
        else:
            # Merge non-dictionary values
            dict1[key] = value
    return dict1


LOW_TMC_ASSIGNRESOURCES_1_0 = {
    "interface": (
        "https://schema.skatelescope.org/ska-low-tmc-assignresources/1.0"
    ),
    "subarray_id": 1,
    "mccs": {
        "subarray_beam_ids": [1],
        "station_ids": [[1, 2]],
        "channel_blocks": [3],
    },
}

LOW_TMC_CONFIGURE_1_0 = {
    "interface": "https://schema.skatelescope.org/ska-low-tmc-configure/1.0",
    "mccs": {
        "stations": [{"station_id": 1}, {"station_id": 2}],
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "station_ids": [1, 2],
                "update_rate": 0.0,
                "channels": [[0, 8, 1, 1], [8, 8, 2, 1], [24, 16, 2, 1]],
                "antenna_weights": [1.0, 1.0, 1.0],
                "phase_centre": [0.0, 0.0],
                "target": {
                    "system": "HORIZON",
                    "name": "DriftScan",
                    "az": 180.0,
                    "el": 45.0,
                },
            }
        ],
    },
    "tmc": {"scan_duration": 10.0},
}

LOW_TMC_RELEASERESOURCES_1_0 = {
    "interface": (
        "https://schema.skatelescope.org/ska-low-tmc-releaseresources/1.0"
    ),
    "subarray_id": 1,
    "release_all": True,
}

LOW_TMC_SCAN_1_0 = {
    "interface": "https://schema.skatelescope.org/ska-low-tmc-scan/1.0",
    "scan_id": 1,
}

TMC_ASSIGNEDRES_1_0 = {
    "interface": (
        "https://schema.skatelescope.org/ska-low-tmc-assignedresources/1.0"
    ),
    "mccs": {
        "subarray_beam_ids": [1],
        "station_ids": [[1, 2]],
        "channel_blocks": [3],
    },
}

TMC_ASSIGNEDRES_EMPTY_1_0 = {
    "interface": (
        "https://schema.skatelescope.org/ska-low-tmc-assignedresources/1.0"
    ),
    "mccs": {
        "subarray_beam_ids": [],
        "station_ids": [],
        "channel_blocks": [],
    },
}

LOW_TMC_ASSIGNRESOURCES_2_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-assignresources/2.0",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "mccs": {
        "subarray_beam_ids": [1],
        "station_ids": [[1, 2]],
        "channel_blocks": [3],
    },
}

LOW_TMC_ASSIGNRESOURCES_3_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-assignresources/3.0",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "mccs": {
        "subarray_beam_ids": [1],
        "station_ids": [[1, 2]],
        "channel_blocks": [3],
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-assignres/0.4",
        "resources": {"receptors": ["SKA001", "SKA002", "SKA003", "SKA004"]},
        "execution_block": {
            "eb_id": "eb-test-20220916-00000",
            "context": {},
            "max_length": 3600.0,
            "beams": [{"beam_id": "vis0", "function": "visibilities"}],
            "scan_types": [
                {
                    "scan_type_id": ".default",
                    "beams": {
                        "vis0": {
                            "channels_id": "vis_channels",
                            "polarisations_id": "all",
                        }
                    },
                },
                {
                    "scan_type_id": "target:a",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_a"}},
                },
                {
                    "scan_type_id": "calibration:b",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_b"}},
                },
            ],
            "channels": [
                {
                    "channels_id": "vis_channels",
                    "spectral_windows": [
                        {
                            "spectral_window_id": "fsp_1_channels",
                            "count": 4,
                            "start": 0,
                            "stride": 2,
                            "freq_min": 350000000.0,
                            "freq_max": 368000000.0,
                            "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                        }
                    ],
                }
            ],
            "polarisations": [
                {
                    "polarisations_id": "all",
                    "corr_type": ["XX", "XY", "YX", "YY"],
                }
            ],
            "fields": [
                {
                    "field_id": "field_a",
                    "phase_dir": {
                        "ra": [123.0],
                        "dec": [-60.0],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "...",
                },
                {
                    "field_id": "field_b",
                    "phase_dir": {
                        "ra": [123.0],
                        "dec": [-60.0],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "...",
                },
            ],
        },
        "processing_blocks": [
            {
                "pb_id": "pb-test-20220916-00000",
                "script": {
                    "kind": "realtime",
                    "name": "test-receive-addresses",
                    "version": "0.6.1",
                },
                "sbi_ids": ["sbi-test-20220916-00000"],
                "parameters": {"time_to_ready": 5},
            }
        ],
    },
    "csp": {
        "interface": "https://schema.skao.int/ska-low-csp-assignresources/2.0",
        "common": {"subarray_id": 1},
        "lowcbf": {
            "resources": [
                {
                    "device": "fsp_01",
                    "shared": True,
                    "fw_image": "pst",
                    "fw_mode": "unused",
                },
                {
                    "device": "p4_01",
                    "shared": True,
                    "fw_image": "p4.bin",
                    "fw_mode": "p4",
                },
            ]
        },
    },
}

LOW_TMC_ASSIGNRESOURCES_3_1 = {
    "interface": "https://schema.skao.int/ska-low-tmc-assignresources/3.1",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "mccs": {
        "subarray_beam_ids": [1],
        "station_ids": [[1, 2]],
        "channel_blocks": [3],
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-assignres/0.4",
        "resources": {"receptors": ["SKA001", "SKA002"]},
        "execution_block": {
            "eb_id": "eb-test-20220916-00000",
            "context": {},
            "max_length": 3600.0,
            "beams": [{"beam_id": "vis0", "function": "visibilities"}],
            "scan_types": [
                {
                    "scan_type_id": ".default",
                    "beams": {
                        "vis0": {
                            "channels_id": "vis_channels",
                            "polarisations_id": "all",
                        }
                    },
                },
                {
                    "scan_type_id": "target:a",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_a"}},
                },
                {
                    "scan_type_id": "calibration:b",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_b"}},
                },
            ],
            "channels": [
                {
                    "channels_id": "vis_channels",
                    "spectral_windows": [
                        {
                            "spectral_window_id": "fsp_1_channels",
                            "count": 4,
                            "start": 0,
                            "stride": 2,
                            "freq_min": 350000000.0,
                            "freq_max": 368000000.0,
                            "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                        }
                    ],
                }
            ],
            "polarisations": [
                {
                    "polarisations_id": "all",
                    "corr_type": ["XX", "XY", "YX", "YY"],
                }
            ],
            "fields": [
                {
                    "field_id": "field_a",
                    "phase_dir": {
                        "ra": [123.0],
                        "dec": [-60.0],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "...",
                },
                {
                    "field_id": "field_b",
                    "phase_dir": {
                        "ra": [123.0],
                        "dec": [-60.0],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "...",
                },
            ],
        },
        "processing_blocks": [
            {
                "pb_id": "pb-test-20220916-00000",
                "script": {
                    "kind": "realtime",
                    "name": "test-receive-addresses",
                    "version": "0.5.0",
                },
                "sbi_ids": ["sbi-test-20220916-00000"],
                "parameters": {},
            }
        ],
    },
}

LOW_TMC_ASSIGNRESOURCES_3_2 = {
    "interface": "https://schema.skao.int/ska-low-tmc-assignresources/3.2",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "mccs": {
        "subarray_beam_ids": [1],
        "station_ids": [[1, 2]],
        "channel_blocks": [3],
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-assignres/0.4",
        "resources": {"receptors": ["SKA001", "SKA002", "SKA003", "SKA004"]},
        "execution_block": {
            "eb_id": "eb-test-20220916-00000",
            "context": {},
            "max_length": 3600.0,
            "beams": [{"beam_id": "vis0", "function": "visibilities"}],
            "scan_types": [
                {
                    "scan_type_id": ".default",
                    "beams": {
                        "vis0": {
                            "channels_id": "vis_channels",
                            "polarisations_id": "all",
                        }
                    },
                },
                {
                    "scan_type_id": "target:a",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_a"}},
                },
                {
                    "scan_type_id": "calibration:b",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_b"}},
                },
            ],
            "channels": [
                {
                    "channels_id": "vis_channels",
                    "spectral_windows": [
                        {
                            "spectral_window_id": "fsp_1_channels",
                            "count": 4,
                            "start": 0,
                            "stride": 2,
                            "freq_min": 350000000.0,
                            "freq_max": 368000000.0,
                            "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                        }
                    ],
                }
            ],
            "polarisations": [
                {
                    "polarisations_id": "all",
                    "corr_type": ["XX", "XY", "YX", "YY"],
                }
            ],
            "fields": [
                {
                    "field_id": "field_a",
                    "phase_dir": {
                        "ra": [123.0],
                        "dec": [-60.0],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "...",
                },
                {
                    "field_id": "field_b",
                    "phase_dir": {
                        "ra": [123.0],
                        "dec": [-60.0],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "...",
                },
            ],
        },
        "processing_blocks": [
            {
                "pb_id": "pb-test-20220916-00000",
                "script": {
                    "kind": "realtime",
                    "name": "test-receive-addresses",
                    "version": "0.5.0",
                },
                "sbi_ids": ["sbi-test-20220916-00000"],
                "parameters": {},
            }
        ],
    },
}
LOW_TMC_ASSIGNRESOURCES_4_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-assignresources/4.0",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "mccs": {
        "interface": (
            "https://schema.skao.int/ska-low-mccs-controller-allocate/3.0"
        ),
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "apertures": [
                    {"station_id": 1, "aperture_id": "AP001.01"},
                    {"station_id": 1, "aperture_id": "AP001.02"},
                    {"station_id": 2, "aperture_id": "AP002.01"},
                    {"station_id": 2, "aperture_id": "AP002.02"},
                ],
                "number_of_channels": 8,
            }
        ],
    },
    "csp": {"pss": {"pss_beam_ids": [1, 2, 3]}, "pst": {"pst_beam_ids": [1]}},
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-assignres/0.4",
        "resources": {
            "receptors": [
                "C4",
                "C57",
                "C108",
                "C165",
                "C193",
                "C200",
                "S8-1",
                "S8-2",
                "S9-1",
                "S9-5",
                "S10-1",
                "S10-6",
                "S16-3",
                "S16-4",
                "S16-6",
            ],
        },
        "execution_block": {
            "eb_id": "eb-test-20220916-00000",
            "context": {},
            "max_length": 3600.0,
            "beams": [{"beam_id": "vis0", "function": "visibilities"}],
            "scan_types": [
                {
                    "scan_type_id": ".default",
                    "beams": {
                        "vis0": {
                            "channels_id": "vis_channels",
                            "polarisations_id": "all",
                        }
                    },
                },
                {
                    "scan_type_id": "target:a",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_a"}},
                },
                {
                    "scan_type_id": "calibration:b",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_b"}},
                },
            ],
            "channels": [
                {
                    "channels_id": "vis_channels",
                    "spectral_windows": [
                        {
                            "spectral_window_id": "fsp_1_channels",
                            "count": 4,
                            "start": 0,
                            "stride": 2,
                            "freq_min": 350000000.0,
                            "freq_max": 368000000.0,
                            "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                        }
                    ],
                }
            ],
            "polarisations": [
                {
                    "polarisations_id": "all",
                    "corr_type": ["XX", "XY", "YX", "YY"],
                }
            ],
            "fields": [
                {
                    "field_id": "field_a",
                    "phase_dir": {
                        "ra": [123.0],
                        "dec": [-60.0],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "...",
                },
                {
                    "field_id": "field_b",
                    "phase_dir": {
                        "ra": [123.0],
                        "dec": [-60.0],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "...",
                },
            ],
        },
        "processing_blocks": [
            {
                "pb_id": "pb-test-20220916-00000",
                "script": {
                    "kind": "realtime",
                    "name": "test-receive-addresses",
                    "version": "0.7.1",
                },
                "sbi_ids": ["sbi-mvp01-20210623-00000"],
                "parameters": {},
            }
        ],
    },
}

TMC_ASSIGNRESOURCES_2_1 = {
    "interface": "https://schema.skao.int/ska-tmc-assignresources/2.1",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "dish": {"receptor_ids": ["0001"]},
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-assignres/0.4",
        "execution_block": {
            "eb_id": "eb-mvp01-20210623-00000",
            "max_length": 100.0,
            "context": {},
            "beams": [
                {"beam_id": "vis0", "function": "visibilities"},
                {
                    "beam_id": "pss1",
                    "search_beam_id": 1,
                    "function": "pulsar search",
                },
                {
                    "beam_id": "pss2",
                    "search_beam_id": 2,
                    "function": "pulsar search",
                },
                {
                    "beam_id": "pst1",
                    "timing_beam_id": 1,
                    "function": "pulsar timing",
                },
                {
                    "beam_id": "pst2",
                    "timing_beam_id": 2,
                    "function": "pulsar timing",
                },
                {"beam_id": "vlbi1", "vlbi_beam_id": 1, "function": "vlbi"},
            ],
            "scan_types": [
                {
                    "scan_type_id": ".default",
                    "beams": {
                        "vis0": {
                            "channels_id": "vis_channels",
                            "polarisations_id": "all",
                        },
                        "pss1": {
                            "field_id": "pss_field_0",
                            "channels_id": "pulsar_channels",
                            "polarisations_id": "all",
                        },
                        "pss2": {
                            "field_id": "pss_field_1",
                            "channels_id": "pulsar_channels",
                            "polarisations_id": "all",
                        },
                        "pst1": {
                            "field_id": "pst_field_0",
                            "channels_id": "pulsar_channels",
                            "polarisations_id": "all",
                        },
                        "pst2": {
                            "field_id": "pst_field_1",
                            "channels_id": "pulsar_channels",
                            "polarisations_id": "all",
                        },
                        "vlbi": {
                            "field_id": "vlbi_field",
                            "channels_id": "vlbi_channels",
                            "polarisations_id": "all",
                        },
                    },
                },
                {
                    "scan_type_id": "target:a",
                    "derive_from": ".default",
                    "beams": {"vis0": {"field_id": "field_a"}},
                },
            ],
            "channels": [
                {
                    "channels_id": "vis_channels",
                    "spectral_windows": [
                        {
                            "spectral_window_id": "fsp_1_channels",
                            "count": 744,
                            "start": 0,
                            "stride": 2,
                            "freq_min": 350000000.0,
                            "freq_max": 368000000.0,
                            "link_map": [[0, 0], [200, 1], [744, 2], [944, 3]],
                        },
                        {
                            "spectral_window_id": "fsp_2_channels",
                            "count": 744,
                            "start": 2000,
                            "stride": 1,
                            "freq_min": 360000000.0,
                            "freq_max": 368000000.0,
                            "link_map": [[2000, 4], [2200, 5]],
                        },
                        {
                            "spectral_window_id": "zoom_window_1",
                            "count": 744,
                            "start": 4000,
                            "stride": 1,
                            "freq_min": 360000000.0,
                            "freq_max": 361000000.0,
                            "link_map": [[4000, 6], [4200, 7]],
                        },
                    ],
                },
                {
                    "channels_id": "pulsar_channels",
                    "spectral_windows": [
                        {
                            "spectral_window_id": "pulsar_fsp_channels",
                            "count": 744,
                            "start": 0,
                            "freq_min": 350000000.0,
                            "freq_max": 368000000.0,
                        }
                    ],
                },
            ],
            "polarisations": [
                {
                    "polarisations_id": "all",
                    "corr_type": ["XX", "XY", "YY", "YX"],
                }
            ],
            "fields": [
                {
                    "field_id": "field_a",
                    "phase_dir": {
                        "ra": [123, 0.1],
                        "dec": [80, 0.1],
                        "reference_time": "...",
                        "reference_frame": "ICRF3",
                    },
                    "pointing_fqdn": "low-tmc/telstate/0/pointing",
                }
            ],
        },
        "processing_blocks": [
            {
                "pb_id": "pb-mvp01-20210623-00000",
                "sbi_ids": ["sbi-mvp01-20200325-00001"],
                "script": {
                    "kind": "realtime",
                    "name": "vis_receive",
                    "version": "0.1.0",
                },
                "parameters": {
                    "holography_receptor_groups": {
                        "group_1": ["SKA001", "SKA036", "SKA063"],
                        "group_2": ["SKA100"],
                    }
                },
            },
            {
                "pb_id": "pb-mvp01-20210623-00001",
                "sbi_ids": ["sbi-mvp01-20200325-00001"],
                "script": {
                    "kind": "realtime",
                    "name": "test_realtime",
                    "version": "0.1.0",
                },
                "parameters": {},
            },
            {
                "pb_id": "pb-mvp01-20210623-00002",
                "sbi_ids": ["sbi-mvp01-20200325-00002"],
                "script": {
                    "kind": "batch",
                    "name": "ical",
                    "version": "0.1.0",
                },
                "parameters": {},
                "dependencies": [
                    {
                        "pb_id": "pb-mvp01-20210623-00000",
                        "kind": ["visibilities"],
                    }
                ],
            },
            {
                "pb_id": "pb-mvp01-20210623-00003",
                "sbi_ids": [
                    "sbi-mvp01-20200325-00001",
                    "sbi-mvp01-20200325-00002",
                ],
                "script": {
                    "kind": "batch",
                    "name": "dpreb",
                    "version": "0.1.0",
                },
                "parameters": {},
                "dependencies": [
                    {
                        "pb_id": "pb-mvp01-20210623-00002",
                        "kind": ["calibration"],
                    }
                ],
            },
        ],
        "resources": {
            "receptors": ["SKA001", "SKA036", "SKA063", "SKA100"],
        },
    },
}


LOW_TMC_CONFIGURE_2_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-configure/2.0",
    "transaction_id": "txn-....-00001",
    "mccs": {
        "stations": [{"station_id": 1}, {"station_id": 2}],
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "station_ids": [1, 2],
                "update_rate": 0.0,
                "channels": [[0, 8, 1, 1], [8, 8, 2, 1], [24, 16, 2, 1]],
                "antenna_weights": [1.0, 1.0, 1.0],
                "phase_centre": [0.0, 0.0],
                "target": {
                    "reference_frame": "HORIZON",
                    "target_name": "DriftScan",
                    "az": 180.0,
                    "el": 45.0,
                },
            }
        ],
    },
    "tmc": {"scan_duration": 10.0},
}

LOW_TMC_CONFIGURE_3_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-configure/3.0",
    "transaction_id": "txn-....-00001",
    "mccs": {
        "stations": [{"station_id": 1}, {"station_id": 2}],
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "station_ids": [1, 2],
                "update_rate": 0.0,
                "channels": [[0, 8, 1, 1], [8, 8, 2, 1], [24, 16, 2, 1]],
                "antenna_weights": [1.0, 1.0, 1.0],
                "phase_centre": [0.0, 0.0],
                "target": {
                    "reference_frame": "HORIZON",
                    "target_name": "DriftScan",
                    "az": 180.0,
                    "el": 45.0,
                },
            }
        ],
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
        "scan_type": "target:a",
    },
    "csp": {
        "interface": "https://schema.skao.int/ska-low-csp-configure/2.0",
        "subarray": {"subarray_name": "science period 23"},
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
            "subarray_id": 1,
        },
        "lowcbf": {
            "stations": {
                "stns": [[1, 0], [2, 0], [3, 0], [4, 0]],
                "stn_beams": [
                    {
                        "beam_id": 1,
                        "freq_ids": [64, 65, 66, 67, 68, 69, 70, 71],
                        "boresight_dly_poly": "url",
                    }
                ],
            },
            "timing_beams": {
                "beams": [
                    {
                        "pst_beam_id": 13,
                        "stn_beam_id": 1,
                        "offset_dly_poly": "url",
                        "stn_weights": [0.9, 1.0, 1.0, 0.9],
                        "jones": "url",
                        "rfi_enable": [True, True, True],
                        "rfi_static_chans": [1, 206, 997],
                        "rfi_dynamic_chans": [242, 1342],
                        "rfi_weighted": 0.87,
                    }
                ]
            },
        },
    },
    "tmc": {"scan_duration": 10.0},
}

LOW_TMC_CONFIGURE_3_1 = {
    "interface": "https://schema.skao.int/ska-low-tmc-configure/3.1",
    "transaction_id": "txn-....-00001",
    "mccs": {
        "stations": [{"station_id": 1}, {"station_id": 2}],
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "station_ids": [1, 2],
                "update_rate": 0.0,
                "channels": [[0, 8, 1, 1], [8, 8, 2, 1], [24, 16, 2, 1]],
                "antenna_weights": [1.0, 1.0, 1.0],
                "phase_centre": [0.0, 0.0],
                "target": {
                    "reference_frame": "HORIZON",
                    "target_name": "DriftScan",
                    "az": 180.0,
                    "el": 45.0,
                },
            }
        ],
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
        "scan_type": "science_A",
    },
    "csp": {
        "interface": "https://schema.skao.int/ska-low-csp-configure/4.0",
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
        },
        "lowcbf": {
            "stations": {
                "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
                "stn_beams": [{"stn_beam_id": 1, "freq_ids": [400]}],
            },
            "vis": {
                "fsp": {"function_mode": "vis", "fsp_ids": [1]},
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "host": [[0, "192.168.0.1"]],
                        "port": [[0, 9000, 1]],
                        "mac": [[0, "02-03-04-0a-0b-0c"]],
                        "integration_ms": 849,
                    }
                ],
            },
        },
    },
    "tmc": {"scan_duration": 10.0},
}

LOW_TMC_CONFIGURE_3_2 = {
    "interface": "https://schema.skao.int/ska-low-tmc-configure/3.2",
    "transaction_id": "txn-....-00001",
    "mccs": {
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "update_rate": 0.0,
                "logical_bands": [
                    {"start_channel": 80, "number_of_channels": 16},
                    {"start_channel": 384, "number_of_channels": 16},
                ],
                "apertures": [
                    {
                        "aperture_id": "AP001.01",
                        "weighting_key_ref": "aperture2",
                    },
                    {
                        "aperture_id": "AP001.02",
                        "weighting_key_ref": "aperture3",
                    },
                    {
                        "aperture_id": "AP002.01",
                        "weighting_key_ref": "aperture2",
                    },
                    {
                        "aperture_id": "AP002.02",
                        "weighting_key_ref": "aperture3",
                    },
                    {
                        "aperture_id": "AP003.01",
                        "weighting_key_ref": "aperture1",
                    },
                ],
                "sky_coordinates": {
                    "reference_frame": "ICRS",
                    "c1": 180.0,
                    "c2": 45.0,
                },
            }
        ],
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
        "scan_type": "science_A",
    },
    "csp": {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.1",
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
        },
        "lowcbf": {
            "stations": {
                "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
                "stn_beams": [{"beam_id": 1, "freq_ids": [400]}],
            },
            "vis": {
                "fsp": {"firmware": "vis", "fsp_ids": [1]},
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "integration_ms": 849,
                    }
                ],
            },
        },
    },
    "tmc": {"scan_duration": 10.0},
}


LOW_TMC_CONFIGURE_3_3 = {
    "interface": "https://schema.skao.int/ska-low-tmc-configure/3.3",
    "transaction_id": "txn-....-00001",
    "mccs": {
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "update_rate": 0.0,
                "logical_bands": [
                    {"start_channel": 80, "number_of_channels": 16},
                    {"start_channel": 384, "number_of_channels": 16},
                ],
                "apertures": [
                    {
                        "aperture_id": "AP001.01",
                        "weighting_key_ref": "aperture2",
                    },
                    {
                        "aperture_id": "AP001.02",
                        "weighting_key_ref": "aperture3",
                    },
                    {
                        "aperture_id": "AP002.01",
                        "weighting_key_ref": "aperture2",
                    },
                    {
                        "aperture_id": "AP002.02",
                        "weighting_key_ref": "aperture3",
                    },
                    {
                        "aperture_id": "AP003.01",
                        "weighting_key_ref": "aperture1",
                    },
                ],
                "sky_coordinates": {
                    "reference_frame": "ICRS",
                    "c1": 180.0,
                    "c2": 45.0,
                },
            }
        ],
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
        "scan_type": "target:a",
    },
    "csp": {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
        },
        "lowcbf": {
            "stations": {
                "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
                "stn_beams": [{"beam_id": 1, "freq_ids": [400]}],
            },
            "vis": {
                "fsp": {"firmware": "vis", "fsp_ids": [1]},
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "integration_ms": 849,
                    }
                ],
            },
        },
    },
    "tmc": {"scan_duration": 10.0},
}

LOW_TMC_CONFIGURE_4_0_PST_SCAN_VOLTAGE_RECORDER = {
    "interface": "https://schema.skao.int/ska-low-tmc-configure/4.0",
    "transaction_id": "txn-....-00001",
    "mccs": {
        "subarray_beams": [
            {
                "subarray_beam_id": 1,
                "update_rate": 0.0,
                "logical_bands": [
                    {"start_channel": 80, "number_of_channels": 16},
                    {"start_channel": 384, "number_of_channels": 16},
                ],
                "apertures": [
                    {
                        "aperture_id": "AP001.01",
                        "weighting_key_ref": "aperture2",
                    },
                    {
                        "aperture_id": "AP001.02",
                        "weighting_key_ref": "aperture3",
                    },
                    {
                        "aperture_id": "AP002.01",
                        "weighting_key_ref": "aperture2",
                    },
                    {
                        "aperture_id": "AP002.02",
                        "weighting_key_ref": "aperture3",
                    },
                    {
                        "aperture_id": "AP003.01",
                        "weighting_key_ref": "aperture1",
                    },
                ],
                "sky_coordinates": {
                    "reference_frame": "ICRS",
                    "c1": 180.0,
                    "c2": 45.0,
                },
            }
        ],
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
        "scan_type": "target:a",
    },
    "csp": {
        "interface": "https://schema.skao.int/ska-low-csp-configure/3.2",
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
            "eb_id": "eb-test-20220916-00000",
        },
        "lowcbf": {
            "stations": {
                "stns": [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
                "stn_beams": [{"beam_id": 1, "freq_ids": [400]}],
            },
            "vis": {
                "fsp": {"firmware": "vis", "fsp_ids": [1]},
                "stn_beams": [
                    {
                        "stn_beam_id": 1,
                        "integration_ms": 849,
                    }
                ],
            },
            "timing_beams": {
                "fsp": {"firmware": "pst", "fsp_ids": [2]},
                "beams": [
                    {
                        "pst_beam_id": 1,
                        "stn_beam_id": 1,
                        "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                    }
                ],
            },
        },
        "pst": {
            "beams": [
                {
                    "beam_id": 1,
                    "scan": {
                        "activation_time": "2022-01-19T23:07:45Z",
                        "bits_per_sample": 32,
                        "num_of_polarizations": 2,
                        "udp_nsamp": 32,
                        "wt_nsamp": 32,
                        "udp_nchan": 24,
                        "num_frequency_channels": 432,
                        "centre_frequency": 200000000.0,
                        "total_bandwidth": 1562500.0,
                        "observation_mode": "VOLTAGE_RECORDER",
                        "observer_id": "jdoe",
                        "project_id": "project1",
                        "pointing_id": "pointing1",
                        "source": "J1921+2153",
                        "itrf": [5109360.133, 2006852.586, -3238948.127],
                        "receiver_id": "receiver3",
                        "feed_polarization": "LIN",
                        "feed_handedness": 1,
                        "feed_angle": 1.234,
                        "feed_tracking_mode": "FA",
                        "feed_position_angle": 10.0,
                        "oversampling_ratio": [8, 7],
                        "coordinates": {
                            "equinox": 2000.0,
                            "ra": "19:21:44.815",
                            "dec": "21:53:02.400",
                        },
                        "max_scan_length": 20000.0,
                        "subint_duration": 30.0,
                        "receptors": ["receptor1", "receptor2"],
                        "receptor_weights": [0.4, 0.6],
                        "num_channelization_stages": 2,
                        "channelization_stages": [
                            {
                                "num_filter_taps": 1,
                                "filter_coefficients": [1.0],
                                "num_frequency_channels": 1024,
                                "oversampling_ratio": [32, 27],
                            },
                            {
                                "num_filter_taps": 1,
                                "filter_coefficients": [1.0],
                                "num_frequency_channels": 256,
                                "oversampling_ratio": [4, 3],
                            },
                        ],
                    },
                },
            ],
        },
    },
    "tmc": {"scan_duration": 10.0},
}

# The only difference between v4.0 and v4.1 is the interface ID and
# the addition of field coordinates to PST timing beams. As the PST
# beam object is inside a list, we need to replace the whole beams list.
LOW_TMC_CONFIGURE_4_1_PST_SCAN_VOLTAGE_RECORDER = _recursive_merge(
    copy.deepcopy(LOW_TMC_CONFIGURE_4_0_PST_SCAN_VOLTAGE_RECORDER),
    {
        "interface": "https://schema.skao.int/ska-tmc-configure/4.1",
        "csp": {
            "lowcbf": {
                "timing_beams": {
                    "beams": [
                        {
                            "pst_beam_id": 1,
                            "field": {
                                "target_name": "PSR J0024-7204R",
                                "reference_frame": "icrs",
                                "attrs": {
                                    "c1": 6.023625,
                                    "c2": -72.08128333,
                                    "pm_c1": 4.8,
                                    "pm_c2": -3.3,
                                },
                            },
                            "stn_beam_id": 1,
                            "stn_weights": [0.9, 1.0, 1.0, 1.0, 0.9, 1.0],
                        }
                    ]
                }
            }
        },
    },
)


LOW_TMC_RELEASERESOURCES_2_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-releaseresources/2.0",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "release_all": True,
}

LOW_TMC_RELEASERESOURCES_3_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-releaseresources/3.0",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "release_all": True,
}

LOW_TMC_SCAN_2_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-scan/2.0",
    "transaction_id": "txn-....-00001",
    "scan_id": 1,
}

LOW_TMC_SCAN_3_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-scan/3.0",
    "transaction_id": "txn-....-00001",
    "scan_id": 1,
}

LOW_TMC_SCAN_4_0 = {
    "interface": "https://schema.skao.int/ska-low-tmc-scan/4.0",
    "transaction_id": "txn-....-00001",
    "scan_id": 1,
}

TMC_RELEASERESOURCES_2_1 = {
    "interface": "https://schema.skao.int/ska-tmc-releaseresources/2.1",
    "transaction_id": "txn-....-00001",
    "subarray_id": 1,
    "release_all": True,
    "receptor_ids": [],
}

TMC_SCAN_2_1 = {
    "interface": "https://schema.skao.int/ska-tmc-scan/2.1",
    "transaction_id": "txn-....-00001",
    "scan_id": 1,
}

TMC_CONFIGURE_2_1 = {
    "interface": "https://schema.skao.int/ska-tmc-configure/2.1",
    "transaction_id": "txn-....-00001",
    "pointing": {
        "target": {
            "reference_frame": "ICRS",
            "target_name": "Polaris Australis",
            "ra": "21:08:47.92",
            "dec": "-88:57:22.9",
        }
    },
    "dish": {"receiver_band": "1"},
    "csp": {
        "interface": "https://schema.skao.int/ska-csp-configure/2.0",
        "subarray": {"subarray_name": "science period 23"},
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
            "frequency_band": "1",
            "subarray_id": 1,
        },
        "cbf": {
            "fsp": [
                {
                    "fsp_id": 1,
                    "function_mode": "CORR",
                    "frequency_slice_id": 1,
                    "integration_factor": 1,
                    "zoom_factor": 0,
                    "channel_averaging_map": [[0, 2], [744, 0]],
                    "channel_offset": 0,
                    "output_link_map": [[0, 0], [200, 1]],
                },
                {
                    "fsp_id": 2,
                    "function_mode": "CORR",
                    "frequency_slice_id": 2,
                    "integration_factor": 1,
                    "zoom_factor": 1,
                    "zoom_window_tuning": 650000,
                    "channel_averaging_map": [[0, 2], [744, 0]],
                    "channel_offset": 744,
                    "output_link_map": [[0, 4], [200, 5]],
                },
            ],
            "vlbi": {},
        },
        "pss": {},
        "pst": {},
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
        "scan_type": "science_A",
    },
    "tmc": {"scan_duration": 10.0},
}


TMC_CONFIGURE_2_2 = {
    "interface": "https://schema.skao.int/ska-tmc-configure/2.2",
    "transaction_id": "txn-....-00001",
    "pointing": {
        "target": {
            "reference_frame": "ICRS",
            "target_name": "Polaris Australis",
            "ra": "21:08:47.92",
            "dec": "-88:57:22.9",
            "ca_offset_arcsec": 0.0,
            "ie_offset_arcsec": 0.0,
        }
    },
    "dish": {"receiver_band": "1"},
    "csp": {
        "interface": "https://schema.skao.int/ska-csp-configure/2.0",
        "subarray": {"subarray_name": "science period 23"},
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
            "frequency_band": "1",
            "subarray_id": 1,
        },
        "cbf": {
            "fsp": [
                {
                    "fsp_id": 1,
                    "function_mode": "CORR",
                    "frequency_slice_id": 1,
                    "integration_factor": 1,
                    "zoom_factor": 0,
                    "channel_averaging_map": [[0, 2], [744, 0]],
                    "channel_offset": 0,
                    "output_link_map": [[0, 0], [200, 1]],
                },
                {
                    "fsp_id": 2,
                    "function_mode": "CORR",
                    "frequency_slice_id": 2,
                    "integration_factor": 1,
                    "zoom_factor": 1,
                    "zoom_window_tuning": 650000,
                    "channel_averaging_map": [[0, 2], [744, 0]],
                    "channel_offset": 744,
                    "output_link_map": [[0, 4], [200, 5]],
                },
            ],
            "vlbi": {},
        },
        "pss": {},
        "pst": {},
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
        "scan_type": "science_A",
    },
    "tmc": {
        "scan_duration": 10.0,
        "partial_configuration": False,
    },
}


# the only difference between v2.2 and v2.3 is the addition of
# pointing.correction and the minor bump to interface schema version. This is
# better indicated by a dict merge like below than duplicating a full
# configuration.
TMC_CONFIGURE_2_3 = _recursive_merge(
    copy.deepcopy(TMC_CONFIGURE_2_2),
    {
        "interface": "https://schema.skao.int/ska-tmc-configure/2.3",
        "pointing": {"correction": "UPDATE"},
    },
)

# The only difference between v2.3 and v3.0 is the narrowing of allowed
# reference frames. Making a field more restrictive cannot be done in a
# backwards-compatible manner hence this requires a major version.
#
# Note that target ra and dec can still be provided even with
# reference_frame='special', but will be ignored. This example includes both
# ra and dec, inherited from the v2.3 example.
#
# Checking that a valid target_name is set when reference_frame='special' is a
# semantic validation responsibility, not syntactic validation, hence there's
# no unit test for this condition.
TMC_CONFIGURE_3_0 = _recursive_merge(
    copy.deepcopy(TMC_CONFIGURE_2_3),
    {
        "interface": "https://schema.skao.int/ska-tmc-configure/3.0",
        "pointing": {
            "target": {"target_name": "MARS", "reference_frame": "special"}
        },
    },
)

# as per the ADR-99
TMC_CONFIGURE_4_0 = {
    "interface": "https://schema.skao.int/ska-tmc-configure/4.0",
    "transaction_id": "",
    "pointing": {
        "target": {
            "reference_frame": "ICRS",
            "target_name": "Polaris Australis",
            "ra": "21:08:47.92",
            "dec": "-88:57:22.9",
            "ca_offset_arcsec": 0.0,
            "ie_offset_arcsec": 0.0,
        },
        "correction": "UPDATE",
    },
    "dish": {"receiver_band": "1"},
    "csp": {
        "interface": "https://schema.skao.int/ska-csp-configurescan/4.0",
        "common": {
            "config_id": "sbi-mvp01-20200325-00001-science_A",
            "frequency_band": "1",
        },
        "midcbf": {
            "frequency_band_offset_stream1": 80,
            "correlation": {
                "processing_regions": [
                    {
                        "fsp_ids": [1, 2, 3, 4],
                        "receptors": ["SKA063", "SKA001", "SKA100"],
                        "start_freq": 350000000,
                        "channel_width": 13440,
                        "channel_count": 52080,
                        "sdp_start_channel_id": 0,
                        "integration_factor": 1,
                    },
                    {
                        "fsp_ids": [1],
                        "start_freq": 548437600,
                        "channel_width": 13440,
                        "channel_count": 14880,
                        "sdp_start_channel_id": 1,
                        "integration_factor": 10,
                    },
                ]
            },
            "vlbi": {},
        },
    },
    "sdp": {
        "interface": "https://schema.skao.int/ska-sdp-configure/0.4",
        "scan_type": "target:a",
    },
    "tmc": {"scan_duration": 10.0, "partial_configuration": False},
}


# The only difference between v4.0 and v4.1 are the interface ID and
# the addition of pointing groups. Note that pointing.groups and the
# pointing.target coordinates co-exist in this example. When pointing.groups
# is specified, the TMC-Mid implementation will ignore pointing.target.
TMC_CONFIGURE_4_1 = {
    "interface": "https://schema.skao.int/ska-tmc-configure/4.1",
    "transaction_id": "",
    "pointing": {
        "groups": [
            {
                "receptors": ["SKA001", "SKA036", "SKA063"],
                "field": {
                    "target_name": "Cen-A",
                    "reference_frame": "ICRS",
                    "attrs": {
                        "c1": 201.365,
                        "c2": -43.0191667,
                    },
                },
                "trajectory": {
                    "name": "mosaic",
                    "attrs": {
                        "x_offsets": [
                            -5.0,
                            0.0,
                            5.0,
                            -5.0,
                            0.0,
                            5.0,
                            -5.0,
                            0.0,
                            5.0,
                        ],
                        "y_offsets": [
                            5.0,
                            5.0,
                            5.0,
                            0.0,
                            0.0,
                            0.0,
                            -5.0,
                            -5.0,
                            -5.0,
                        ],
                    },
                },
                "projection": {"name": "SSN", "alignment": "ICRS"},
            },
            {
                "receptors": ["SKA100"],
                "field": {
                    "target_name": "Cen-A",
                    "reference_frame": "ICRS",
                    "attrs": {
                        "c1": 201.365,
                        "c2": -43.0191667,
                    },
                },
            },
        ]
    },
}


def get_low_tmc_assignresources_example(version: str):
    """Generate examples for TMC assign resources strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(low_tmc_assignresources_uri(1, 0)):
        return copy.deepcopy(LOW_TMC_ASSIGNRESOURCES_1_0)

    if version.startswith(low_tmc_assignresources_uri(2, 0)):
        return copy.deepcopy(LOW_TMC_ASSIGNRESOURCES_2_0)

    if version.startswith(low_tmc_assignresources_uri(3, 0)):
        return copy.deepcopy(LOW_TMC_ASSIGNRESOURCES_3_0)

    if version.startswith(low_tmc_assignresources_uri(3, 1)):
        return copy.deepcopy(LOW_TMC_ASSIGNRESOURCES_3_1)

    if version.startswith(low_tmc_assignresources_uri(3, 2)):
        return copy.deepcopy(LOW_TMC_ASSIGNRESOURCES_3_2)

    if version.startswith(low_tmc_assignresources_uri(4, 0)):
        return copy.deepcopy(LOW_TMC_ASSIGNRESOURCES_4_0)
    raise ValueError(f"Could not generate example for schema {version})!")


def get_tmc_assignresources_example(version: str):
    """Generate examples for TMC assign resources strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(tmc_assignresources_uri(2, 1)):
        return copy.deepcopy(TMC_ASSIGNRESOURCES_2_1)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_low_tmc_configure_example(version: str):
    """Generate examples for TMC configuration strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(low_tmc_configure_uri(1, 0)):
        return copy.deepcopy(LOW_TMC_CONFIGURE_1_0)

    if version.startswith(low_tmc_configure_uri(2, 0)):
        return copy.deepcopy(LOW_TMC_CONFIGURE_2_0)

    if version.startswith(low_tmc_configure_uri(3, 0)):
        return copy.deepcopy(LOW_TMC_CONFIGURE_3_0)

    if version.startswith(low_tmc_configure_uri(3, 1)):
        return copy.deepcopy(LOW_TMC_CONFIGURE_3_1)

    if version.startswith(low_tmc_configure_uri(3, 2)):
        return copy.deepcopy(LOW_TMC_CONFIGURE_3_2)

    if version.startswith(low_tmc_configure_uri(3, 3)):
        return copy.deepcopy(LOW_TMC_CONFIGURE_3_3)

    if version.startswith(low_tmc_configure_uri(4, 0)):
        return copy.deepcopy(LOW_TMC_CONFIGURE_4_0_PST_SCAN_VOLTAGE_RECORDER)

    if version.startswith(low_tmc_configure_uri(4, 1)):
        return copy.deepcopy(LOW_TMC_CONFIGURE_4_1_PST_SCAN_VOLTAGE_RECORDER)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_low_tmc_releaseresources_example(version: str):
    """Generate examples for tmc resource release strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(low_tmc_releaseresources_uri(1, 0)):
        return copy.deepcopy(LOW_TMC_RELEASERESOURCES_1_0)

    if version.startswith(low_tmc_releaseresources_uri(2, 0)):
        return copy.deepcopy(LOW_TMC_RELEASERESOURCES_2_0)

    if version.startswith(low_tmc_releaseresources_uri(3, 0)):
        return copy.deepcopy(LOW_TMC_RELEASERESOURCES_3_0)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_low_tmc_scan_example(version: str):
    """Generate examples for TMC scan strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(low_tmc_scan_uri(1, 0)):
        return copy.deepcopy(LOW_TMC_SCAN_1_0)

    if version.startswith(low_tmc_scan_uri(2, 0)):
        return copy.deepcopy(LOW_TMC_SCAN_2_0)

    if version.startswith(low_tmc_scan_uri(3, 0)):
        return copy.deepcopy(LOW_TMC_SCAN_3_0)

    if version.startswith(low_tmc_scan_uri(4, 0)):
        return copy.deepcopy(LOW_TMC_SCAN_4_0)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_tmc_scan_example(version: str):
    """Generate examples for TMC Mid scan strings.

    :param version: Version URI of configuration format
    """
    if version.startswith(tmc_scan_uri(2, 1)):
        return copy.deepcopy(TMC_SCAN_2_1)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_tmc_assignedres_example(version: str, empty: bool = False):
    """Generate examples for TMC assigned resources strings.

    :param version: Version URI of configuration format
    :param empty: True if example is for empty sub-array
    """
    if version.startswith(tmc_assignedres_uri(1, 0)):
        if empty:
            return copy.deepcopy(TMC_ASSIGNEDRES_EMPTY_1_0)
        return copy.deepcopy(TMC_ASSIGNEDRES_1_0)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_tmc_releaseresources_example(version: str):
    """Generate examples for tmc resource release strings.

    :param version: Version URI of configuration format
    """

    if version.startswith(tmc_releaseresources_uri(2, 1)):
        return copy.deepcopy(TMC_RELEASERESOURCES_2_1)

    raise ValueError(f"Could not generate example for schema {version})!")


def get_tmc_configure_example(version: str):
    """Generate examples for TMC configuration strings.
    :param version: Version URI of configuration format

    """
    if version == tmc_configure_uri(3, 0):
        return copy.deepcopy(TMC_CONFIGURE_3_0)

    if version == tmc_configure_uri(2, 3):
        return copy.deepcopy(TMC_CONFIGURE_2_3)

    if version == tmc_configure_uri(2, 2):
        return copy.deepcopy(TMC_CONFIGURE_2_2)

    if version == tmc_configure_uri(2, 1):
        return copy.deepcopy(TMC_CONFIGURE_2_1)

    if version == tmc_configure_uri(4, 0):
        return copy.deepcopy(TMC_CONFIGURE_4_0)

    if version == tmc_configure_uri(4, 1):
        return copy.deepcopy(TMC_CONFIGURE_4_1)

    raise ValueError(f"Could not generate example for schema {version})!")

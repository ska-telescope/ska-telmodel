"""
Refactoring to move from

   bla.add_field(x, And(a, if_strict(b)), ...)

to

   bla.add_opt_field(x, a, check_strict=b,...)

"""

import sys

from bowler import Query
from fissix import fixer_util, pygram


def tmschema_strict_check(node, cap, fn):
    # Remove both checks
    check = cap["check"]
    check_strict = cap["check_strict"]
    check.remove()
    check_strict.remove()

    # Replace top-level term by simple check
    arglist = cap["check_par"].parent
    cap["check_par"].replace(check)

    # Add check_strict=... in front of first keyword argument
    for i, arg in enumerate(arglist.children):
        if arg.type == pygram.python_symbols.argument:
            i -= 1
            break

    # Insert
    arglist.insert_child(
        i, fixer_util.KeywordArg(fixer_util.Name("check_strict"), check_strict)
    )
    arglist.insert_child(i, fixer_util.Comma())


Query(sys.argv[1]).select(
    """
simple_stmt<
  power<
    NAME
    trailer< '.' ('add_field' | 'add_opt_field') >
    trailer< '(' arglist<
      any
      ','
      check_par=power<
        'And'
        trailer< '(' arglist<
          check=any
          ','
          power<
           'if_strict'
           trailer< '(' check_strict=any ')' >
          >
          ','*
        > ')' >
      >
      any*
    > ')' >
  > any*
>"""
).modify(tmschema_strict_check).execute()

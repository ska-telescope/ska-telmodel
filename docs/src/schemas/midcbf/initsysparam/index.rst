ska-mid.cbf-initsysparam
========================
Examples for the different versions of the initsysparam schema

.. toctree::
  :maxdepth: 1
  :caption: MID CBF initsysparam examples

  ska-mid-cbf-initsysparam-1.1
  ska-mid-cbf-initsysparam-1.0

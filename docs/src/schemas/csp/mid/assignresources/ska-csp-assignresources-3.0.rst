CSP assignresources 3.0
=======================

JSON schema and example for CSP Mid assignresources

.. ska-schema:: https://schema.skao.int/ska-csp-assignresources/3.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-csp-assignresources/3.0

       Example JSON
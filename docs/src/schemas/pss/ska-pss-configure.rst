ska-pss-configure
=====================
Examples for the different versions of the configure schema

.. toctree::
  :maxdepth: 1
  :caption: PSS configure examples

  ska-pss-configure-1.0
  ska-pss-configure-0.1 

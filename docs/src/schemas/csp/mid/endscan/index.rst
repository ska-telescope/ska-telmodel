ska-csp-endscan
===============

Examples for the different versions of the endscan schema

.. toctree::
  :maxdepth: 1
  :caption: MID CSP endscan examples

  ska-csp-endscan-2.3
  ska-csp-endscan-2.2

ska-csp-delaymodel
==================

Examples for the different versions of the delay model schema

.. toctree::
  :maxdepth: 1
  :caption: MID CSP delay model examples

  ska-csp-delaymodel-3.0
  ska-csp-delaymodel-2.2

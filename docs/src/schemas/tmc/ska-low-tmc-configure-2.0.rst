
.. ska-schema:: https://schema.skao.int/ska-low-tmc-configure/2.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-tmc-configure/2.0

       Example JSON.
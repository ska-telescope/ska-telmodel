from functools import partial
from typing import List, Union

from .._common import interface_uri

TMC_VERSION_PREFIX = "https://schema.skao.int/"

LOW_TMC_SCAN_PREFIX = TMC_VERSION_PREFIX + "ska-low-tmc-scan/"
LOW_TMC_ASSIGNRESOURCES_PREFIX = (
    TMC_VERSION_PREFIX + "ska-low-tmc-assignresources/"
)
LOW_TMC_CONFIGURE_PREFIX = TMC_VERSION_PREFIX + "ska-low-tmc-configure/"
LOW_TMC_RELEASERESOURCES_PREFIX = (
    TMC_VERSION_PREFIX + "ska-low-tmc-releaseresources/"
)
TMC_RELEASERESOURCES_PREFIX = TMC_VERSION_PREFIX + "ska-tmc-releaseresources/"

TMC_ASSIGNEDRES_PREFIX = TMC_VERSION_PREFIX + "ska-low-tmc-assignedresources/"
# for MVP-Mid PI#16 onwards
TMC_SCAN_PREFIX = (
    TMC_VERSION_PREFIX + "ska-tmc-scan/"
)  # for MVP-Mid PI#16 onwards
TMC_CONFIGURE_PREFIX = TMC_VERSION_PREFIX + "ska-tmc-configure/"
TMC_ASSIGNRESOURCES_PREFIX = TMC_VERSION_PREFIX + "ska-tmc-assignresources/"
# JSON strings for MVP-Low for PI#10
tmc_assignedres_uri = partial(interface_uri, TMC_ASSIGNEDRES_PREFIX)
low_tmc_assignresources_uri = partial(
    interface_uri, LOW_TMC_ASSIGNRESOURCES_PREFIX
)
low_tmc_configure_uri = partial(interface_uri, LOW_TMC_CONFIGURE_PREFIX)

low_tmc_scan_uri = partial(interface_uri, LOW_TMC_SCAN_PREFIX)
low_tmc_releaseresources_uri = partial(
    interface_uri, LOW_TMC_RELEASERESOURCES_PREFIX
)
tmc_releaseresources_uri = partial(interface_uri, TMC_RELEASERESOURCES_PREFIX)

# JSON strings for MVP-Mid for PI#16 onwards
tmc_scan_uri = partial(interface_uri, TMC_SCAN_PREFIX)
tmc_assignresources_uri = partial(interface_uri, TMC_ASSIGNRESOURCES_PREFIX)
tmc_configure_uri = partial(interface_uri, TMC_CONFIGURE_PREFIX)


_PREFIXES = [
    LOW_TMC_ASSIGNRESOURCES_PREFIX,
    TMC_ASSIGNRESOURCES_PREFIX,
    LOW_TMC_CONFIGURE_PREFIX,
    LOW_TMC_SCAN_PREFIX,
    TMC_SCAN_PREFIX,
    LOW_TMC_RELEASERESOURCES_PREFIX,
    TMC_CONFIGURE_PREFIX,
    TMC_RELEASERESOURCES_PREFIX,
]


def check_tmc_interface_version(
    version: str,
    allowed_prefixes: Union[str, List[str]] = _PREFIXES,
) -> str:
    """
    Check TMC interface version.

    Checks that the interface URI has one of the allowed prefixes. If it does,
    the version number is returned. If not, a ValueError exception is raised.

    :param version: TMC interface URI
    :param allowed_prefixes: allowed URI prefix(es)
    :returns: version number

    """
    if not isinstance(allowed_prefixes, list):
        allowed_prefixes = [allowed_prefixes]

    # Valid?
    for prefix in allowed_prefixes:
        if version.startswith(prefix):
            number = version[len(prefix) :]
            return number

    raise ValueError(f"TMC interface URI '{version}' not allowed")

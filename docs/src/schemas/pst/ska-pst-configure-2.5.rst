JSON schema and example for Configure version 2.5
=================================================
.. ska-schema:: https://schema.skao.int/ska-pst-configure/2.5
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.5 low_pst_scan_ft

      Example (LOW PST configuration for FLOW-THROUGH scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.5 low_pst_scan_pt

      Example (LOW PST configuration for PULSAR TIMING scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.5 low_pst_scan_ds

      Example (LOW PST configuration for DYNAMIC SPECTRUM scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.5 low_pst_scan_vr

      Example (LOW PST configuration for voltage recorder scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.5 mid_pst_scan_ft

      Example (MID PST configuration for FLOW-THROUGH scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.5 mid_pst_scan_pt

      Example (MID PST configuration for PULSAR TIMING scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.5 mid_pst_scan_ds

      Example (MID PST configuration for DYNAMIC SPECTRUM scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.5 mid_pst_scan_vr

      Example (MID PST configuration for voltage recorder scan 2.5)

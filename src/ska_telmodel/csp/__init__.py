# functions returning examples of JSON strings accepted by
# LOWCSP subarray commands

from .examples import (
    get_csp_assignresources_example,
    get_csp_config_example,
    get_csp_delaymodel_example,
    get_csp_endscan_example,
    get_csp_low_delaymodel_example,
    get_csp_releaseresources_example,
    get_csp_scan_example,
)
from .interface import make_csp_config
from .low_examples import (
    get_low_csp_assignresources_example,
    get_low_csp_configure_example,
    get_low_csp_releaseresources_example,
    get_low_csp_scan_example,
)
from .low_schema import (
    get_low_csp_assignresources_schema,
    get_low_csp_configure_schema,
    get_low_csp_releaseresources_schema,
    get_low_csp_scan_schema,
)

# Strings encoding the LOWCSP schema URI names
from .low_version import (
    LOWCSP_ASSIGNRESOURCES_PREFIX,
    LOWCSP_CONFIGURE_PREFIX,
    LOWCSP_ENDSCAN_PREFIX,
    LOWCSP_RELEASERESOURCES_PREFIX,
    LOWCSP_SCAN_PREFIX,
)
from .schema import (
    get_csp_assignresources_schema,
    get_csp_config_schema,
    get_csp_delaymodel_schema,
    get_csp_endscan_schema,
    get_csp_low_delaymodel_schema,
    get_csp_releaseresources_schema,
    get_csp_scan_schema,
)
from .version import (
    CSP_ASSIGNRESOURCES_PREFIX,
    CSP_CONFIG_PREFIX,
    CSP_CONFIGSCAN_PREFIX,
    CSP_DELAYMODEL_PREFIX,
    CSP_ENDSCAN_PREFIX,
    CSP_LOW_DELAYMODEL_PREFIX,
    CSP_MID_DELAYMODEL_PREFIX,
    CSP_RELEASERESOURCES_PREFIX,
    CSP_SCAN_PREFIX,
)

__all__ = [
    "get_csp_assignresources_example",
    "get_csp_config_example",
    "get_csp_scan_example",
    "get_csp_endscan_example",
    "get_csp_releaseresources_example",
    "get_csp_delaymodel_example",
    "get_csp_low_delaymodel_example",
    "make_csp_config",
    "get_csp_assignresources_schema",
    "get_csp_config_schema",
    "get_csp_scan_schema",
    "get_csp_endscan_schema",
    "get_csp_releaseresources_schema",
    "get_csp_delaymodel_schema",
    "get_csp_low_delaymodel_schema",
    "get_low_csp_assignresources_example",
    "get_low_csp_configure_example",
    "get_low_csp_scan_example",
    "get_low_csp_releaseresources_example",
    "get_low_csp_assignresources_schema",
    "get_low_csp_configure_schema",
    "get_low_csp_scan_schema",
    "get_low_csp_releaseresources_schema",
    "LOWCSP_ASSIGNRESOURCES_PREFIX",
    "LOWCSP_CONFIGURE_PREFIX",
    "LOWCSP_RELEASERESOURCES_PREFIX",
    "LOWCSP_SCAN_PREFIX",
    "LOWCSP_ENDSCAN_PREFIX",
    "CSP_ASSIGNRESOURCES_PREFIX",
    "CSP_CONFIG_PREFIX",
    "CSP_CONFIGSCAN_PREFIX",
    "CSP_SCAN_PREFIX",
    "CSP_ENDSCAN_PREFIX",
    "CSP_RELEASERESOURCES_PREFIX",
    "CSP_DELAYMODEL_PREFIX",
    "CSP_MID_DELAYMODEL_PREFIX",
    "CSP_LOW_DELAYMODEL_PREFIX",
]

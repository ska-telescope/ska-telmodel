import tempfile
from pathlib import Path

from ska_telmodel.data import TMData


def test_list_keys_single_file():
    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        Path(dirname, "test.file").touch()
        sources = TMData(source_uris=[f"file://{dirname}"], update=True)
        assert sources._sources[0].list_keys("test.file") == ["test.file"]

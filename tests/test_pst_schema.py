import pytest

from ska_telmodel.pst.examples import get_pst_config_example
from ska_telmodel.pst.version import (
    PST_CONFIGURE_PREFIX,
    check_interface_version,
)
from ska_telmodel.schema import validate


@pytest.mark.parametrize(
    "pst_ver,scan_type,expected_observation_mode",
    [
        ("2.4", "pst_scan_vr", "VOLTAGE_RECORDER"),
        ("2.4", "pst_scan_ft", "FLOW_THROUGH"),
        ("2.4", "pst_scan_ds", "DYNAMIC_SPECTRUM"),
        ("2.4", "pst_scan_pt", "PULSAR_TIMING"),
        ("2.5", "pst_scan_vr", "VOLTAGE_RECORDER"),
        ("2.5", "pst_scan_ft", "FLOW_THROUGH"),
        ("2.5", "pst_scan_ds", "DYNAMIC_SPECTRUM"),
        ("2.5", "pst_scan_pt", "PULSAR_TIMING"),
        ("2.4", "low_pst_scan_vr", "VOLTAGE_RECORDER"),
        ("2.4", "low_pst_scan_ft", "FLOW_THROUGH"),
        ("2.4", "low_pst_scan_ds", "DYNAMIC_SPECTRUM"),
        ("2.4", "low_pst_scan_pt", "PULSAR_TIMING"),
        ("2.5", "low_pst_scan_vr", "VOLTAGE_RECORDER"),
        ("2.5", "low_pst_scan_ft", "FLOW_THROUGH"),
        ("2.5", "low_pst_scan_ds", "DYNAMIC_SPECTRUM"),
        ("2.5", "low_pst_scan_pt", "PULSAR_TIMING"),
        ("2.4", "mid_pst_scan_vr", "VOLTAGE_RECORDER"),
        ("2.4", "mid_pst_scan_ft", "FLOW_THROUGH"),
        ("2.4", "mid_pst_scan_ds", "DYNAMIC_SPECTRUM"),
        ("2.4", "mid_pst_scan_pt", "PULSAR_TIMING"),
        ("2.5", "mid_pst_scan_vr", "VOLTAGE_RECORDER"),
        ("2.5", "mid_pst_scan_ft", "FLOW_THROUGH"),
        ("2.5", "mid_pst_scan_ds", "DYNAMIC_SPECTRUM"),
        ("2.5", "mid_pst_scan_pt", "PULSAR_TIMING"),
    ],
)
def test_pst_configure_schema(
    pst_ver: str, scan_type: str, expected_observation_mode: str
) -> None:
    uri = PST_CONFIGURE_PREFIX + pst_ver
    scan_config = get_pst_config_example(uri, scan_type)
    assert (
        scan_config["interface"] == uri
    ), f"expected {scan_config['interface']} to be {uri}"
    validate(uri, scan_config, 2)
    observsation_mode = scan_config["pst"]["scan"]["observation_mode"]
    assert (
        observsation_mode == expected_observation_mode
    ), f"expected {observsation_mode=} to be {expected_observation_mode=}"
    if scan_type.startswith("mid_"):
        # assert frequency band is not Low
        assert scan_config["common"]["frequency_band"] != "low"
    else:
        assert scan_config["common"]["frequency_band"] == "low"


@pytest.mark.parametrize("pst_ver", ["1.0"])
def test_pst_schema_validation_with_invalid_version_low(pst_ver: str):
    """
    Test to verify invalid PST version
    """
    uri = PST_CONFIGURE_PREFIX + pst_ver
    with pytest.raises(
        ValueError, match=r"Could not generate example for schema*"
    ):
        get_pst_config_example(uri, "pst_scan_ft")


@pytest.mark.parametrize("pst_ver", ["1.0"])
def test_pst_schema_validation_with_invalid_version_mid(pst_ver: str):
    """
    Test to verify invalid PST version
    """
    uri = PST_CONFIGURE_PREFIX + pst_ver
    with pytest.raises(
        ValueError, match=r"Could not generate example for schema*"
    ):
        get_pst_config_example(uri, "mid_pst_scan_ft")


@pytest.mark.parametrize("pst_ver", ["1.0"])
def test_pst_schema_validation_with_invalid_prefix(pst_ver):
    """
    Test to verify invalid PST version
    """
    invalid_uri = "https://schema.skao.int/ska-pst-config/" + pst_ver
    with pytest.raises(ValueError, match=r"PST interface URI not allowed*"):
        pst_config_beam_ft = get_pst_config_example(invalid_uri, "pst_scan_ft")
        validate(invalid_uri, pst_config_beam_ft, 2)


@pytest.mark.parametrize("pst_ver", ["2.4", "2.5"])
def test_pst_check_interface_version(pst_ver):
    """
    Test to verify invalid PST version
    """
    uri = PST_CONFIGURE_PREFIX + pst_ver

    version_num = check_interface_version(uri, PST_CONFIGURE_PREFIX)
    assert version_num == pst_ver

"""Version 0.3 of SDP schema"""

from inspect import cleandoc

from schema import Literal, Optional, Or

from ska_telmodel._common import TMSchema
from ska_telmodel.sdp.common import (
    get_eb_name_schema,
    get_pb_name_schema,
    get_txn_name_schema,
)

from .v0_1 import get_scan_channels
from .v0_2 import get_sdp_recvaddrs

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_scan",
    "get_sdp_recvaddrs",
]


def get_scan_type(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "Scan type",
        version,
        strict,
        as_reference=True,
        description=cleandoc(
            """
            A scan configuration for SDP. Once AssignResources has been
            performed successfully, subsequent Configure commands can
            select from these scan types in order to coordinate SDP
            with other sub-systems participating in the observation -
            for instance to switch between targets, or perform special
            calibration scans.
            """
        ),
    )

    schema.add_field("scan_type_id", str)
    schema.add_opt_field(
        "reference_frame",
        str,
        check_strict="ICRS",
        description=cleandoc(
            """
            Specification of the reference frame or system for a set
            of pointing coordinates (see ADR-49)
            """
        ),
    )
    schema.add_opt_field(
        "ra", str, description="Right Ascension in degrees (see ADR-49)"
    )
    schema.add_opt_field(
        "dec", str, description="Declination in degrees (see ADR-49)"
    )
    schema.add_opt_field("channels", [get_scan_channels(version, strict)])
    return schema


def get_script_schema(name: str, version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        name,
        version,
        strict,
        description=cleandoc(
            """
            Data object within the Processing Block which describes
            the workflow that should be executed along with its
            configuration, to be assigned to a subarray.
            This is used to select the processing script from a set
            of scripts that have been validated to run on the SDP.
            """
        ),
    )
    schema.add_field(
        "kind",
        str,
        check_strict=Or("realtime", "batch"),
        description="The kind of processing script (realtime or batch)",
    )
    schema.add_field(
        "name", str, description="The name of the processing script"
    )
    schema.add_field(
        "version",
        str,
        description="Version of the processing script. "
        "Uses semantic versioning.",
    )
    return schema


def get_pb_dependency(version: str, strict: bool) -> TMSchema:
    return TMSchema.new(
        "Processing block dependency",
        version,
        strict,
        schema={
            "pb_id": get_pb_name_schema(version, strict),
            "kind": [str],
        },
    )


def get_processing_block(
    version: str, strict: bool, script_name="workflow"
) -> TMSchema:
    schema = TMSchema.new(
        "Processing block",
        version,
        strict,
        as_reference=True,
        description=cleandoc(
            """
            A Processing Block is an atomic unit of data processing
            for the purpose of SDP's internal scheduler. Each PB
            references a processing script and together with the
            associated execution block provides all parameters
            necessary to carry out scheduling - both on TM's side for
            observation planning and on SDP's side - as well as enable
            processing to locate all required inputs once it is in
            progress.

            PBs are used for both real-time and deferred, batch,
            processing. An execution block will often contain many
            Processing Blocks, for example for ingest,
            self-calibration and Data Product preparation.
            """
        ),
    )

    schema.add_field(
        "pb_id",
        get_pb_name_schema(version, strict),
        description="Unique identifier for this processing block.",
    )

    schema.add_field(
        script_name,
        get_script_schema(script_name, version, strict),
        description=cleandoc(
            """
            Specification of the workflow to be executed along with
            configuration parameters for the workflow.
            """
        ),
    )
    schema.add_opt_field(
        "parameters",
        dict,
        description=cleandoc(
            """
            Configuration parameters needed to execute the workflow. As these
            parameters will be workflow specific, this is left as an
            object to be specified by the workflow definition.
            """
        ),
    )
    schema.add_opt_field(
        "dependencies",
        [get_pb_dependency(version, strict)],
        description=cleandoc(
            """
            A dependency between processing blocks means that one
            processing block requires something from the other
            processing block to run - typically an intermediate Data
            Product.  This generally means that

            1. The dependent processing block might only be able
               to start once the dependency has been fulfilled

            2. Data associated with the dependency must be kept alive
               until the dependent processing block is finished.

            As processing blocks might have many different outputs,
            the dependency "kind" can be used to specify how this
            dependency is meant to be interpreted
            (e.g. "visibilities", "calibration"...)
            """
        ),
    )
    return schema


def assign_res_common(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "SDP assign resources",
        version,
        strict,
        description=cleandoc(
            """
            Used for assigning resources to an SDP subarray.

            As concrete resource usage for the SDP depend strongly on
            the underlying processing script, this fully parameterises all
            processing blocks to be executed. This especially means
            that in contrast to most other sub-systems, SDP processing
            deployments might persist across scans (and scan
            configuration) boundaries.
            """
        ),
    )
    schema.add_opt_field("interface", str)
    schema.add_opt_field(
        "transaction_id", get_txn_name_schema(version, strict)
    )
    return schema


def get_sdp_assignres(version: str, strict: bool) -> TMSchema:
    schema = assign_res_common(version, strict)

    schema.add_field(
        "eb_id",
        get_eb_name_schema(version, strict),
        description="Execution block ID to associate with processing",
    )
    schema.add_opt_field(
        "max_length",
        float,
        description=cleandoc(
            """
            Hint about the maximum observation length to support by
            the SDP. Used for ensuring that enough buffer capacity is
            available to capture measurements. Resources assignment
            might fail if we do not have enough space to guarantee
            that all data could be captured.
            """
        ),
    )
    schema.add_field(
        "scan_types",
        [get_scan_type(version, strict)],
        description="Scan types to be supported on subarray",
    )
    schema.add_field(
        "processing_blocks", [get_processing_block(version, strict)]
    )
    return schema


def get_sdp_configure(version: str, strict: bool) -> TMSchema:
    schema = TMSchema.new(
        "SDP configure",
        version,
        strict,
        description=cleandoc(
            """
            Configures an SDP subarray for a number of scans of a certain
            previously-assigned type. See resource assignment.
            """
        ),
    )
    schema.add_opt_field("interface", str)
    schema.add_opt_field(
        "transaction_id", get_txn_name_schema(version, strict)
    )
    schema.add_field("scan_type", str)
    schema.add_opt_field("new_scan_types", [get_scan_type(version, strict)])
    return schema


def get_sdp_scan(version: str, strict: bool) -> TMSchema:
    return TMSchema.new(
        "SDP scan",
        version,
        strict,
        schema={
            Optional("interface"): str,
            Optional("transaction_id"): get_txn_name_schema(version, strict),
            Literal("scan_id", description="ID associated with new scan"): int,
        },
        description="Indicates to SDP that a new scan is about to start",
    )

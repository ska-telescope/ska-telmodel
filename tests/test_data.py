import io
import json
import logging
import pathlib
import tarfile
import tempfile
from hashlib import sha1

import gitlab
import pytest

import ska_telmodel.data.sources
from ska_telmodel.data import TMData


@pytest.fixture(scope="function")
def tmdata_mem():
    sources = [
        "mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "mem://?tango/ska_wide/SKAMaster.yaml=test: test%0Atest2: test2",
        'mem://?tango/ska_wide/test.json={"asd":"asd"}',
    ]
    yield TMData(sources)


@pytest.fixture(scope="function")
def tmdata_file():
    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname, "tango", "ska_wide")
        parent.mkdir(parents=True)
        with open(parent / "SKAMaster.yaml", "w") as f:
            print("test: test", file=f)
            print("test2: test2", file=f)
        with open(parent / "test.json", "w") as f:
            print('{"asd":"asd"}', file=f)
        yield TMData([f"file://{dirname}"])


def _add_file_to_tar(tar, name, contents):
    bio = io.BytesIO(contents.encode("utf8"))
    info = tarfile.TarInfo(name)
    info.size = len(contents)
    tar.addfile(info, bio)


def _add_dir_to_tar(tar, name):
    info = tarfile.TarInfo(name)
    info.type = tarfile.DIRTYPE
    info.mode = 0o755
    tar.addfile(info)


def _make_tarfile(bio, project_name=None, commit_id=None, path="tmdata"):
    if project_name is not None:
        prefix = pathlib.Path(project_name).name
        prefix += f"-{commit_id}-{commit_id}-{path.replace('/', '-')}/"
    else:
        prefix = ""
    with tarfile.TarFile(mode="w", fileobj=bio) as f:
        _add_dir_to_tar(f, f"{prefix}")
        _add_dir_to_tar(f, f"{prefix}{path}/")
        _add_dir_to_tar(f, f"{prefix}{path}/tango/")
        _add_dir_to_tar(f, f"{prefix}{path}/tango/ska_wide/")
        _add_file_to_tar(
            f,
            f"{prefix}{path}/tango/ska_wide/SKAMaster.yaml",
            "test: test\ntest2: test2\n",
        )
        _add_file_to_tar(
            f, f"{prefix}{path}/tango/ska_wide/test.json", '{"asd":"asd"}\n'
        )


def make_pseudo_gitlab(tmdata_path):
    """
    Created an object mocking the Gitlab python API

    :param tmdata_path: The path in Gitlab repository where mock
      telescope model data is placed
    """

    # Repository contents
    project_name = "ska-telescope/ska-telmodel-pseudo"
    project_id = 1234
    commit_id = sha1(b"commit").hexdigest()
    if tmdata_path:
        tmdata_prefix = tmdata_path + "/"
    else:
        tmdata_prefix = ""
    tree = [
        {
            "name": "SKAMaster.yaml",
            "path": f"{tmdata_prefix}tango/ska_wide/SKAMaster.yaml",
            "type": "blob",
            "id": commit_id,
        },
        {
            "name": "test.json",
            "path": f"{tmdata_prefix}tango/ska_wide/test.json",
            "type": "blob",
            "id": commit_id,
        },
    ]

    # Emulate Gitlab
    class PseudoGitlab:
        @property
        def projects(self):
            return PseudoProjects()

        def http_get(self, url, query_data, raw, streamed):
            assert url == f"/projects/{project_id}/repository/archive"
            assert query_data == dict(sha=commit_id, path=tmdata_path)
            assert raw
            assert streamed
            return PseudoResult()

    class PseudoProjects:
        def get(self, project_name):
            assert project_name == "ska-telescope/ska-telmodel-pseudo"
            return PseudoProject()

    class PseudoProject:
        @property
        def branches(self):
            return PseudoBranches()

        @property
        def commits(self):
            return PseudoCommits()

        @property
        def encoded_id(self):
            return 1234

        def repository_tree(self, path, ref, recursive, all):
            assert path == tmdata_path
            assert ref == commit_id
            assert recursive
            assert all
            return tree

    class PseudoBranches:
        def list(self):
            return [PseudoBranch()]

    class PseudoBranch:
        @property
        def attributes(self):
            return {"default": True}

        @property
        def name(self):
            return "main"

    class PseudoCommits:
        def get(self, ref):
            assert ref == "main"
            return PseudoCommit()

    class PseudoCommit:
        @property
        def id(self):
            return sha1(b"commit").hexdigest()

        pass

    class PseudoResult:
        def _add_dir(self, tar, name):
            info = tarfile.TarInfo(name)
            info.type = tarfile.DIRTYPE
            info.mode = 0o755
            tar.addfile(info)

        def _add_file(self, tar, name, contents):
            bio = io.BytesIO(contents.encode("utf8"))
            info = tarfile.TarInfo(name)
            info.size = len(contents)
            tar.addfile(info, bio)

        def iter_content(self, chunk_size):
            bio = io.BytesIO()
            _make_tarfile(bio, project_name, commit_id, tmdata_path)
            yield bio.getvalue()

    return PseudoGitlab()


@pytest.fixture(scope="function")
def tmdata_gitlab():
    with tempfile.TemporaryDirectory("ska-telmodel-cache") as dirname:
        # Yield the telescope model data object, configured to use our
        # pseudo-Gitlab object. We add various versions that all boil
        # down to the same thing (pinned, unpinned, with explicit
        # branch...) to touch different codepaths, especially related
        # to caching. In test_tmdata_get_sources we will check that
        # they all end up equivalent.
        commit_id = sha1(b"commit").hexdigest()
        gitlab_prefix = "gitlab://gitlab.com/ska-telescope"
        sources = [
            f"{gitlab_prefix}/ska-telmodel-pseudo?~{commit_id}#tmdata",
            f"{gitlab_prefix}/ska-telmodel-pseudo?main#tmdata",
            f"{gitlab_prefix}/ska-telmodel-pseudo#tmdata",
            f"{gitlab_prefix}/ska-telmodel-pseudo#tmdata",
        ]
        gl = make_pseudo_gitlab("tmdata")
        yield TMData(
            sources,
            backend_pars={
                "gitlab": dict(
                    gl=gl, try_nexus=False, env={"SKA_TELMODEL_CACHE": dirname}
                )
            },
        )


# A Gitlab API that doesn't have any projects
class EmptyGitlab:
    @property
    def projects(self):
        return EmptyProjects()


class EmptyProjects:
    def get(self, project_name):
        raise gitlab.exceptions.GitlabGetError()


def tmdata_nexus(backend):
    """Check that we check Nexus for refs"""

    # Repository contents
    commit_id = sha1(b"commit").hexdigest()
    tree = [
        {"name": "tmdata", "path": "tmdata", "type": "dir", "id": commit_id},
        {
            "name": "test.json",
            "path": "tmdata/tango/ska_wide/test.json",
            "type": "blob",
            "id": commit_id,
        },
        {
            "name": "test.json",
            "path": "tmdata/tango/ska_wide/invalid",
            "type": "blob",
            "id": commit_id,
        },
        {
            "name": "SKAMaster.yaml",
            "path": "tmdata/tango/ska_wide/SKAMaster.yaml",
            "type": "blob",
            "id": commit_id,
        },
        {
            "name": "unrelated",
            "path": "unrelated",
            "type": "blob",
            "id": commit_id,
        },
    ]

    # Set up a temporary directory as source for "Nexus" data
    with tempfile.TemporaryDirectory("test-telmodel-nexus") as dirname:
        with tempfile.TemporaryDirectory("test-telmodel-cache") as cachedir:
            path = pathlib.Path(
                dirname, "gitlab.com/ska-telescope/ska-telmodel-pseudo/tmdata"
            )
            path.mkdir(parents=True)
            (path / f"~{commit_id}").mkdir()

            # Populate with data
            with open(path / "main", "w") as f:
                print(commit_id, file=f)
            with open(path / f"~{commit_id}/tmtree.json", "w") as f:
                json.dump(tree, f)
            with open(path / f"~{commit_id}/tmdata.tar.gz", "wb") as f:
                _make_tarfile(f)

            # Create TMData object
            gitlab_prefix = f"{backend}://gitlab.com/ska-telescope"
            sources = [
                f"{gitlab_prefix}/ska-telmodel-pseudo?~{commit_id}#tmdata",
                f"{gitlab_prefix}/ska-telmodel-pseudo?main#tmdata",
            ]
            pars = {
                backend: dict(
                    gl=EmptyGitlab(),
                    env={
                        "SKA_TELMODEL_NEXUS": f"file://{dirname}",
                        "SKA_TELMODEL_CACHE": cachedir,
                    },
                )
            }

            for f in (path / f"~{commit_id}").glob("*"):
                print("Existing:", f)

            yield TMData(sources, backend_pars=pars)


@pytest.fixture(scope="function")
def tmdata_gitlab_car():
    for tmdata in tmdata_nexus("gitlab"):
        yield tmdata


@pytest.fixture(scope="function")
def tmdata_car():
    for tmdata in tmdata_nexus("car"):
        yield tmdata


@pytest.fixture(scope="function")
def tmdata_car_cache():
    for tmdata in tmdata_nexus("car"):
        # List + get all entries to make sure cache is populated
        for key in tmdata:
            tmdata[key].get()
        # Construct new TMData object that shares environment with existing one
        # (and therefore uses the cache)
        yield TMData(
            tmdata.get_sources(),
            backend_pars={
                "car": dict(gl=EmptyGitlab(), env=tmdata._sources[0]._env)
            },
        )


@pytest.fixture(scope="function")
def tmdata_all(
    tmdata_mem,
    tmdata_file,
    tmdata_gitlab,
    tmdata_gitlab_car,
    tmdata_car,
    tmdata_car_cache,
):
    yield {
        "mem": tmdata_mem,
        "file": tmdata_file,
        "gitlab": tmdata_gitlab,
        "gitlab_car": tmdata_gitlab_car,
        "car": tmdata_car,
        "car_cache": tmdata_car_cache,
    }


@pytest.mark.parametrize(
    "backend", ["mem", "file", "gitlab", "gitlab_car", "car", "car_cache"]
)
def test_tmdata_exists(tmdata_all, backend):
    """Check that we can list keys"""

    tmdata = tmdata_all[backend]

    assert "tango/ska_wide/SKAMaster.yaml" in tmdata
    assert "tango/ska_wide/test.json" in tmdata
    assert "tango/doesnt_exist.bla" not in tmdata
    assert "ska_wide/SKAMaster.yaml" in tmdata["tango"]
    assert "ska_wide/test.json" in tmdata["tango"]
    assert "doesnt_exist.bla" not in tmdata["tango"]
    assert "SKAMaster.yaml" in tmdata["tango"]["ska_wide"]
    assert "test.json" in tmdata["tango/ska_wide"]


@pytest.mark.parametrize(
    "backend", ["mem", "file", "gitlab", "gitlab_car", "car", "car_cache"]
)
def test_tmdata_list(tmdata_all, backend):
    """Check that we can list keys"""

    tmdata = tmdata_all[backend]

    keys = set(tmdata)
    assert keys == {
        "tango/ska_wide/SKAMaster.yaml",
        "tango/ska_wide/test.json",
    }

    keys2 = list(tmdata["tango"])
    assert keys2 == ["ska_wide/SKAMaster.yaml", "ska_wide/test.json"]

    keys3 = list(tmdata["tango/ska_wide"])
    assert keys3 == ["SKAMaster.yaml", "test.json"]

    keys4 = set(tmdata["tango/doesnt_exist"])
    assert keys4 == set()


@pytest.mark.parametrize(
    "backend", ["mem", "file", "gitlab", "gitlab_car", "car", "car_cache"]
)
def test_tmdata_get(tmdata_all, backend):
    """Check that we can get objects"""

    tmdata = tmdata_all[backend]
    expected = b"test: test\ntest2: test2\n"
    assert tmdata["tango/ska_wide/SKAMaster.yaml"].get() == expected
    assert tmdata["tango/ska_wide"]["SKAMaster.yaml"].get() == expected
    assert tmdata["tango"]["ska_wide"]["SKAMaster.yaml"].get() == expected
    assert tmdata["tango"]["ska_wide/SKAMaster.yaml"].get() == expected

    expected2 = b'{"asd":"asd"}\n'
    assert tmdata["tango/ska_wide/test.json"].get() == expected2
    assert tmdata["tango/ska_wide"]["test.json"].get() == expected2
    assert tmdata["tango"]["ska_wide"]["test.json"].get() == expected2
    assert tmdata["tango"]["ska_wide/test.json"].get() == expected2


@pytest.mark.parametrize(
    "backend", ["mem", "file", "gitlab", "gitlab_car", "car", "car_cache"]
)
def test_tmdata_open(tmdata_all, backend):
    """Check that we can open objects"""

    tmdata = tmdata_all[backend]
    with tmdata["tango/ska_wide/test.json"].open() as f:
        contents = json.load(f)
    assert contents == {"asd": "asd"}


@pytest.mark.parametrize(
    "backend", ["mem", "file", "gitlab", "gitlab_car", "car"]
)
def test_tmdata_copy(tmdata_all, backend):
    """Check that we can copy objects to the file system"""

    tmdata = tmdata_all[backend]
    with tempfile.TemporaryDirectory("test-tmdata-copy") as dirname:
        # Copy to temporary file
        out_file = pathlib.Path(dirname, "test.json")
        tmdata["tango/ska_wide/test.json"].copy(out_file)

        # Check contents
        with open(out_file) as f:
            contents = json.load(f)
        assert contents == {"asd": "asd"}


def test_tmdata_yaml(tmdata_mem):
    """Check that we can load YAML"""
    expected_d = {"test": "test", "test2": "test2"}
    assert tmdata_mem["tango/ska_wide/SKAMaster.yaml"].get_dict() == expected_d


def test_tmdata_json(tmdata_mem):
    """Check that we can load JSON"""
    expected_d = {"asd": "asd"}
    assert tmdata_mem["tango/ska_wide/test.json"].get_dict() == expected_d


def test_tmdata_mem_invalid_key(caplog):
    """
    Check that we complain if we pass an invalid key to the memory
    backend
    """

    sources = ["mem://?tango/ska_wide/test=bogus"]
    caplog.set_level(logging.WARNING)
    list(TMData(sources))
    assert "is not a valid telescope model key" in caplog.text


def test_tmdata_unknown_suffix():
    """Check that we complain if key suffix is unknown"""

    sources = ["mem://?tango/ska_wide/test.unknown=bogus"]
    with pytest.raises(
        ValueError, match=r"Cannot deserialise object with suffix \.unknown!"
    ):
        TMData(sources)["tango/ska_wide/test.unknown"].get_dict()


def test_tmdata_invalid_data_prefix():
    """Check a ValueError is raised if we pass an invalid path"""

    sources = ["mem://?tango/ska_wide/SKAMaster.yaml=test_bogus"]
    with pytest.raises(
        ValueError, match=r"Invalid telescope model data prefix.*"
    ):
        TMData(sources, prefix="/")
    with pytest.raises(
        ValueError, match=r"Invalid telescope model data prefix.*"
    ):
        TMData(sources)["/"]
    with pytest.raises(
        ValueError, match=r"Invalid telescope model data key.*"
    ):
        TMData(sources).get("/")


def test_tmdata_key_does_not_exist(tmdata_mem):
    """Check a ValueError is raised if the key does not exist"""

    with pytest.raises(
        KeyError, match=r"No telescope model data with key .* exists!"
    ):
        tmdata_mem["tango/ska_wide/doesnt_exist.yaml"]
    with pytest.raises(KeyError, match=r"Empty key/prefix not allowed!"):
        tmdata_mem[""]


def test_tmdata_environment(monkeypatch):
    """
    Check that we can set telescope model sources via
    environment variable
    """
    monkeypatch.setenv(
        "SKA_TELMODEL_SOURCES", 'mem://?test.json={"asd":"asd"}'
    )
    assert TMData()["test.json"].get_dict() == {"asd": "asd"}


def test_tmdata_defaults(monkeypatch):
    """
    Check that telescope model source defaults work
    """

    with tempfile.TemporaryDirectory("test-telmodel-cache") as cachedir:
        monkeypatch.setenv("SKA_TELMODEL_CACHE", cachedir)
        assert (
            TMData().get_sources(False)
            == ska_telmodel.data.sources.DEFAULT_SOURCES
        )


def test_tmdata_bogus_mem_source():
    """Check that we complain if key suffix is unknown"""

    sources = ["mem://asd"]
    with pytest.raises(ValueError, match=r"Memory TMData backend got path"):
        TMData(sources)


def test_tmdata_bogus_file_source():
    """Check that we complain if file backend path does not exist"""

    with tempfile.TemporaryDirectory("test-tmdata-bogus") as dirname:
        sources = [f"file://{dirname}/doesnt_exist"]
        with pytest.raises(ValueError, match=r"Base path does not exist"):
            TMData(sources)


def test_tmdata_invalid_file_key_name(caplog):
    """
    Check that we complain if there's a file in the directory that's
    not a valid telescope model name.
    """

    with tempfile.TemporaryDirectory("test-tmdata-invalid-name") as dirname:
        # Create an invalid file in directory (doesn't have an extension)
        pathlib.Path(dirname, "asd").touch()
        sources = [f"file://{dirname}"]

        # Now trying to list files should generate a warning
        caplog.set_level(logging.WARNING)
        list(TMData(sources))
        assert "File name no valid key, ignored: asd" in caplog.text


def test_tmdata_bogus_gitlab_project():
    """Check that we complain if Gitlab project does not exist"""

    class PseudoGitlab:
        @property
        def projects(self):
            return PseudoProjects()

    class PseudoProjects:
        def get(self, project_name):
            raise gitlab.exceptions.GitlabGetError()

    sources = [
        "gitlab://gitlab.com/ska-telescope/ska-telmodel-doesnt-exist#tmdata"
    ]
    pars = {"gitlab": dict(gl=PseudoGitlab(), try_nexus=False)}
    with pytest.raises(
        RuntimeError, match=r"Could not access gitlab.com project"
    ):
        TMData(sources, backend_pars=pars)


@pytest.mark.parametrize("backend", ["mem", "file", "gitlab", "car"])
def test_tmdata_get_sources(tmdata_all, backend):
    """Check that we can retrieve the (pinned/unpinned) sources list"""

    tmdata = tmdata_all[backend]

    # Unpinned
    sources = tmdata.get_sources()
    assert sources == tmdata.get_sources()

    # Pinned
    sources_p = tmdata.get_sources(True)
    if backend == "gitlab":
        assert sources_p == 4 * [
            "gitlab://gitlab.com/ska-telescope/ska-telmodel-pseudo?"
            "~4015b57a143aec5156fd1444a017a32137a3fd0f#tmdata"
        ]
    elif backend == "gitlab_car":
        assert sources_p == 2 * [
            "gitlab://gitlab.com/ska-telescope/ska-telmodel-pseudo?"
            "~4015b57a143aec5156fd1444a017a32137a3fd0f#tmdata"
        ]
    elif backend == "car":
        assert sources_p == 2 * [
            "car:ska-telmodel-pseudo?~4015b57a143aec5156fd1444a017a32137a3fd0f"
        ]
    else:
        assert sources_p == sources


@pytest.mark.parametrize("backend", ["gitlab", "car"])
def test_tmdata_nexus(monkeypatch, backend):
    """Check that we check Nexus for refs"""

    # Use a Gitlab that doesn't have anything
    class PseudoGitlab:
        @property
        def projects(self):
            return PseudoProjects()

    class PseudoProjects:
        def get(self, project_name):
            raise gitlab.exceptions.GitlabGetError()

    # Repository contents
    commit_id = sha1(b"commit").hexdigest()
    tree = [
        {"name": "tmdata", "path": "tmdata", "type": "dir", "id": commit_id},
        {
            "name": "SKAMaster.yaml",
            "path": "tmdata/tango/ska_wide/SKAMaster.yaml",
            "type": "blob",
            "id": commit_id,
        },
        {
            "name": "test.json",
            "path": "tmdata/tango/ska_wide/test.json",
            "type": "blob",
            "id": commit_id,
        },
        {
            "name": "test.json",
            "path": "tmdata/tango/ska_wide/invalid",
            "type": "blob",
            "id": commit_id,
        },
        {
            "name": "unrelated",
            "path": "unrelated",
            "type": "blob",
            "id": commit_id,
        },
    ]

    # Set up a temporary directory as source for "Nexus" data
    with tempfile.TemporaryDirectory("test-telmodel-nexus") as dirname:
        with tempfile.TemporaryDirectory("test-telmodel-cache") as cachedir:
            # Also try to set CAR_RAW_REPOSITORY_URL to an URL ending
            # with "-internal", which should be equivalent to setting
            # CAR_TMDATA_REPOSITORY_URL or SKA_TELMODEL_NEXUS to the
            # same path ending with "-telmodel".
            monkeypatch.setenv(
                "CAR_RAW_REPOSITORY_URL", f"file://{dirname}-internal"
            )
            monkeypatch.delenv("CAR_TMDATA_REPOSITORY_URL", raising=False)
            monkeypatch.setenv("SKA_TELMODEL_CACHE", cachedir)
            path = pathlib.Path(
                dirname + "-telmodel",
                "gitlab.com/ska-telescope/ska-telmodel-nexus/tmdata",
            )
            path.mkdir(parents=True)
            (path / f"~{commit_id}").mkdir()

            # Populate with data
            with open(path / "main", "w") as f:
                print(commit_id, file=f)
            with open(path / f"~{commit_id}/tmtree.json", "w") as f:
                json.dump(tree, f)

            # Create TMData object
            sources = [
                f"{backend}://gitlab.com/ska-telescope/"
                "ska-telmodel-nexus?main#tmdata"
            ]
            pars = {"gitlab": dict(gl=PseudoGitlab(), try_nexus=True)}
            tmdata = TMData(sources, backend_pars=pars)

            # Check contents
            keys = set(tmdata)
            assert keys == {
                "tango/ska_wide/SKAMaster.yaml",
                "tango/ska_wide/test.json",
            }

            # Put tarfile
            with open(path / f"~{commit_id}/tmdata.tar.gz", "wb") as f:
                _make_tarfile(f)

            # Check that we can retrieve contents
            expected = b"test: test\ntest2: test2\n"
            assert tmdata["tango/ska_wide/SKAMaster.yaml"].get() == expected
            expected2 = b'{"asd":"asd"}\n'
            assert tmdata["tango/ska_wide/test.json"].get() == expected2

            # Expecting -1 as using the Gitlab API
            assert tmdata["tango/ska_wide/test.json"].size == -1

            # Create again (test caching code path)
            tmdata2 = TMData(sources, backend_pars=pars)
            with tmdata2["tango/ska_wide/SKAMaster.yaml"].open() as f:
                assert f.read() == expected

            # And yet again for copy
            tmdata3 = TMData(sources, backend_pars=pars)
            with tempfile.TemporaryDirectory("test-tmdata-copy") as dirname:
                tmdata3["tango/ska_wide/SKAMaster.yaml"].copy(
                    dirname + "/test.yaml"
                )
                with open(dirname + "/test.yaml", "rb") as f:
                    assert f.read() == expected


@pytest.mark.parametrize("backend", ["gitlab", "car"])
def test_tmdata_nexus_project_doesnt_exist(monkeypatch, backend):
    """Check that we check Nexus for refs"""

    # Use a Gitlab that doesn't have anything
    class PseudoGitlab:
        @property
        def projects(self):
            return PseudoProjects()

    class PseudoProjects:
        def get(self, project_name):
            raise gitlab.exceptions.GitlabGetError()

    # Set up a temporary directory as source for "Nexus" data
    with tempfile.TemporaryDirectory("test-telmodel-nexus") as dirname:
        with tempfile.TemporaryDirectory("test-telmodel-cache") as cachedir:
            monkeypatch.setenv("SKA_TELMODEL_NEXUS", f"file://{dirname}")
            monkeypatch.setenv("SKA_TELMODEL_CACHE", cachedir)

            # Create TMData object
            sources = [
                f"{backend}://gitlab.com/ska-telescope/"
                "ska-telmodel-nexus2?main#tmdata"
            ]
            pars = {"gitlab": dict(gl=PseudoGitlab(), try_nexus=True)}

            # This should try to access the GitLab project, which fails
            if backend == "gitlab":
                with pytest.raises(
                    RuntimeError,
                    match=r"Could not access gitlab.com project.*",
                ):
                    with pytest.warns(
                        UserWarning, match=r".*not cached in SKA CAR.*"
                    ):
                        TMData(sources, backend_pars=pars)
            elif backend == "car":
                with pytest.raises(
                    RuntimeError, match=r".*not found in SKA CAR.*"
                ):
                    TMData(sources, backend_pars=pars)


@pytest.mark.parametrize("path", ["", "tmdata", "nested/tmdata"])
def test_tmdata_gitlab_paths(monkeypatch, path):
    with tempfile.TemporaryDirectory("ska-telmodel-cache") as dirname:
        monkeypatch.setenv("SKA_TELMODEL_CACHE", dirname)

        gl_repos = "gitlab://gitlab.com/ska-telescope/ska-telmodel-pseudo"
        gl = make_pseudo_gitlab(path)
        pars = {"gitlab": dict(gl=gl, try_nexus=False)}

        # Check that we can retrieve data no matter the repository path
        expected = b"test: test\ntest2: test2\n"
        tmdata = TMData([f"{gl_repos}?main#{path}"], backend_pars=pars)
        assert tmdata["tango/ska_wide/SKAMaster.yaml"].get() == expected


def test_invalid_nexus_uri():
    """Cover case where the Nexus URI is invalid"""

    # This will attempt to look this up in the CAR, which will fail because
    # ska-telmodel-pseudo does not exist.
    with pytest.raises(RuntimeError, match=r".*not found in SKA CAR.*"):
        TMData(
            ["car://gitlab.com/ska-telescope/ska-telmodel-pseudo?pseudo#main"],
        )

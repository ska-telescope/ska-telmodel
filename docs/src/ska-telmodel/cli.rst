
.. _cli:

Command Line Usage
==================

The library provides the ``ska-telmodel`` command line utility that
can be used to perform basic data retrieval and validation
tasks. Usage information:

.. code-block:: text

    $ ska-telmodel --help
    usage: ska-telmodel [-h] [-v] [-U] [-S SOURCES] [-l]
                    {ls,cat,cp,validate,pin,upload} ...

    SKA telescope model command line utility. The environment variable
    SKA_TELMODEL_SOURCES can also be used for sources.

    positional arguments:
      {ls,cat,cp,validate,pin,upload}
        ls                  List telescope model keys with a particular prefix.
        cat                 Retrieves and prints the telescope model data
                            identified by the given key to stdout.
        cp                  Retrieves specified telescope model data, and copies
                            it to the given path.
        validate            Validates given keys (or files) against applicable
                            schemas from the telescope model library
        pin                 Generates a "pinned" telescope model data source list,
                            where all URIs replaced such that they will uniquely
                            identify the contents of the telescope model data
                            repository.
        upload              Upload a file/directory into a telmodel repo.

    options:
      -h, --help            show this help message and exit
      -v, --verbose         Verbose mode
      -U, --update          Update source list
      -S SOURCES, --sources SOURCES
                            Set telescope model data sources of truth
                            (','-separated list of URIs)
      -l, --local           Equivalent to "--sources=file://."

And for sub command specific help:

ls
--

.. code-block:: text

    $ ska-telmodel ls --help
    usage: ska-telmodel ls [-h] [-l] [--human-readable] [--summary] [prefix]

    positional arguments:
      prefix            The prefix to use

    options:
      -h, --help            show this help message and exit
      -l, --long            Print long listing
      -H, --human-readable  Print sizes in human readable form, requires '-l'
      -s, --summary         Print a summary after the list, requires '-l'

Example Output
**************

The Basic List:

.. code-block:: text

    $ ska-telmodel ls --long
    +----------------------------+----------------------------------------------------------------------------+----------+
    | Source                     | Key                                                                        |     Size |
    +----------------------------+----------------------------------------------------------------------------+----------+
    | car:ska-telmodel-data?main | instrument/dishid_vcc_configuration/mid_cbf_parameters.json                |    263 B |
    | car:mccs/ska-low-mccs?main | instrument/mccs-configuration/antenna_export_w2.json                       | 613704 B |
    | car:mccs/ska-low-mccs?main | instrument/mccs-configuration/station_export_w2.json                       |   2828 B |
    | car:ost/ska-ost-osd?main   | instrument/scheduling-block/validation/low_sbd-validation-constants.json   |   2081 B |
    | car:ost/ska-ost-osd?main   | instrument/scheduling-block/validation/mid_sbd-validation-constants.json   |   5363 B |
    ...


Using all the options:

.. code-block:: text

    ska-telmodel ls --long --human-readable --summary
    +----------------------------+----------------------------------------------------------------------------+--------+
    | Source                     | Key                                                                        |   Size |
    +----------------------------+----------------------------------------------------------------------------+--------+
    | car:ska-telmodel-data?main | instrument/dishid_vcc_configuration/mid_cbf_parameters.json                |  263 B |
    ...
    | car:ska-telmodel?master    | software/tango/ska_wide/SKAMaster.yaml                                     |   1 KB |
    +----------------------------+----------------------------------------------------------------------------+--------+

    Total Keys: 51
    Total Size: 3 MB

Summary without the human readable sizes:

.. code-block:: text

    $ ska-telmodel ls --long --summary
    +----------------------------+----------------------------------------------------------------------------+----------+
    | Source                     | Key                                                                        |     Size |
    +----------------------------+----------------------------------------------------------------------------+----------+
    | car:ska-telmodel-data?main | instrument/dishid_vcc_configuration/mid_cbf_parameters.json                |    263 B |
    ...
    | car:ska-telmodel?master    | software/tango/ska_wide/SKAMaster.yaml                                     |   1038 B |
    +----------------------------+----------------------------------------------------------------------------+----------+

  Total Keys: 51
  Total Size: 2686027

And if there is a large file in the list (note this is an example):

.. code-block:: text

    $ ska-telmodel '--sources=gitlab://gitlab.com/ska...odel?main#tmdata' ls --long --summary --human-readable
    +--------------------------------------------+---------------------+-------------+----------+--------+
    | Source                                     | Key                 | Large File? | Cached? |   Size |
    +--------------------------------------------+---------------------+-------------+----------+--------+
    | gitlab://gitlab.com/ska...odel?main#tmdata | ska/gsm/dump.sql.gz | yes         | no       | 365 MB |
    +--------------------------------------------+---------------------+-------------+----------+--------+

cat
---

.. code-block:: text

    $ ska-telmodel cat --help
    usage: ska-telmodel cat [-h] key

    positional arguments:
      key         The keys to output

    options:
      -h, --help  show this help message and exit

cp
--

.. code-block:: text

    $ ska-telmodel cp --help
    usage: ska-telmodel cp [-h] [-R] key [path]

    positional arguments:
      key              From what directory/file to copy from
      path             Where to copy to

    options:
      -h, --help       show this help message and exit
      -R, --recursive  Copy / validate keys or files recursively

validate
--------

.. code-block:: text

    $ ska-telmodel validate --help
    usage: ska-telmodel validate [-h] [-t] [-R] key

    positional arguments:
      key              From what directory/file to copy from

    options:
      -h, --help       show this help message and exit
      -t, --strict     Strict validation mode
      -R, --recursive  Copy / validate keys or files recursively

pin
---

.. code-block:: text

    $ ska-telmodel pin --help
    usage: ska-telmodel pin [-h]

    options:
      -h, --help  show this help message and exit

.. _CLI Upload:

upload
------

.. code-block:: text

    $ ska-telmodel upload --help
    usage: ska-telmodel upload [-h] [-r REPO] [--force-car-upload]
                                 data_file [path]

    positional arguments:
      data_file             Data file to upload
      path                  Specify the path in the tmdata where we should upload to.

    options:
      -h, --help            show this help message and exit
      -r REPO, --repo REPO  Specify gitlab repository to be updated.
      --force-car-upload    Force this file to upload as a large file


See :ref:`Data Sources` for explanations about telescope model data sources.


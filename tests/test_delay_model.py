import pytest

from ska_telmodel._common import split_interface_version
from ska_telmodel.schema import example_by_uri, validate
from ska_telmodel.schemas.layout import (
    FIXED_DELAY_PREFIX,
    FIXED_DELAY_VERSIONS,
    GEOCENTRIC_LOCATION_PREFIX,
    GEOCENTRIC_LOCATION_VERSIONS,
    GEODETIC_LOCATION_PREFIX,
    GEODETIC_LOCATION_VERSIONS,
    LAYOUT_PREFIX,
    LAYOUT_VERSIONS,
    LOCAL_LOCATION_PREFIX,
    LOCAL_LOCATION_VERSIONS,
    LOCATION_PREFIX,
    LOCATION_VERSIONS,
    RECEPTOR_PREFIX,
    RECEPTOR_VERSIONS,
    check_layout_interface_version,
)


def test_delaymodel_versions(caplog):
    assert (
        check_layout_interface_version(LAYOUT_PREFIX + "0.7", LAYOUT_PREFIX)
        == "0.7"
    )

    invalid_prefix = "http://schexma.example.org/invalid"
    with pytest.raises(
        ValueError,
        match=f"LAYOUT interface URI '{invalid_prefix}' not allowed",
    ):
        check_layout_interface_version(invalid_prefix)


@pytest.mark.parametrize("delaymodel_uri", LAYOUT_VERSIONS)
def test_delaymodel(delaymodel_uri):
    # Check normal case validates
    validate(delaymodel_uri, example_by_uri(delaymodel_uri), 2)

    # Check that an error is raised if 'receptors' is omitted
    delaymodel_erronious = example_by_uri(delaymodel_uri)
    del delaymodel_erronious["receptors"]
    with pytest.raises(ValueError):
        validate(delaymodel_uri, delaymodel_erronious, 2)

    # Check validation when interface version is given as part of JSON
    validate(None, example_by_uri(delaymodel_uri), 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LAYOUT_PREFIX + "9.9")


@pytest.mark.parametrize("location_uri", LOCATION_VERSIONS)
def test_location(location_uri):
    # Check normal case validates
    validate(location_uri, example_by_uri(location_uri), 2)

    # Check that an error is raised if 'receptors' is omitted
    location_erronious = example_by_uri(location_uri)
    del location_erronious["geocentric"]
    with pytest.raises(ValueError):
        validate(location_uri, location_erronious, 2)

    # Check validation when interface version is given as part of JSON
    validate(None, example_by_uri(location_uri), 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LOCATION_PREFIX + "9.9")


@pytest.mark.parametrize("fixed_uri", FIXED_DELAY_VERSIONS)
def test_fixed_delay(fixed_uri):
    # Check normal case validates
    validate(fixed_uri, example_by_uri(fixed_uri), 2)

    # Check that an error is raised if 'receptors' is omitted
    fixed_erronious = example_by_uri(fixed_uri)
    del fixed_erronious["fixed_delay_id"]
    with pytest.raises(ValueError):
        validate(fixed_uri, fixed_erronious, 2)

    # Check validation when interface version is given as part of JSON
    validate(None, example_by_uri(fixed_uri), 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(FIXED_DELAY_PREFIX + "9.9")


@pytest.mark.parametrize("location_uri", GEOCENTRIC_LOCATION_VERSIONS)
def test_geocentric_position(location_uri):
    # Check normal case validates
    validate(location_uri, example_by_uri(location_uri), 2)

    # Check that an error is raised if 'receptors' is omitted
    location_erronious = example_by_uri(location_uri)
    del location_erronious["x"]
    with pytest.raises(ValueError):
        validate(location_uri, location_erronious, 2)

    # Check validation when interface version is given as part of JSON
    validate(None, example_by_uri(location_uri), 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(GEOCENTRIC_LOCATION_PREFIX + "9.9")


@pytest.mark.parametrize("location_uri", GEODETIC_LOCATION_VERSIONS)
def test_geodetic_position(location_uri):
    # Check normal case validates
    validate(location_uri, example_by_uri(location_uri), 2)

    # Check that an error is raised if 'receptors' is omitted
    location_erronious = example_by_uri(location_uri)
    del location_erronious["lat"]
    with pytest.raises(ValueError):
        validate(location_uri, location_erronious, 2)

    # Check validation when interface version is given as part of JSON
    validate(None, example_by_uri(location_uri), 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(GEODETIC_LOCATION_PREFIX + "9.9")


@pytest.mark.parametrize("location_uri", LOCAL_LOCATION_VERSIONS)
def test_local_position(location_uri):
    # Check normal case validates
    validate(location_uri, example_by_uri(location_uri), 2)

    # Check that an error is raised if 'receptors' is omitted
    location_erronious = example_by_uri(location_uri)
    del location_erronious["east"]
    with pytest.raises(ValueError):
        validate(location_uri, location_erronious, 2)

    # Check validation when interface version is given as part of JSON
    validate(None, example_by_uri(location_uri), 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(LOCAL_LOCATION_PREFIX + "9.9")


@pytest.mark.parametrize("receptor_uri", RECEPTOR_VERSIONS)
def test_receptor(receptor_uri):
    # Check normal case validates
    validate(receptor_uri, example_by_uri(receptor_uri), 2)

    # Check that an error is raised if 'receptors' is omitted
    receptor_erronious = example_by_uri(receptor_uri)
    if split_interface_version(receptor_uri) > (1, 0):
        del receptor_erronious["station_label"]
        assert "station_name" not in receptor_erronious
    else:
        del receptor_erronious["station_name"]
        assert "station_label" not in receptor_erronious
    with pytest.raises(ValueError):
        validate(receptor_uri, receptor_erronious, 2)

    # Check validation when interface version is given as part of JSON
    validate(None, example_by_uri(receptor_uri), 1)

    # Check invalid version raises error
    with pytest.raises(ValueError, match=r"Could not generate example"):
        example_by_uri(RECEPTOR_PREFIX + "9.9")

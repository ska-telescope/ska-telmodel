"""
This file contains functions that can be called to create a schema for MIDCBF
commands, then the schema can be used to check JSON input to the
command.
"""

from inspect import cleandoc

from schema import Or, Regex

from .._common import TMSchema, split_interface_version
from . import validators


def get_midcbf_initsysparam_schema(version: str, strict: bool):
    """MID CBF InitSysParam command input schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: the JSON schema for the MID.CBF FSP configuration.
    """

    (major, minor) = split_interface_version(version)
    initsysparam_schema_list = []

    # Create the schema for the Mid CBF InitSysParam command via the URI format
    initsysparam_schema_with_uri = TMSchema.new(
        "mid-cbf parameters source URI", version, strict, as_reference=True
    )
    initsysparam_schema_with_uri.add_field(
        "interface",
        str,
        description="URI of JSON schema for this command's JSON payload.",
    )
    initsysparam_schema_with_uri.add_field(
        "tm_data_sources",
        [str],
        check_strict=lambda lst: len(lst) == 1,
        description=cleandoc(
            """
            The telmodel data source. This parameter must be provided as a
            list containing a single entry.
            """
        ),
    )
    initsysparam_schema_with_uri.add_field(
        "tm_data_filepath",
        (Regex(r"""^\S+\.json$""") if strict else str),
        description=cleandoc(
            """
            Path to the JSON file containing the dish parameters
            required to execute the Mid CBF InitSysParam command.
            """
        ),
    )

    # Create the subschema for the vcc ID and sample rate of a given dish
    dish_mapping_details_schema = TMSchema.new(
        "dish mapping details", version, strict, as_reference=True
    )
    dish_mapping_details_schema.add_field(
        "vcc",
        int,
        check_strict=lambda n: n >= 1 and n <= 197,
        description=cleandoc(
            """
            The VCC ID for the given dish ID.

            Range: [1-197]
            """
        ),
    )
    if (major, minor) < (1, 1):
        dish_mapping_details_schema.add_field(
            "k",
            int,
            check_strict=lambda n: n >= 1 and n <= 2222,
            description=cleandoc(
                """
                The offset-index k value for the dish ID.

                Range: [1-2222]
                """
            ),
        )
    else:
        dish_mapping_details_schema.add_field(
            "k",
            int,
            check_strict=lambda n: n >= 1 and n <= 2222,
            description=cleandoc(
                """
                The offset-index k value for the dish ID.

                The same K value can be specified for multiple receptors but
                the expectation is that during operations this will not be the
                case.

                Range: [1-2222]
                """
            ),
        )

    # Create the subschema for the dish ID to vcc ID and sample rate mapping
    dish_mapping_schema = TMSchema.new(
        "dish mapping",
        version,
        strict,
        as_reference=True,
    )

    # Borrowed from ../sdp/schema.py/get_type_name_schema
    # A bit of a hack - this is used in a key, and JSON schemas want a
    # string there if they are to generate meaningful documentation.
    if strict == "json-schema":  # pragma: no cover
        dish_id_key = "dish ID"
    else:
        dish_id_key = Regex(
            r"""^(SKA(00[1-9]|0[1-9][0-9]|1[0-2][0-9]|13[0-3]))$|"""
            + r"""^(MKT(0[0-5][0-9]|06[0-3]))$"""
        )
    dish_mapping_schema.add_field(
        dish_id_key,
        dish_mapping_details_schema,
        description=cleandoc(
            """
            At least one dish ID must be specified,
            and each dish ID must be a valid ID.

            Valid dish IDs include:

            SKA dishes: "SKAnnn", where nnn is a zero padded integer in the
            range of 001 to 133.

            MeerKAT dishes: "MKTnnn", where nnn is a zero padded integer in the
            range of 000 to 063.
            """
        ),
    )

    # Create the schema for the Mid CBF InitSysParam command via
    # explicitly defined parameters
    initsysparam_schema = TMSchema.new(
        "mid-cbf parameters",
        version,
        strict,
        as_reference=True,
    )
    initsysparam_schema.add_field(
        "interface",
        str,
        description="URI of JSON schema for this command's JSON payload.",
    )
    initsysparam_schema.add_field(
        "dish_parameters",
        dish_mapping_schema,
        check_strict=lambda dct: len(dct.keys()) >= 1
        and validators.validate_unique_dish_mapping_values(dct),
        description=cleandoc(
            """
            Dish parameters section containing the information
            needed to map each dish ID to its initialization parameters,
            including the vcc ID and offset-index k value.
            """
        ),
    )

    initsysparam_schema_list.append(initsysparam_schema)
    initsysparam_schema_list.append(initsysparam_schema_with_uri)

    return TMSchema.new(
        "MID.CBF Parameters",
        version,
        strict,
        schema=Or(initsysparam_schema, initsysparam_schema_with_uri),
    )

ska-csp-releaseresources
========================

Examples for the different versions of the releaseresources schema

.. toctree::
  :maxdepth: 1
  :caption: MID CSP assignresources examples

  ska-csp-releaseresources-3.0
  ska-csp-releaseresources-2.3
  ska-csp-releaseresources-2.2

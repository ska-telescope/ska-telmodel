CSP configure 4.0
=================

JSON schema and example for CSP Low configure

This schema includes the changes performed by the Nakshatra team to
fix the incompatibilities between the different published and used schemas.
Note that CBF configure schema version 1.0 is still not available

.. ska-schema:: https://schema.skao.int/ska-low-csp-configure/4.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/4.0

      Example JSON (LOW CSP Configuration for CBF 1.0, PST 2.5)

JSON schema and example for Configure version 2.4
=================================================
.. ska-schema:: https://schema.skao.int/ska-pst-configure/2.4
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.4 low_pst_scan_ft

      Example (LOW PST configuration for FLOW THROUGH scan 2.4)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.4 low_pst_scan_pt

      Example (LOW PST configuration for PULSAR TIMING scan 2.4)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.4 low_pst_scan_ds

      Example (LOW PST configuration for DYNAMIC SPECTRUM scan 2.4)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.4 low_pst_scan_vr

      Example (LOW PST configuration for VOLTAGE RECORDER scan 2.4)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.4 mid_pst_scan_ft

      Example (MID PST configuration for FLOW THROUGH scan 2.4)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.4 mid_pst_scan_pt

      Example (MID PST configuration for PULSAR TIMING scan 2.4)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.4 mid_pst_scan_ds

      Example (MID PST configuration for DYNAMIC SPECTRUM scan 2.4)

   .. ska-schema-example:: https://schema.skao.int/ska-pst-configure/2.4 mid_pst_scan_vr

      Example (MID PST configuration for VOLTAGE RECORDER scan 2.4)

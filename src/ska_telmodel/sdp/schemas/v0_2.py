"""Version 0.2 of SDP schema"""

from inspect import cleandoc

from schema import Optional

from ska_telmodel._common import TMSchema

from .v0_1 import (
    get_receive_map,
    get_sdp_assignres,
    get_sdp_configure,
    get_sdp_scan,
)

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_scan",
    "get_sdp_recvaddrs",
]


RECVADDRS_DESCRIPTION = cleandoc(
    """
    Provides information about receive node addresses to use
    for ingesting measurement data to SDP (such as visibility
    SPEAD streams).

    Receive addresses consists of a map of scan type to a
    receive address map. This address map must be set once the
    SDP subarray finishes the transition following
    ``AssignResources`` (i.e. IDLE following the current
    state of ADR-8). TMC will then check SDP's subarray
    ``receiveAddresses`` attribute when preparing to configure
    elements for a certain scan type.

    Note that this has been changed to use the more compact
    channel map format defined in ADR-4. The general idea
    still applies: A map is given as a list, each entry of the
    format ``[start_channel, value]``. The first entry specifies
    the first channel ID the map applies to. So in the
    example, the host for channels 0-399 is "192.168.0.1",
    while the host for channels 400-799 is "192.168.0.2" and
    so forth.

    A minor extension applies to the port map, where every map
    entry is given as ``[start_channel, start_value,
    increment]``. The true value for a channel is given from
    the applicable map entry by::

        value = start_value + (channel - start_channel) * increment

    So in the example, channels 0-399 should be sent to host
    "192.168.0.1" at ports 9000-9399, and channels 400-799 to
    host "192.168.0.2" at ports 9000-9399. If we had said
    ``"port": [[0, 9000, 0]`` all packets would be sent to the
    same port. Equally ``"port": [[0, 9000, 2]`` would indicate
    spacing the ports out by steps of 2.

    Unused channel IDs should be ignored. This especially
    applies to unused gaps and channel ID strides possibly
    resulting from averaging at CBF. This means that with an
    averaging degree of 2 (see channelAveragingMap in ADR-4),
    only every second channel ID would be used in the example
    above.
    """
)


def get_sdp_recvaddrs(version: str, strict: bool) -> TMSchema:
    return TMSchema.new(
        "SDP receive addresses map",
        version,
        strict,
        schema={
            Optional("interface"): str,
            str: get_receive_map(strict),
        },
        description=RECVADDRS_DESCRIPTION,
    )

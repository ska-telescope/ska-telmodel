from . import ska_schema, ska_schema_example


def setup(app):
    app.add_directive("ska-schema", ska_schema.SkaSchema)
    app.add_directive(
        "ska-schema-example", ska_schema_example.SkaSchemaExample
    )
    app.add_config_value("ska_schema_jsonschema_path", None, rebuild="env")
    return {"version": "0.1.0"}

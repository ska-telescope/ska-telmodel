
ska-sdp-assignres
=================

.. ska-schema:: https://schema.skao.int/ska-sdp-assignres/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-sdp-assignres/1.0

       Example

.. ska-schema:: https://schema.skao.int/ska-sdp-assignres/0.5
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-sdp-assignres/0.5

       Example

.. ska-schema:: https://schema.skao.int/ska-sdp-assignres/0.4
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-sdp-assignres/0.4

       Example

.. ska-schema:: https://schema.skao.int/ska-sdp-assignres/0.3
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-sdp-assignres/0.3

       Example

.. ska-schema:: https://schema.skao.int/ska-sdp-assignres/0.2
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-sdp-assignres/0.2

       Example

.. ska-schema:: https://schema.skao.int/ska-sdp-assignres/0.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

"""Cache mechanism for static JSON schema files."""
import json
import logging
import time

import requests

from . import cache

_LOGGER = logging.getLogger("ska_telmodel")

CACHE_TIMEOUT = 300  # 5 minutes in seconds


class SchemaCache:
    """Cache handler for JSON schema files fetched from URLs."""

    def __init__(self, base_url: str):
        """Initialize the schema cache.

        Args:
            base_url: Base URL for schema files
        """
        self.base_url = base_url.rstrip("/")

    def _get_cache_key(self, url: str) -> str:
        """Generate a cache key from URL."""
        return f"schema_cache_{url.replace('/', '_')}"

    def _is_cache_valid(self, cache_key: str) -> bool:
        """Check if cached data is still valid."""
        if not cache.cache_exists(cache_key, []):
            return False

        # Check if cache has expired
        cache_time = cache.get_cache_time(cache_key, [])
        return (time.time() - cache_time.timestamp()) < CACHE_TIMEOUT

    def get_schema(self, url_path: str) -> dict:
        """Get schema from cache or fetch from URL.

        Args:
            url_path: Path component of the URL

        Returns:
            dict: JSON schema data
        """
        full_url = f"{self.base_url}/{url_path.lstrip('/')}"
        cache_key = self._get_cache_key(full_url)

        # Try to get from cache first
        if self._is_cache_valid(cache_key):
            cache_path = cache.cache_path(cache_key, [])
            _LOGGER.info(f"Using cached schema from {cache_path}")
            with open(cache_path, "r") as f:
                return json.load(f)

        # Fetch from URL
        response = requests.get(full_url)
        response.raise_for_status()
        schema_data = response.json()

        # Save to cache
        cache_path = cache.cache_path(cache_key, [])
        cache_path.parent.mkdir(parents=True, exist_ok=True)
        with open(cache_path, "w") as f:
            json.dump(schema_data, f)

        return schema_data

"""SDP schema prefixes and versions."""

SDP_ASSIGNRES_PREFIX = "https://schema.skao.int/ska-sdp-assignres/"
SDP_RELEASERES_PREFIX = "https://schema.skao.int/ska-sdp-releaseres/"
SDP_CONFIGURE_PREFIX = "https://schema.skao.int/ska-sdp-configure/"
SDP_SCAN_PREFIX = "https://schema.skao.int/ska-sdp-scan/"
SDP_RECVADDRS_PREFIX = "https://schema.skao.int/ska-sdp-recvaddrs/"

SDP_INTERFACE_VERSIONS = [(0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (1, 0)]


def sdp_interface_versions(prefix: str, min_ver=None, max_ver=None):
    """
    Returns a list of SDP interface version URIs

    :param prefix: Interface URI prefix
    :param min_ver: Tuple of minimum version to return
    :param max_ver: Tuple of maximum version to return
    """

    sdp_vers = SDP_INTERFACE_VERSIONS
    if min_ver is not None:
        sdp_vers = [v for v in sdp_vers if v >= min_ver]
    if max_ver is not None:
        sdp_vers = [v for v in sdp_vers if v <= max_ver]
    assert (
        prefix[-1] == "/"
    ), "Please only pass prefixes ending with '/' to sdp_interface_versions!"
    return [f"{prefix}{v0}.{v1}" for v0, v1 in sdp_vers]

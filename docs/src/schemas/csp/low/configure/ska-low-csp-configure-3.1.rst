CSP configure 3.1
=================

JSON schema and example for CSP Low configure

.. ska-schema:: https://schema.skao.int/ska-low-csp-configure/3.1
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.1

      Example JSON (LOW CSP Configuration for CBF 0.2)

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.1 pst_scan_ft

      Example JSON (CSP configuration for PST flow through scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.1 pst_scan_pt

      Example JSON (CSP configuration for PST pulsar timing scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.1 pst_scan_vr

      Example JSON (CSP configuration for PST voltage recorder scan 2.5)

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-configure/3.1 pst_scan_ds

      Example JSON (CSP configuration for PST dynamic spectrum scan 2.5)


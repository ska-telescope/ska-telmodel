"""Version 0.1 of SDP schema"""

from inspect import cleandoc

from schema import And, Literal, Optional, Or, Schema

from ska_telmodel._common import TMSchema, get_channel_map_schema, mk_if
from ska_telmodel.sdp.common import (
    get_pb_name_schema,
    get_receptor_schema,
    get_sbi_name_schema,
)

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_scan",
    "get_sdp_recvaddrs",
]


def get_pb_dependency(version: str, strict: bool) -> Schema:
    return TMSchema.new(
        "Processing block dependency",
        version,
        strict,
        schema={
            "pb_id": get_pb_name_schema(version, strict),
            "type": [str],
        },
    )


def get_processing_block(version: str, strict: bool) -> Schema:
    return TMSchema.new(
        "Processing block",
        version,
        strict,
        description=cleandoc(
            """
            A Processing Block is an atomic unit of data processing
            for the purpose of SDP's internal scheduler. Each PB
            references a processing script and together with the
            associated execution block provides all parameters
            necessary to carry out scheduling - both on TM's side for
            observation planning and on SDP's side - as well as enable
            processing to locate all required inputs once it is in
            progress.

            PBs are used for both real-time and deferred, batch,
            processing. An execution block will often contain many
            Processing Blocks, for example for ingest,
            self-calibration and Data Product preparation.
            """
        ),
        schema={
            "id": get_pb_name_schema(version, strict),
            "workflow": {"type": str, "id": str, "version": str},
            Optional("parameters"): dict,
            Optional("dependencies"): [get_pb_dependency(version, strict)],
        },
        as_reference=True,
    )


def _add_channel_fields(schema: TMSchema, strict: bool) -> None:
    schema.add_field("count", int, description="Number of channels")
    schema.add_field("start", int, description="First channel ID")
    schema.add_opt_field(
        "stride", int, description="Distance between subsequent channel IDs"
    )
    schema.add_field(
        "freq_min", float, description="Lower bound of first channel"
    )
    schema.add_field(
        "freq_max", float, description="Upper bound of last channel"
    )
    schema.add_opt_field(
        "link_map",
        get_channel_map_schema(int, 0, strict),
        description=cleandoc(
            """
            Channel map that specifies which network link is going to
            get used to send channels to SDP. Intended to allow SDP to
            optimise network and receive node configuration.
            """
        ),
    )


def get_spectral_window(version: str, strict: bool) -> Schema:
    schema = TMSchema.new(
        "Spectral window",
        version,
        strict,
        schema={"spectral_window_id": Schema(str)},
    )
    _add_channel_fields(schema, strict)
    return schema


def get_scan_channels(version: str, strict: bool) -> Schema:
    schema = TMSchema.new(
        "Scan channels",
        version,
        strict,
        description=cleandoc(
            """
            Informs SDP ingest about the expected channel configuration,
            especially which frequencies are expected to be mapped to
            which channel ID. Note that channel IDs are not guaranteed to be
            continuous, so this might involve gaps and/or strides.
            """
        ),
        as_reference=True,
    )
    _add_channel_fields(schema, strict)
    return schema


def get_scan_type(version: str, strict: bool) -> Schema:
    if_strict = mk_if(strict)

    return TMSchema.new(
        "Scan type",
        version,
        strict,
        schema={
            "id": str,
            Optional("coordinate_system"): And("ICRS", if_strict(Or("ICRS"))),
            Optional("ra"): str,
            Optional("dec"): str,
            Optional("channels"): [get_scan_channels(version, strict)],
        },
        as_reference=True,
    )


def get_external_resources_schema(version: str, strict: bool) -> Schema:
    schema = TMSchema.new(
        "SDP external resources", version, strict, ignore_extra_keys=True
    )
    schema.add_opt_field("receptors", [get_receptor_schema(strict)])
    return schema


def get_sdp_assignres(version: str, strict: bool) -> Schema:
    schema = TMSchema.new(
        "SDP assign resources",
        version,
        strict,
        schema={
            Optional("interface"): str,
            "id": get_sbi_name_schema(version, strict),
            Optional("max_length"): float,
            Literal(
                "scan_types",
                description="Scan types to be supported on subarray",
            ): [get_scan_type(version, strict)],
            "processing_blocks": [get_processing_block(version, strict)],
        },
    )
    return schema


def get_sdp_configure(version: str, strict: bool) -> Schema:
    return TMSchema.new(
        "SDP configure",
        version,
        strict,
        schema={
            Optional("interface"): str,
            "scan_type": str,
            Optional("new_scan_types"): [get_scan_type(version, strict)],
        },
    )


def get_sdp_scan(version: str, strict: bool) -> Schema:
    return TMSchema.new(
        "SDP scan",
        version,
        strict,
        schema={
            Optional("interface"): str,
            "id": int,
        },
    )


# Pre-ADR-10 schema
def get_sdp_recvaddrs(version: str, strict: bool) -> Schema:
    return TMSchema.new(
        "SDP receive addresses",
        version,
        strict,
        schema={
            Optional("interface"): str,
            "scanId": int,
            "totalChannels": int,
            "receiveAddresses": [
                {
                    "phaseBinId": int,
                    "fspId": int,
                    "hosts": [
                        {
                            "host": str,
                            "channels": [
                                {
                                    "portOffset": int,
                                    "startChannel": int,
                                    "numChannels": int,
                                }
                            ],
                        }
                    ],
                }
            ],
        },
    )


def get_host() -> Literal:
    return Literal(
        "host",
        description=cleandoc(
            """
            Destination host names (as channel map)

            Note that these are not currently guaranteed to be IP
            addresses, so a DNS resolution  might be required.
            """
        ),
    )


def get_port() -> Literal:
    return Literal(
        "port",
        description="Destination ports (as channel map)",
    )


def get_receive_map(strict: bool) -> dict:
    return {
        get_host(): get_channel_map_schema(str, 0, strict),
        Optional(
            Literal(
                "mac",
                description=cleandoc(
                    """
                    Destination MAC addresses (as channel map)

                    Likely not going to be used, downstream systems should use
                    ARP to determine the MAC address using ``host`` instead.
                    See ADR-36
                    """
                ),
            )
        ): get_channel_map_schema(str, 0, strict),
        Optional(get_port()): get_channel_map_schema(int, 0, strict),
    }

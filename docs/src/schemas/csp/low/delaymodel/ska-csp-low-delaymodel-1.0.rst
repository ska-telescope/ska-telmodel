CSP delay model 1.0
===================

JSON schema and example for CSP Low delay model

.. ska-schema:: https://schema.skao.int/ska-low-csp-delaymodel/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-low-csp-delaymodel/1.0

       Example JSON

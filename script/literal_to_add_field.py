import sys

from bowler import Query
from fissix import fixer_util, pgen2


def increase_indent(node):
    for n in node.post_order():
        if "\n" in n.prefix:
            n.prefix += "    "
    return node


def literal_to_add_field(node, cap, fn):
    # Construct add_field(items, val, ...)
    indent = node.prefix
    items = cap["items"]
    args = cap["args"]
    val = cap["val"]
    items.remove()
    for arg in args:
        arg.remove()
    val.remove()
    items.prefix = ""

    rest_args = args[1:]
    if rest_args and rest_args[-1].type == pgen2.token.COMMA:
        rest_args = rest_args[:-1]

    result = fixer_util.Call(
        fixer_util.Name("add_field"),
        [
            items,
            fixer_util.Comma(),
            args[0],
            fixer_util.Comma(),
            increase_indent(val),
            *rest_args,
        ],
        prefix=indent,
    )
    print(result)
    return result


Query(sys.argv[1]).select(
    """
    expr_stmt< power< items=NAME trailer< '['
        power< 'Literal' trailer< '(' arglist< args=any* > ')' > >
    ']' > > '=' val=any >
    """
).modify(literal_to_add_field).execute()


def literal_to_add_field_optional(node, cap, fn):
    # Construct add_field(items, val, ...)
    indent = node.prefix
    items = cap["items"]
    args = cap["args"]
    optional_args = cap.get("optional_args", [])
    val = cap["val"]
    items.remove()
    for arg in args:
        arg.remove()
    for arg in optional_args:
        arg.remove()
    val.remove()
    items.prefix = ""

    rest_args = args[1:]
    if rest_args and rest_args[-1].type == pgen2.token.COMMA:
        rest_args = rest_args[:-1]
    if optional_args and optional_args[-1].type == pgen2.token.COMMA:
        optional_args = optional_args[:-1]

    result = fixer_util.Call(
        fixer_util.Name("add_field"),
        [
            items,
            fixer_util.Comma(),
            args[0],
            fixer_util.Comma(),
            increase_indent(val),
            *rest_args,
            *optional_args,
            fixer_util.Comma(),
            fixer_util.KeywordArg(
                fixer_util.Name("optional"), fixer_util.Name("True")
            ),
        ],
        prefix=indent,
    )
    return result


Query(sys.argv[1]).select(
    """
expr_stmt<
  power< items=NAME
    trailer< '['
      power< 'Optional'
        trailer< '('
         arglist<
          power< 'Literal'
            trailer< '('
              arglist< args=any* >
            ')' >
          >
          optional_args=any*
          >
       ')' >
      >
    ']' >
  >
  '='
  val=any
>"""
).modify(literal_to_add_field_optional).execute()

Query(sys.argv[1]).select(
    """
expr_stmt<
  power< items=NAME
    trailer< '['
      power< 'Optional'
        trailer< '('
          args=any*
        ')' >
      >
    ']' >
  >
  '='
  val=any
>"""
).modify(literal_to_add_field_optional).execute()

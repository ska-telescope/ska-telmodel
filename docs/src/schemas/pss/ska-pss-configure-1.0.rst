JSON schema and example for Configure version 1.0
=================================================
.. ska-schema:: https://schema.skao.int/ska-pss-configure/1.0
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-pss-configure/1.0 pss_schema_example

      Example (PSS configuration for scan 1.0)
import json
import logging
import pathlib
import sys
import tempfile
import urllib
from unittest.mock import ANY, MagicMock, call, patch

import pytest

import ska_telmodel.cli
from ska_telmodel.data.frontend import TMObject
from ska_telmodel.data.large_files import large_file_link_content
from ska_telmodel.data.new_data_backend import GitBackend
from ska_telmodel.sdp.example import get_sdp_assignres_example
from ska_telmodel.sdp.version import (
    SDP_ASSIGNRES_PREFIX,
    sdp_interface_versions,
)


def run_main(monkeypatch, *pars):
    """Run a certain command line through the CLI"""

    def _internal_function(self, input_string):
        pass

    monkeypatch.setattr(sys, "argv", [sys.argv[0], *pars])
    monkeypatch.setattr(
        ska_telmodel.cli.CLI, "_confirm_choice", _internal_function
    )
    ska_telmodel.cli.main()


def test_cli_help(monkeypatch, capsys):
    """Test --help"""
    with pytest.raises(SystemExit):
        run_main(monkeypatch, "--help")
    captured = capsys.readouterr()
    assert "usage:" in captured.out
    assert "positional arguments:" in captured.out
    assert "options:" in captured.out
    assert not captured.err


def test_cli_ls(monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "ls",
    )

    captured = capsys.readouterr()
    assert captured.out == "tango/ska_wide/SKAMaster.yaml\n"
    assert not captured.err

    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "ls",
        "tango/ska_wide",
    )

    captured = capsys.readouterr()
    assert captured.out == "SKAMaster.yaml\n"
    assert not captured.err


def test_cli_pin(monkeypatch, capsys):
    """Test 'ska-telmodel pin'"""

    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "pin",
    )

    captured = capsys.readouterr()
    assert (
        captured.out == "SKA_TELMODEL_SOURCES="
        "mem://?tango/ska_wide/SKAMaster.yaml=test_bogus\n"
    )
    assert not captured.err


def test_cli_ls_linked(monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml.link=test_bogus",
        "ls",
    )

    captured = capsys.readouterr()
    assert captured.out == "tango/ska_wide/SKAMaster.yaml\n"
    assert not captured.err


def test_cli_ls_long_no_linked(monkeypatch, capsys):
    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "ls",
        "--long",
    )

    captured = capsys.readouterr()
    lines = captured.out.split("\n")
    assert [col.strip() for col in lines[1].split("|")] == [
        "",
        "Source",
        "Key",
        "Size",
        "",
    ]
    assert [col.strip() for col in lines[3].split("|")] == [
        "",
        "mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "tango/ska_wide/SKAMaster.yaml",
        "11 B",
        "",
    ]
    assert len(lines) == 6
    assert not captured.err


def test_cli_ls_long_no_linked_subkeys(monkeypatch, capsys):
    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "ls",
        "--long",
        "tango",
    )

    captured = capsys.readouterr()
    lines = captured.out.split("\n")
    assert [col.strip() for col in lines[1].split("|")] == [
        "",
        "Source",
        "Key",
        "Size",
        "",
    ]
    assert [col.strip() for col in lines[3].split("|")] == [
        "",
        "mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "tango/ska_wide/SKAMaster.yaml",
        "11 B",
        "",
    ]
    assert len(lines) == 6
    assert not captured.err


def test_cli_ls_long_no_linked_summary(monkeypatch, capsys):
    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "ls",
        "--long",
        "--summary",
    )

    captured = capsys.readouterr()
    lines = captured.out.split("\n")
    assert [col.strip() for col in lines[1].split("|")] == [
        "",
        "Source",
        "Key",
        "Size",
        "",
    ]
    assert [col.strip() for col in lines[3].split("|")] == [
        "",
        "mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "tango/ska_wide/SKAMaster.yaml",
        "11 B",
        "",
    ]
    assert lines[6] == "Total Keys: 1"
    assert lines[7] == "Total Size: 11"
    assert len(lines) == 9
    assert not captured.err


def test_cli_ls_long_no_linked_summary_human_readable(monkeypatch, capsys):
    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "ls",
        "--long",
        "--summary",
        "--human-readable",
    )

    captured = capsys.readouterr()
    lines = captured.out.split("\n")
    assert [col.strip() for col in lines[1].split("|")] == [
        "",
        "Source",
        "Key",
        "Size",
        "",
    ]
    assert [col.strip() for col in lines[3].split("|")] == [
        "",
        "mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "tango/ska_wide/SKAMaster.yaml",
        "11 B",
        "",
    ]
    assert lines[6] == "Total Keys: 1"
    assert lines[7] == "Total Size: 11 B"
    assert len(lines) == 9
    assert not captured.err


@patch("ska_telmodel.data.frontend.large_file_search")
@patch("ska_telmodel.data.large_files.cache_path")
def test_cli_ls_long(mock_cache, mock_search, monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    mock_search.side_effect = [
        ["", "", 12_000_000_000],
        ["", "", 12_000],
        ["", "", 12_000_000],
    ]

    with tempfile.TemporaryDirectory() as tmp_cache_dir:
        mock_cache.return_value = pathlib.Path(tmp_cache_dir, "large_files")
        large_files = pathlib.Path(tmp_cache_dir, "large_files")
        large_files.mkdir(parents=True)
        with open(large_files / "123abc", "w") as f:
            f.write("12345" * 100)

        with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
            parent = pathlib.Path(dirname, "tango", "ska_wide")
            parent.mkdir(parents=True)
            with open(parent / "test_file.yaml", "w") as f:
                f.write("test: file")
            with open(parent / "frontend.py.link", "w") as f:
                content = large_file_link_content(
                    "123abc",
                    "http://test.link/file.link",
                    "tango/ska_wide/frontend.py",
                )
                f.write(content)
            with open(parent / "frontendkb.py.link", "w") as f:
                content = large_file_link_content(
                    "123abcd",
                    "http://test.link/file.link",
                    "tango/ska_wide/frontend2.py",
                )
                f.write(content)
            with open(parent / "frontendmb.py.link", "w") as f:
                content = large_file_link_content(
                    "123abcd",
                    "http://test.link/file.link",
                    "tango/ska_wide/frontend2.py",
                )
                f.write(content)
            with open(parent / "frontendgb.py.link", "w") as f:
                content = large_file_link_content(
                    "123abcd",
                    "http://test.link/file.link",
                    "tango/ska_wide/frontend2.py",
                )
                f.write(content)
            monkeypatch.chdir(dirname)
            run_main(
                monkeypatch, "--local", "ls", "--long", "--human-readable"
            )

    captured = capsys.readouterr()
    lines = captured.out.split("\n")
    assert [col.strip() for col in lines[1].split("|")] == [
        "",
        "Source",
        "Key",
        "Large File?",
        "Cached?",
        "Size",
        "",
    ]
    # Skipping row 2 (header splitter)
    assert [col.strip() for col in lines[3].split("|")] == [
        "",
        f"file://{dirname}",
        "tango/ska_wide/frontend.py",
        "yes",
        "yes",
        "500 B",
        "",
    ]
    assert [col.strip() for col in lines[4].split("|")] == [
        "",
        f"file://{dirname}",
        "tango/ska_wide/frontendgb.py",
        "yes",
        "no",
        "11 GB",
        "",
    ]
    assert [col.strip() for col in lines[5].split("|")] == [
        "",
        f"file://{dirname}",
        "tango/ska_wide/frontendkb.py",
        "yes",
        "no",
        "12 KB",
        "",
    ]
    assert [col.strip() for col in lines[6].split("|")] == [
        "",
        f"file://{dirname}",
        "tango/ska_wide/frontendmb.py",
        "yes",
        "no",
        "11 MB",
        "",
    ]
    assert [col.strip() for col in lines[7].split("|")] == [
        "",
        f"file://{dirname}",
        "tango/ska_wide/test_file.yaml",
        "no",
        "yes",
        "10 B",
        "",
    ]
    assert lines[-1] == ""
    assert len(lines) == 10
    assert not captured.err


def test_cli_pin_default(monkeypatch, capsys):
    """Test 'ska-telmodel pin'"""

    run_main(monkeypatch, "pin")

    captured = capsys.readouterr()
    assert captured.out.startswith("SKA_TELMODEL_SOURCES=")
    assert not captured.err


def test_cli_cat(monkeypatch, capsys):
    """Test 'ska-telmodel cat'"""

    run_main(
        monkeypatch,
        "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
        "cat",
        "tango/ska_wide/SKAMaster.yaml",
    )

    captured = capsys.readouterr()
    assert captured.out == "test_bogus\n"
    assert not captured.err


def test_cli_cat_env(monkeypatch, capsys):
    """Test 'ska-telmodel cat'"""

    monkeypatch.setenv(
        "SKA_TELMODEL_SOURCES",
        "mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
    )
    run_main(monkeypatch, "cat", "tango/ska_wide/SKAMaster.yaml")

    captured = capsys.readouterr()
    assert captured.out == "test_bogus\n"
    assert not captured.err


def test_cli_cat_local(monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname, "tango", "ska_wide")
        parent.mkdir(parents=True)
        with open(parent / "SKAMaster.yaml", "w") as f:
            print("test_bogus", file=f)
        monkeypatch.chdir(dirname)
        run_main(
            monkeypatch, "--local", "cat", "tango/ska_wide/SKAMaster.yaml"
        )

    captured = capsys.readouterr()
    assert captured.out == "test_bogus\n"
    assert not captured.err


@patch("ska_telmodel.data.frontend.large_file_download")
def test_cli_cat_local_linked(mock_download, monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname, "tango", "ska_wide")
        parent.mkdir(parents=True)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/ska_wide/frontend.py",
            )
            f.write(content)
        monkeypatch.chdir(dirname)
        run_main(monkeypatch, "--local", "cat", "tango/ska_wide/frontend.py")

    captured = capsys.readouterr()
    assert captured.out == "Lorem Ipsum"
    assert not captured.err


def test_cli_cp(monkeypatch, capsys):
    """Test 'ska-telmodel cp'"""

    with tempfile.TemporaryDirectory("test-telmodel-nexus") as dirname:
        run_main(
            monkeypatch,
            "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
            "cp",
            "tango/ska_wide/SKAMaster.yaml",
            dirname,
        )

        captured = capsys.readouterr()
        assert (
            captured.out
            == f"tango/ska_wide/SKAMaster.yaml -> {dirname}/SKAMaster.yaml\n"
        )
        assert not captured.err

        with open(dirname + "/SKAMaster.yaml") as f:
            assert f.read() == "test_bogus\n"


def test_cli_cp_dest(monkeypatch, capsys):
    """Test 'ska-telmodel cp' to specific destination"""

    with tempfile.TemporaryDirectory("test-telmodel-nexus") as dirname:
        run_main(
            monkeypatch,
            "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
            "cp",
            "tango/ska_wide/SKAMaster.yaml",
            dirname + "/test.yaml",
        )

        captured = capsys.readouterr()
        assert (
            captured.out
            == f"tango/ska_wide/SKAMaster.yaml -> {dirname}/test.yaml\n"
        )
        assert not captured.err

        with open(dirname + "/test.yaml") as f:
            assert f.read() == "test_bogus\n"


@patch("ska_telmodel.data.frontend.large_file_download")
def test_cli_cp_local_dest_linked(mock_download, monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname, "tango", "ska_wide")
        parent.mkdir(parents=True)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/ska_wide/frontend.py",
            )
            f.write(content)
        monkeypatch.chdir(dirname)
        run_main(
            monkeypatch,
            "--local",
            "cp",
            "tango/ska_wide/frontend.py",
            f"{dirname}/test.file",
        )
        with open(f"{dirname}/test.file", "r") as file:
            download_content = file.read()

    captured = capsys.readouterr()
    assert (
        captured.out == f"tango/ska_wide/frontend.py -> {dirname}/test.file\n"
    )
    assert download_content == "Lorem Ipsum"
    assert not captured.err


@patch("ska_telmodel.data.frontend.large_file_download")
def test_cli_cp_local_dest_linked_use_link(mock_download, monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname, "tango", "ska_wide")
        parent.mkdir(parents=True)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/ska_wide/frontend.py",
            )
            f.write(content)
        monkeypatch.chdir(dirname)
        run_main(
            monkeypatch,
            "--local",
            "cp",
            "tango/ska_wide/frontend.py.link",
            f"{dirname}/test.file",
        )
        with open(f"{dirname}/test.file", "r") as file:
            download_content = file.read()

    captured = capsys.readouterr()
    assert (
        captured.out == f"tango/ska_wide/frontend.py -> {dirname}/test.file\n"
    )
    assert download_content == "Lorem Ipsum"
    assert not captured.err


@patch("ska_telmodel.data.frontend.large_file_download")
def test_cli_cp_local_dest_dir_linked(mock_download, monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname, "tango", "ska_wide")
        parent.mkdir(parents=True)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/ska_wide/frontend.py",
            )
            f.write(content)
        monkeypatch.chdir(dirname)
        run_main(monkeypatch, "--local", "cp", "tango/ska_wide/frontend.py")
        with open(f"{dirname}/frontend.py", "r") as file:
            download_content = file.read()

    captured = capsys.readouterr()
    assert captured.out == "tango/ska_wide/frontend.py -> frontend.py\n"
    assert download_content == "Lorem Ipsum"
    assert not captured.err


def test_cli_cp_r(monkeypatch, capsys):
    """Test recursive 'ska-telmodel cp'"""

    with tempfile.TemporaryDirectory("test-telmodel-nexus") as dirname:
        run_main(
            monkeypatch,
            "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
            "cp",
            "-R",
            "",
            dirname,
        )

        captured = capsys.readouterr()
        path = "tango/ska_wide/SKAMaster.yaml"
        assert captured.out == f"{path} -> {dirname}/{path}\n"
        assert not captured.err

        with open(dirname + "/tango/ska_wide/SKAMaster.yaml") as f:
            assert f.read() == "test_bogus\n"


def test_cli_cp_r2(monkeypatch, capsys):
    """Test recursive 'ska-telmodel cp' for subdirectory"""

    with tempfile.TemporaryDirectory("test-telmodel-nexus") as dirname:
        run_main(
            monkeypatch,
            "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
            "cp",
            "-R",
            "tango",
            dirname,
        )

        captured = capsys.readouterr()
        path = "ska_wide/SKAMaster.yaml"
        assert captured.out == f"tango/{path} -> {dirname}/{path}\n"
        assert not captured.err

        with open(dirname + "/ska_wide/SKAMaster.yaml") as f:
            assert f.read() == "test_bogus\n"


def test_cli_cp_r3(monkeypatch, capsys):
    """Test recursive 'ska-telmodel cp' for individual file"""

    with tempfile.TemporaryDirectory("test-telmodel-nexus") as dirname:
        run_main(
            monkeypatch,
            "--sources=mem://?tango/ska_wide/SKAMaster.yaml=test_bogus",
            "cp",
            "-R",
            "tango/ska_wide/SKAMaster.yaml",
            dirname,
        )

        captured = capsys.readouterr()
        assert (
            captured.out
            == f"tango/ska_wide/SKAMaster.yaml -> {dirname}/SKAMaster.yaml\n"
        )
        assert not captured.err

        with open(dirname + "/SKAMaster.yaml") as f:
            assert f.read() == "test_bogus\n"


@patch("ska_telmodel.data.frontend.large_file_download")
def test_cli_cp_r_local_dest_dir_linked(mock_download, monkeypatch, capsys):
    """Test 'ska-telmodel ls'"""

    with tempfile.TemporaryDirectory("ska-telmodel") as dirname:
        parent = pathlib.Path(dirname, "tango", "ska_wide")
        parent.mkdir(parents=True)
        mock_download.return_value = parent / "download_content"
        with (parent / "download_content").open("w") as file:
            file.write("Lorem Ipsum")
        with open(parent / "frontend.py.link", "w") as f:
            content = large_file_link_content(
                "123abc",
                "http://test.link/file.link",
                "tmdata/ska_wide/frontend.py",
            )
            f.write(content)
        monkeypatch.chdir(dirname)
        run_main(monkeypatch, "--local", "cp", "-R", "tango/ska_wide")
        with open(f"{dirname}/frontend.py", "r") as file:
            download_content = file.read()

    captured = capsys.readouterr()
    assert captured.out == "tango/ska_wide/frontend.py -> frontend.py\n"
    assert download_content == "Lorem Ipsum"
    assert not captured.err


def test_cli_validate(monkeypatch, caplog):
    """Test 'ska-telmodel validate'"""

    schema = sdp_interface_versions(SDP_ASSIGNRES_PREFIX)[-1]
    example = get_sdp_assignres_example(schema)
    example["interface"] = schema

    encoded = urllib.parse.urlencode({"test.json": json.dumps(example)})

    caplog.set_level(logging.INFO)
    run_main(
        monkeypatch,
        f"--sources=mem://?{encoded}",
        "validate",
        "test.json",
    )

    assert "Checking test.json" in caplog.text
    assert schema in caplog.text


@pytest.mark.parametrize("prefix", ["", "test"])
def test_cli_validate_recursive(monkeypatch, caplog, prefix):
    """Test recursive 'ska-telmodel validate'"""

    schema = sdp_interface_versions(SDP_ASSIGNRES_PREFIX)[-1]
    example = get_sdp_assignres_example(schema)
    example["interface"] = schema

    encoded = urllib.parse.urlencode({"test/test.json": json.dumps(example)})

    caplog.set_level(logging.INFO)
    run_main(
        monkeypatch, f"--sources=mem://?{encoded}", "validate", "-R", prefix
    )

    assert "Checking test/test.json" in caplog.text
    assert schema in caplog.text


def test_cli_validate_fail(monkeypatch, caplog):
    """Test 'ska-telmodel validate' schema validation fail"""

    versions = sdp_interface_versions(SDP_ASSIGNRES_PREFIX)
    example = get_sdp_assignres_example(versions[-1])
    example["interface"] = versions[0]  # mislabel!
    encoded = urllib.parse.urlencode({"test.json": json.dumps(example)})

    caplog.set_level(logging.ERROR)
    with pytest.raises(SystemExit, match="1"):
        run_main(
            monkeypatch, f"--sources=mem://?{encoded}", "validate", "test.json"
        )

    assert "Validation '" in caplog.text
    assert "' error:" in caplog.text
    assert "Missing keys:" in caplog.text


def test_cli_validate_fail_no_interface(monkeypatch, caplog):
    """Test 'ska-telmodel validate' failing due to 'interface' not set"""

    schema = sdp_interface_versions(SDP_ASSIGNRES_PREFIX)[-1]
    example = get_sdp_assignres_example(schema)
    encoded = urllib.parse.urlencode({"test.json": json.dumps(example)})

    caplog.set_level(logging.ERROR)
    with pytest.raises(SystemExit, match="1"):
        run_main(
            monkeypatch, f"--sources=mem://?{encoded}", "validate", "test.json"
        )

    assert "No schema to use for validation!" in caplog.text


def test_cli_validate_fail_deserialise(monkeypatch, caplog):
    """Test 'ska-telmodel validate' failing due to unserialisable object"""

    caplog.set_level(logging.ERROR)
    with pytest.raises(SystemExit, match="1"):
        run_main(
            monkeypatch,
            "--sources=mem://?test.json=invalid",
            "validate",
            "test.json",
        )

    assert "Expecting value" in caplog.text


def test_cli_help_cmd(monkeypatch, capsys):
    """Test recursive 'ska-telmodel cp'"""

    with pytest.raises(SystemExit):
        run_main(monkeypatch, "cp", "--help")

    captured = capsys.readouterr()
    assert "positional arguments" in captured.out
    assert "options:" in captured.out


def test_cli_upload_file_not_exist(monkeypatch, caplog):
    caplog.set_level(logging.INFO)
    with pytest.raises(SystemExit):
        run_main(monkeypatch, "upload", "some-non-existing-file")
    assert 'Input file "some-non-existing-file" does not exist' in caplog.text


def test_cli_upload_no_repo_not_found(monkeypatch, caplog):
    test_file = "tests/upload_backend_test_data/linky.file.link"
    with pytest.raises(SystemExit):
        run_main(
            monkeypatch,
            "upload",
            test_file,
        )

    assert (
        "Could not automatically determine repo/path, "
        "try specifying --repo or path"
    ) in caplog.text


def test_cli_upload_no_repo_dir_given_not_found(monkeypatch, caplog):
    caplog.set_level(logging.INFO)
    with pytest.raises(SystemExit):
        run_main(
            monkeypatch,
            "upload",
            "tests/upload_backend_test_data/linky.file.link",
            "instrument/ska1_low",
        )
    assert (
        "Could not automatically determine repo/path, "
        "try specifying --repo or path"
    ) in caplog.text


def test_cli_upload_known_repo_file(monkeypatch, caplog):
    caplog.set_level(logging.INFO)
    call_list, mock_sources = _create_mocked_gitbackend(
        monkeypatch, mock_sources_function=False
    )

    with tempfile.TemporaryDirectory() as tmp_dir:
        upload_existing = pathlib.Path(tmp_dir) / "SKAMaster.yaml"
        with upload_existing.open("w") as file:
            file.write("new_content: 1")
        upload = pathlib.Path(tmp_dir) / "SKAMaster_new.yaml"
        with upload.open("w") as file:
            file.write("updated_content: 1")

        run_main(
            monkeypatch,
            f"--sources=file://{tmp_dir}",
            "upload",
            f"{tmp_dir}/SKAMaster_new.yaml",
            "SKAMaster.yaml",
        )

        assert upload_existing.exists()
        with upload_existing.open("r") as file:
            assert file.read() == "updated_content: 1"

    assert call_list == []

    assert mock_sources.mock_calls == []
    assert "Adding file to repository directly" in caplog.text
    assert "File added" in caplog.text


def test_cli_upload_known_repo_mem(monkeypatch, caplog):
    with pytest.raises(SystemExit):
        with tempfile.TemporaryDirectory() as tmp_dir:
            upload = pathlib.Path(tmp_dir) / "SKAMaster.yaml"
            with upload.open("w") as file:
                file.write("new_content: 1")
            run_main(
                monkeypatch,
                "--sources=mem://?SKAMaster.yaml=test_bogus",
                "upload",
                f"{tmp_dir}/SKAMaster.yaml",
                "SKAMaster.yaml",
            )
    assert "Memory backend is not supported" in caplog.text


def test_cli_upload_known_repo_car(monkeypatch, caplog):
    caplog.set_level(logging.INFO)
    call_list, mock_sources = _create_mocked_gitbackend(monkeypatch)
    mock_file = MagicMock()
    mock_source = MagicMock()
    mock_source.backend_name.return_value = "car"
    mock_source.project_name = "car_project"
    my_tm_object = TMObject(mock_source, "")
    mock_file.source = mock_source
    mock_sources.__getitem__.return_value = my_tm_object

    monkeypatch.setenv(
        "CAR_TMDATA_USERNAME",
        "stdin_user",
    )
    monkeypatch.setenv(
        "CAR_TMDATA_PASSWORD",
        "stdin_password",
    )
    monkeypatch.setattr("builtins.input", lambda _: "stdin_input")
    with tempfile.TemporaryDirectory() as tmp_dir:
        upload = pathlib.Path(tmp_dir) / "SKAMaster.yaml"
        with upload.open("w") as file:
            file.write("new_content: 1")
        run_main(
            monkeypatch,
            "--sources=car:ska-telmodel-data?main",
            "upload",
            f"{tmp_dir}/SKAMaster.yaml",
            "SKAMaster.yaml",
        )

    assert call_list == [
        ["sources", ["car:ska-telmodel-data?main", False, False]],
        ["init", ["car_project", "gitlab.com"]],
        ["start", ["stdin_input"]],
        ["add", [f"{tmp_dir}/SKAMaster.yaml", "SKAMaster.yaml"]],
        ["commit", ["stdin_input"]],
        ["end", []],
    ]

    assert mock_sources.mock_calls == [call.__getitem__("SKAMaster.yaml")]
    assert "Adding file to repository directly" in caplog.text
    assert "File added" in caplog.text


def test_cli_upload_known_repo_git(monkeypatch, caplog):
    caplog.set_level(logging.INFO)
    call_list, mock_sources = _create_mocked_gitbackend(monkeypatch)
    mock_file = MagicMock()
    mock_source = MagicMock()
    mock_source.backend_name.return_value = "gitlab"
    mock_source.project_name = "gitlab_project"
    my_tm_object = TMObject(mock_source, "")
    mock_file.source = mock_source
    mock_sources.__getitem__.return_value = my_tm_object

    monkeypatch.setenv(
        "CAR_TMDATA_USERNAME",
        "stdin_user",
    )
    monkeypatch.setenv(
        "CAR_TMDATA_PASSWORD",
        "stdin_password",
    )
    monkeypatch.setattr("builtins.input", lambda _: "stdin_input")
    with tempfile.TemporaryDirectory() as tmp_dir:
        upload = pathlib.Path(tmp_dir) / "SKAMaster.yaml"
        with upload.open("w") as file:
            file.write("new_content: 1")
        run_main(
            monkeypatch,
            "--sources=gitlab:ska-telmodel-data?main",
            "upload",
            f"{tmp_dir}/SKAMaster.yaml",
            "SKAMaster.yaml",
        )

    assert call_list == [
        ["sources", ["gitlab:ska-telmodel-data?main", False, False]],
        ["init", ["gitlab_project", "gitlab.com"]],
        ["start", ["stdin_input"]],
        ["add", [f"{tmp_dir}/SKAMaster.yaml", "SKAMaster.yaml"]],
        ["commit", ["stdin_input"]],
        ["end", []],
    ]

    assert mock_sources.mock_calls == [call.__getitem__("SKAMaster.yaml")]
    assert "Adding file to repository directly" in caplog.text
    assert "File added" in caplog.text


@patch("ska_telmodel.cli.upload_large_file")
def test_cli_upload_give_repo_file_large_File(
    mock_upload, monkeypatch, caplog
):
    caplog.set_level(logging.INFO)
    call_list, mock_sources = _create_mocked_gitbackend(
        monkeypatch, mock_sources_function=False
    )
    mock_upload.return_value = "link file contents"

    with tempfile.TemporaryDirectory() as tmp_dir:
        upload_existing = pathlib.Path(tmp_dir) / "SKAMaster.yaml"
        with upload_existing.open("w") as file:
            file.write("new_content: 1")
        upload = pathlib.Path(tmp_dir) / "SKAMaster_new.yaml"
        with upload.open("w") as file:
            file.write("updated_content: 1")

        run_main(
            monkeypatch,
            "upload",
            f"--repo={tmp_dir}",
            "--force-car-upload",
            f"{tmp_dir}/SKAMaster_new.yaml",
            "SKAMaster.yaml",
        )

        link_file = pathlib.Path(tmp_dir) / "SKAMaster.yaml.link"
        assert link_file.exists()
        with link_file.open("r") as file:
            link_file_contents = file.read()

    assert "link file contents" == link_file_contents
    assert call_list == []
    assert mock_sources.mock_calls == []
    assert mock_upload.mock_calls == [
        call("local_files", upload, "SKAMaster.yaml")
    ]
    assert "Large file. Uploading file to the CAR" in caplog.text
    assert "File added" in caplog.text


@patch("ska_telmodel.cli.upload_large_file")
def test_cli_upload_give_repo_git_large_file(mock_upload, monkeypatch, caplog):
    caplog.set_level(logging.INFO)
    call_list, mock_sources = _create_mocked_gitbackend(
        monkeypatch, mock_sources_function=False
    )

    mock_upload.return_value = "link file contents"

    monkeypatch.setattr("builtins.input", lambda _: "stdin_input")
    with tempfile.TemporaryDirectory() as tmp_dir:
        upload = pathlib.Path(tmp_dir) / "SKAMaster_new.yaml"
        with upload.open("w") as file:
            file.write("updated_content: 1")

        run_main(
            monkeypatch,
            "upload",
            "--repo=random/git",
            "--force-car-upload",
            f"{tmp_dir}/SKAMaster_new.yaml",
            "SKAMaster.yaml",
        )

    assert call_list == [
        ["init", ["random/git", "gitlab.com"]],
        ["start", ["stdin_input"]],
        [
            "add",
            [ANY, "SKAMaster.yaml.link"],
        ],
        ["commit", ["stdin_input"]],
        ["end", []],
    ]
    assert mock_sources.mock_calls == []
    assert mock_upload.mock_calls == [
        call("random/git", upload, "SKAMaster.yaml")
    ]
    assert "Large file. Uploading file to the CAR" in caplog.text
    assert "File added" in caplog.text


input_count = 0


@patch("ska_telmodel.cli.upload_large_file")
def test_upload_to_git_existing_branch(mock_upload, monkeypatch, caplog):
    caplog.set_level(logging.INFO)
    global input_count
    call_list = []

    def mock_init(
        self,
        repo="ska-telescope/ska-telmodel-data",
        git_host="gitlab.com",
    ):
        call_list.append(["init", [repo, git_host]])
        self.repo = repo
        self.git_host = git_host

    def start_transaction(self, name_of_update, create_new_branch=True):
        call_list.append(["start", [name_of_update, create_new_branch]])
        if create_new_branch:
            raise ValueError("Branch Already Exists")

    def add_data(self, path, key):
        call_list.append(["add", [str(path), key]])

    def commit(self, message):
        call_list.append(["commit", [message]])

    def commit_transaction(self):
        call_list.append(["end", []])

    mocked_sources = MagicMock()

    def mock_sources(self, sources, local, update):
        call_list.append(["sources", [sources, local, update]])
        self._sources = mocked_sources

    monkeypatch.setattr(ska_telmodel.cli.CLI, "sources", mock_sources)

    monkeypatch.setattr(GitBackend, "__init__", mock_init)
    monkeypatch.setattr(GitBackend, "start_transaction", start_transaction)
    monkeypatch.setattr(GitBackend, "add_data", add_data)
    monkeypatch.setattr(GitBackend, "commit", commit)
    monkeypatch.setattr(GitBackend, "commit_transaction", commit_transaction)

    mock_upload.return_value = "link file contents"

    input_count = 0

    def stdin_input(s=""):
        global input_count
        input_count += 1
        if input_count == 1:
            return "str1"
        if input_count == 2:
            return ""
        if input_count == 3:
            return "Commit message"

    monkeypatch.setattr("builtins.input", stdin_input)
    with tempfile.TemporaryDirectory() as tmp_dir:
        upload = pathlib.Path(tmp_dir) / "SKAMaster_new.yaml"
        with upload.open("w") as file:
            file.write("updated_content: 1")

        run_main(
            monkeypatch,
            "upload",
            "--repo=random/git",
            "--force-car-upload",
            f"{tmp_dir}/SKAMaster_new.yaml",
            "SKAMaster.yaml",
        )

    assert call_list == [
        ["sources", [None, False, False]],
        ["init", ["random/git", "gitlab.com"]],
        ["start", ["str1", True]],
        ["start", ["str1", False]],
        [
            "add",
            [ANY, "SKAMaster.yaml.link"],
        ],
        ["commit", ["Commit message"]],
        ["end", []],
    ]
    assert mocked_sources.mock_calls == []
    assert mock_upload.mock_calls == [
        call("random/git", upload, "SKAMaster.yaml")
    ]
    assert "Branch already exists" in caplog.text
    assert "Large file. Uploading file to the CAR" in caplog.text
    assert "File added" in caplog.text
    assert (
        "Branch used: str1, please create relevant merge request."
        in caplog.text
    )
    assert (
        "https://gitlab.com/random/git/-/merge_requests/new?"
        "merge_request%5Bsource_branch%5D=str1"
    ) in caplog.text


@patch("ska_telmodel.cli.upload_large_file")
def test_upload_to_git_create_new_second_branch(
    mock_upload, monkeypatch, caplog
):
    caplog.set_level(logging.INFO)
    global input_count
    call_list = []

    def mock_init(
        self,
        repo="ska-telescope/ska-telmodel-data",
        git_host="gitlab.com",
    ):
        call_list.append(["init", [repo, git_host]])
        self.repo = repo
        self.git_host = git_host

    def start_transaction(self, name_of_update, create_new_branch=True):
        call_list.append(["start", [name_of_update, create_new_branch]])
        if create_new_branch and name_of_update == "str1":
            raise ValueError("Branch Already Exists")

    def add_data(self, path, key):
        call_list.append(["add", [str(path), key]])

    def commit(self, message):
        call_list.append(["commit", [message]])

    def commit_transaction(self):
        call_list.append(["end", []])

    mocked_sources = MagicMock()

    def mock_sources(self, sources, local, update):
        call_list.append(["sources", [sources, local, update]])
        self._sources = mocked_sources

    monkeypatch.setattr(ska_telmodel.cli.CLI, "sources", mock_sources)

    monkeypatch.setattr(GitBackend, "__init__", mock_init)
    monkeypatch.setattr(GitBackend, "start_transaction", start_transaction)
    monkeypatch.setattr(GitBackend, "add_data", add_data)
    monkeypatch.setattr(GitBackend, "commit", commit)
    monkeypatch.setattr(GitBackend, "commit_transaction", commit_transaction)

    mock_upload.return_value = "link file contents"

    input_count = 0

    def stdin_input(s=""):
        global input_count
        input_count += 1
        if input_count == 1:
            return "str1"
        if input_count == 2:
            return "str2"
        if input_count == 3:
            return "Commit message"

    monkeypatch.setattr("builtins.input", stdin_input)
    with tempfile.TemporaryDirectory() as tmp_dir:
        upload = pathlib.Path(tmp_dir) / "SKAMaster_new.yaml"
        with upload.open("w") as file:
            file.write("updated_content: 1")

        run_main(
            monkeypatch,
            "upload",
            "--repo=random/git",
            "--force-car-upload",
            f"{tmp_dir}/SKAMaster_new.yaml",
            "SKAMaster.yaml",
        )

    assert call_list == [
        ["sources", [None, False, False]],
        ["init", ["random/git", "gitlab.com"]],
        ["start", ["str1", True]],
        ["start", ["str2", True]],
        [
            "add",
            [ANY, "SKAMaster.yaml.link"],
        ],
        ["commit", ["Commit message"]],
        ["end", []],
    ]
    assert mocked_sources.mock_calls == []
    assert mock_upload.mock_calls == [
        call("random/git", upload, "SKAMaster.yaml")
    ]
    assert "Branch already exists" in caplog.text
    assert "Large file. Uploading file to the CAR" in caplog.text
    assert "File added" in caplog.text
    assert (
        "Branch used: str2, please create relevant merge request."
        in caplog.text
    )
    assert (
        "https://gitlab.com/random/git/-/merge_requests/new?"
        "merge_request%5Bsource_branch%5D=str2"
    ) in caplog.text


def _create_mocked_gitbackend(monkeypatch, mock_sources_function=True):
    call_list = []

    def mock_init(
        self,
        repo="ska-telescope/ska-telmodel-data",
        git_host="gitlab.com",
    ):
        call_list.append(["init", [repo, git_host]])
        self.repo = repo
        self.git_host = git_host

    def start_transaction(self, name_of_update):
        call_list.append(["start", [name_of_update]])

    def add_data(self, path, key):
        call_list.append(["add", [str(path), key]])

    def commit(self, message):
        call_list.append(["commit", [message]])

    def commit_transaction(self):
        call_list.append(["end", []])

    mocked_sources = MagicMock()

    def mock_sources(self, sources, local, update):
        call_list.append(["sources", [sources, local, update]])
        self._sources = mocked_sources

    if mock_sources_function:
        monkeypatch.setattr(ska_telmodel.cli.CLI, "sources", mock_sources)

    monkeypatch.setattr(GitBackend, "__init__", mock_init)
    monkeypatch.setattr(GitBackend, "start_transaction", start_transaction)
    monkeypatch.setattr(GitBackend, "add_data", add_data)
    monkeypatch.setattr(GitBackend, "commit", commit)
    monkeypatch.setattr(GitBackend, "commit_transaction", commit_transaction)
    return call_list, mocked_sources

CSP releaseresources 2.3
========================

JSON schema and example for CSP Mid releaseresources

.. ska-schema:: https://schema.skao.int/ska-csp-releaseresources/2.3
   :auto_reference:
   :auto_target:
   :lift_description:
   :lift_definitions:
   :lift_title:

   .. ska-schema-example:: https://schema.skao.int/ska-csp-releaseresources/2.3

       Example JSON (Release all resources)

   .. ska-schema-example:: https://schema.skao.int/ska-csp-releaseresources/2.3 2

       Example JSON (Release specified resources)

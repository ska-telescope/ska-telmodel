"""Version 1.0 of SDP schema"""

from ska_telmodel._common import TMSchema
from ska_telmodel.skydirection import get_skydirection

from . import v0_5
from .v0_5 import (
    get_sdp_configure,
    get_sdp_recvaddrs,
    get_sdp_releaseres,
    get_sdp_scan,
)

__all__ = [
    "get_sdp_assignres",
    "get_sdp_configure",
    "get_sdp_scan",
    "get_sdp_recvaddrs",
    "get_sdp_releaseres",
]


def get_sdp_assignres(version: str, strict: bool) -> TMSchema:
    # Get 0.5 version
    schema = v0_5.get_sdp_assignres(version, strict)

    # Change phase_dir to use ADR-63's skydirection
    field_schema = schema["execution_block"]["fields"][0]
    del field_schema["phase_dir"]
    field_schema.add_field("phase_dir", get_skydirection(version, strict))

    return schema

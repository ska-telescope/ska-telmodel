"""
SKA Low MCCS schemas
"""

from inspect import cleandoc

from schema import And, Schema

from .._common import TMSchema, mk_if, split_interface_version
from . import validators


def get_mccs_assignedres_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control assigned resources schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    if_strict = mk_if(strict)

    items = TMSchema.new("Low MCCS assigned resources", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_field(
        "subarray_beam_ids",
        [
            And(
                int,
                if_strict(lambda n: validators.validate_subarray_beam_id(n)),
            ),
        ],
        check_strict=lambda x: validators.validate_subarray_beam_ids(x),
        description=cleandoc(
            """
            IDs of the MCCS sub-array beams allocated to this MCCS
            subarray.

            Each ID must be between 1 and 48, the maximum number of MCCS
            sub-array beams.

            As of PI10, only one MCCS sub-array beam can be
            configured per allocation request. Multiple beams must
            be allocated via multiple allocation requests.
            """
        ),
    )
    items.add_field(
        "station_ids",
        [[And(int, if_strict(lambda n: validators.validate_station_id(n)))]],
        description=cleandoc(
            """
            IDs of MCCS stations allocated to each sub-array beam.

            Each ID must be between 1 and 512, the maximum number of
            stations.
            """
        ),
    )
    items.add_field(
        "channel_blocks",
        [int],
        check_strict=lambda n: validators.validate_channel_blocks(n),
        description=cleandoc(
            """
            Number of channel blocks allocated to each sub-array beam.

            Maximum number of channel blocks = 48.
            """
        ),
    )

    return items


def get_mccs_assignres_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control assign resources schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    if_strict = mk_if(strict)

    items = TMSchema.new("Low MCCS assign resources", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    if split_interface_version(version) < (3, 0):
        items.add_field(
            "subarray_id",
            int,
            check_strict=lambda n: validators.validate_subarray_id(n),
            description=(
                "ID of sub-array targeted by this resource allocation request"
            ),
        )
        items.add_field(
            "subarray_beam_ids",
            [
                And(
                    int,
                    if_strict(
                        lambda n: validators.validate_subarray_beam_id(n)
                    ),
                )
            ],
            check_strict=lambda x: validators.validate_subarray_beam_ids(x),
            description=cleandoc(
                """
                IDs of the MCCS sub-array beams to allocate to this MCCS
                subarray.

                Each ID must be between 1 and 48, the maximum number of
                sub-array beams.

                As of PI10, only one MCCS sub-array beam can be
                configured per allocation request. Multiple beams must
                be allocated via multiple allocation requests.
                """
            ),
        )
        items.add_field(
            "station_ids",
            [
                [
                    And(
                        int,
                        if_strict(lambda n: validators.validate_station_id(n)),
                    )
                ]
            ],
            description=cleandoc(
                """
                IDs of MCCS stations to allocate to this sub-array beam.

                Each ID must be between 1 and 512, the maximum number of
                stations.
                """
            ),
        )
        items.add_field(
            "channel_blocks",
            [int],
            check_strict=lambda n: validators.validate_channel_blocks(n),
            description=cleandoc(
                """
                Number of channel blocks to allocate to this sub-array beam.

                Maximum number of channel blocks = 48.
                """
            ),
        )

    if split_interface_version(version) == (3, 0):
        mccs_subarray_beams_items = TMSchema.new(
            "MCCS subarray beams", version, strict
        )
        mccs_subarray_beams_items.add_field(
            "subarray_beam_id",
            int,
            check_strict=lambda n: validators.validate_low_subarray_beam_ids(
                n
            ),
            description=cleandoc(
                """
                ID of MCCS sub-array beam .

                ID must be an integer between 1 and 48.
                """
            ),
        )
        apertures_items = TMSchema.new("MCCS Aperture ID", version, strict)
        apertures_items.add_field(
            "aperture_id",
            str,
            check_strict=lambda n: validators.validate_aperture_id(n),
            description=cleandoc(
                """
                ID of MCCS aperture.
                Aperture ID, of the form APXXX.YY
                XXX=station YY=substation.
                """
            ),
        )
        apertures_items.add_field(
            "station_id",
            int,
            check_strict=lambda n: validators.validate_station_id(n),
            description=cleandoc(
                """
                IDs of MCCS stations to allocate to this sub-array beam.

                Each ID must be between 1 and 512, the maximum number of
                stations.
                """
            ),
        )
        mccs_subarray_beams_items.add_field(
            "apertures",
            And(
                [apertures_items],
                lambda n: len(n) <= validators.MAX_NO_APERATURES,
            ),
            description="MCCS apertures.",
        )
        mccs_subarray_beams_items.add_field(
            "number_of_channels",
            int,
            check_strict=lambda n: validators.validate_number_of_channel(n),
            description=cleandoc(
                """
                The allocated number of SPS channels.

                ID must be an integer between 8 and 384.
                """
            ),
        )
        items.add_field(
            "subarray_beams",
            And(
                [mccs_subarray_beams_items],
                lambda n: len(n) <= validators.MAX_NO_SUBARRAY_BEAMS,
            ),
            description="MCCS sub-array beam.",
        )

    return items


def get_mccs_configure_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control configuration schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    if_strict = mk_if(strict)

    items = TMSchema.new("Low MCCS configure", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_field(
        "stations",
        [
            {
                "station_id": And(
                    int, if_strict(lambda n: validators.validate_station_id(n))
                )
            }
        ],
        check_strict=lambda x: validators.validate_stations(x),
        description=cleandoc(
            """
            IDs of the MCCS stations to configure.

            Maximum array size = 512, the maximum number of MCCS
            stations.
            """
        ),
    )
    subarray_beam_items = TMSchema.new(
        "Low MCCS subarray beam", version, strict
    )
    subarray_beam_items.add_field(
        "subarray_beam_id",
        int,
        check_strict=lambda n: validators.validate_subarray_beam_id(n),
        description=cleandoc(
            """
            ID of MCCS sub-array beam to configure.

            ID must be an integer between 1 and 48.
            """
        ),
    )
    subarray_beam_items.add_field(
        "station_ids",
        [And(int, if_strict(lambda n: validators.validate_station_id(n)))],
        description=cleandoc(
            """
            IDs of MCCS stations within this sub-array
            beamto configure.

            Array size must be less than 512, the maximum
            number of MCCS stations.

            Each item in the list must be an integer
            between 1 and 512.
            """
        ),
    )
    subarray_beam_items.add_field(
        "update_rate",
        float,
        check_strict=lambda n: validators.validate_update_rate(n),
        description=cleandoc(
            """
            Update rate for pointing information.

            Value must be 0.0 or greater.

            TODO: clarify whether this is specified as a
            frequency or as a cadence, plus units.
            """
        ),
    )
    subarray_beam_items.add_field(
        "channels",
        [And([int], if_strict(lambda n: validators.validate_channel(n)))],
        check_strict=lambda x: validators.validate_channels(x),
        description=cleandoc(
            """
            Channel block configurations.

            Each item in the list is a channel block
            configuration, each specified as a list of 4
            numbers as follows:

            [start channel, number of channels, beam index, sub-station index]

            Constraints are:

            0 < start channel < 376

            start channel must be a multiple of 8

            8 < number of channels < 48

            1 < beam index < 48

            1 < sub-station index < 8
            """
        ),
    )
    subarray_beam_items.add_field(
        "antenna_weights",
        [
            And(
                float,
                if_strict(lambda n: validators.validate_antenna_weight(n)),
            )
        ],
        check_strict=lambda x: validators.validate_antenna_weights(x),
        description=cleandoc(
            """
            Antenna weights.

            Maximum array size = 512 (=256 antennas x2 pols per
            sub-array beam).

            Antennas signals can be weighted to modify the station
            beam, varying from 0.0 for full exclusion to potentially
            256.0 for an antenna contribution compensated for the
            number of antennas in the beam. This value is an
            amplitude multiplier added to that antenna signal before
            adding into the sum.

            Weights apply to all channels assigned to a beam.
            """
        ),
    )
    subarray_beam_items.add_field(
        "phase_centre",
        [
            And(
                float,
                if_strict(lambda n: validators.validate_phase_centre_value(n)),
            )
        ],
        check_strict=lambda x: validators.validate_phase_centre(x),
        description=cleandoc(
            """
            Phase centre offset for the station beam, in
            metres.

            The reference position for station phase must be
            modified to reflect antenna weighting and their
            contribution to the station beam. This offset can
            be can considered the desired centre of mass for the
            station.

            Constraints:
            array size = 2
            -20 < phase centre value < 20
            """
        ),
    )
    subarray_beam_items.add_field(
        "sky_coordinates",
        [float],
        description="Azimuth/elevation of sub-array beam target, in degrees.",
    )

    items.add_field(
        "subarray_beams",
        And(
            [subarray_beam_items],
            lambda n: len(n) <= validators.MAX_NO_SUBARRAY_BEAMS,
        ),
        description="MCCS sub-array beam configuration.",
    )

    return items


def get_mccs_scan_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control scan schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """
    items = TMSchema.new("Low MCCS scan", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_field(
        "scan_id",
        int,
        description=cleandoc(
            """
            Scan ID to associate with the data.

            The scan ID and SBI ID are used together to uniquely associate
            the data taken with the telescope configuration in effect at
            the moment of observation.
            """
        ),
    )
    items.add_field(
        "start_time",
        float,
        check_strict=lambda n: validators.validate_scan_start_time(n),
        description=cleandoc(
            """
            Start time for the scan.

            Currently unused and can be set to 0.0.
            """
        ),
    )

    return items


def get_mccs_releaseres_schema(version: str, strict: bool) -> Schema:
    """SKA Low Monitoring and Control resources release schema

    :param version: Interface Version URI
    :param strict: Schema strictness
    :return: Schema
    """

    items = TMSchema.new("Low MCCS resource release", version, strict)
    items.add_field(
        "interface",
        str,
        description="URI of JSON schema applicable to this JSON payload.",
    )
    items.add_field(
        "subarray_id",
        int,
        check_strict=lambda n: validators.validate_subarray_id(n),
        description="ID of the MCCS sub-array which should release resources.",
    )
    items.add_field(
        "release_all",
        bool,
        description=cleandoc(
            """
            true to release all resources, false to release only the
            resources defined in this payload.

            Note: partial resource release for MCCS is not implemented
            and the identification of the resources to release is not yet
            part of the schema.
            """
        ),
    )

    return items
